Scripts for Reborn Mod

Reborn_Game_Manager: Place this script on a hidden object and place that object on the map. If you are using this object in multiple maps, they must be of the same ID (well, that's the preferably way). It handles MMK2 and Cyborg limits (provided that the objects are running the script)

Parameters - None

Reborn_MMK2: Script used on a unit you want to tie to the MMK2 limit. Upon creation, if the limit is reached it will automaticlly blow up. MMK2 limit is one.

Parameters - GameManager_ID (the ID of the game manager on the map(s), game manager MUST exist in order for this to work) and Explosion_Preset_Name (explosion used to kill the MMK2). MoneyToReturn is the amount of credits to give the player if he losses the unit because there already is an MMK2 on the map.

Reborn_Cyborg: Same as the MMK2 script, but ties into the Cyborg Commando limit. CC Limit is one.

Reborn_Deployable_Vehicle: When someone jumps into the vehicle, it sets that person as the "owner". Once an owner is defined, the owner can shoot the vehicle and it will change into an animated version which plays the deploy animation. Health and Armor is transfered. LeaveOffset is used when the vehicle blows up (also when deploying), to move the player out of position so that he doesn't get squished.

Parameters - Animated_Preset (the preset name of the animated vehicle) and Explosion_Preset (the preset used the explode the tank)

Reborn_Deploy_Animation: Just used on the vehicle running the deploy animation. Blows up the object after so long and spawns the deployed, or undeployed, preset.

Parameters - Time_Till_Death (how long to wait before killing the tank), Deployed_Tank_Preset (the preset of the deployed, or undeployed, tank), and Explosion_Preset (to kill the animator)

Reborn_MMK2_Turret_Spawn: Makes an object at it's controller's bone. This is used to spawn the automated turret you see on the MMK2 in Reborn.

Parameters - Turret_Preset (preset name of the object to spawn), Bone_Name (the name of the bone to attach the object to), and Explosion_Preset (made at the bone when the MMK2 or whatevers dies to get rid of the object spawned)

Reborn_Diggable_Vehicle: Used to make a vehicle go underground. Somewhat confusing but here's how it works:

A player hops in the pilot slot of a vehicle, and a timer starts. When the timer expires, the vehicle goes underground.

-If the pilot hops out of the vehicle, the timer resets and does not start until another player hops into the pilot slot.

-If the pilot hops out while underground, the vehicle and pilot move upground.

One thing you must beware of, the distance betwean the underground layer and the normal battle field MUST be LESS OR EQUAL TO the parameter of Z_Hieght_Adjust.

Parameters - Mode (not used), Dig_Delay (how long the delay is before the vehicle goes underground when the pilot jumps in), Z_Hieght_Adjust (the amount to move the tank up or down when digging)

Reborn_Play_Sound_On_Create: Plays a 2D sound on creation.

Parameters - Sound_Preset_Name (the name of the 2D sound preset)

Survival Scripts
----------------
Most of these SHOULD work but some bugs have been seen (I think we got them all but I am unsure as some might have been bugs in the map, etc)

SUR_Spawn_Delay: Creates an object after so long, can make the created object follow a path. Used in a map to let AI construct it's own base.

Parameters - Spawn_Delay (the delay before spawning the object), Spawn_Pos (the position to add to the script's object's position), Spawn_Obj (the object to spawn), Spawn_Path (the ID of the path for the vehicle to follow), Do_Path (1=Yes 0=No, tells if the object should follow the path)

SUR_Generator_Zone: This is used in the Survival game type. What it does is, as time passes, an varible drops. It starts at 3. If it reaches 0, it kills the given buildings (to end the game). However if the given preset enters the zone, the varible is increased. So you could set it up so that everytime the harvester drives through the zone, the varible is increased (so long as it's below 3) and the team stays alive.

Parameters - Time_Delay (the delay betwean decreasing the varible), Supply_Truck_Preset (the preset name of the object to drive through the zone), Emitter_Pos (the position to create the object to show the variable's value, was initially an emitter, as you can tell by the name), Emitter_Preset_Good (the preset name if the varible is 3), Emitter_Preset_Ok (the preset name if the varible is 2), Emitter_Preset_Bad (the preset name if the varible is 1), Build_D_One , Build_D_Two , and Build_D_Three (the building IDs to destroy).

SUR_MiniGen: Similar to the generator script (in the game mode, used to destroy turrets as time passes). Essentally, if the generator gets blown up, it destroys its turrets. If the generator's time goes out, it destroys its turrets.

Parameters - Time_Delay (how long to wait before blowing everything up), the rest are the IDs of the turrets.

SUR_NHB: This is the "brain" of the army. Judges who goes where, also taunts a little. He splits up his army into areas (where the enemy gets their aircraft, ground vehicles, etc) to make sure to prevent the enemy from grabbing additional weapons that could hinder his objective. He will also keep track of the requests for jobs and make assumptions on death counts and re-forms his plans accordingly. If too many deaths/requests occur, NHB switches to a full assault and sends all units after the objectives, and will also summon an air strike.
GDI_Gen and Nod_Gen - These are the primary objectives that must be destroyed to win. Anything destroyable object can fill these parameters.
Strike_Preset - This is the preset to create (creates 2 of them) later in the game as an attack unit. Preferablly an air unit.
Taunt1, Taunt2, and OrcaWarning: These are the objects, preferably buildings, to apply explosive damage to (soemthing like 9999). Upon destruction these buildings will trigger a warning causing a message to apear, to use for taunts or warnings. The buildings MUST be on the GDI or Nod team, set one of the primary objectives to blow these up when they die so you don't have to wait until the bot uses these buildings to end the game. Set the object ID to something else if you dont want the bot to use these.

SUR_NHB_Soldier: This is used for infantry. These guys are the main part of the army. They ask NHB where to go and NHB tells them. These guys will also check if they're standing around in the same place for so long (if they get stuck) and, if so, will go back and continue the path.
NHB_ID: ID of the object with the NHB script.
Rest of params: Path information for various targets. Vpad would be where the vehicle pad or maybe WF would be at. Helipad would be for where players get air units... etc... Nod and GDI Gen paths are paths to primary objectives.
Note: Soldiers can also take out C4. To do this, send a message with the message number as 503030 and the param as 501120. This will, however, convince the soldiers that they are at their destination so they will not go back on their path if they stray from it.

SUR_APC_AI: This is used for an APCish unit. NHB judges how many troops come out of the APC, the APC itself however judges where it goes. Can also be used on say, a tank, giving it the ability to drop troops and fire apon enemys. The unit with this script acts as a guard once it makes it's delivery. Drops troops once it recieves a message with the number 503030 and a param with the number 202020.
Preset_To_Create: Preset of the soldiers to create.
NHB_ID: ID of the object with the NHB script.

SUR_Orca_AI: Doesn't really do anything special. Can be attached to anything, but NHB only sends units with this script out to the GDI generator (or whatever you designate that object to be).
NHB_ID: ID of the object with the NHB script.
Attack_Offset: The position, based on the target position, to move to while shooting at the target.

SUR_Chinook_AI: Not special. Designed for a chinook to fly around a path and, once it recieves a message with the number 503030 (zone entry or something else), drops something (like a tank).
Preset - The preset to create.
Position_Offset - Where, based on the chinook's position, to create the object.

SUR_Dep_Turret: This is really just a base defense script. However, it is designed to fire only on mutants (and maybe unteamed). Will not fire on items on GDI or Nod.
Params are same as that of M00_Base_Defense

SUR_Obj_Destroyer: Just kills something else when the object with this script dies.
ID - ID of the object to destroy. Uses Explosive damage type.

SUR_Timed_Death: .......kills itself after so long.....
Delay - How long to wait.
Damage - How much damage to apply.
Damage_Type - What damage type to apply.

SUR_GrantPowerup_OnEntry: Grants a powerup if a certain preset enters the zone.
Preset - This is the preset that gets the powerup if it enters the zone (set to Any with EXACT spelling for any preset to get the powerup)

Powerup - This is the powerup to be granted.

SUR_New_PT: For some, this might be an interesting script. This is used on a vehicle that looks like a PT (or something else that you can poke). Then it sends the desiginated message to something. In effect, you've just enabled new objects that you can poke (press E) and have it work in MP! Used to buy vehicles from a vehicle pad or helipad......kind of like in Tribes or PlanetSide, etc... Note that this object is re-created every time someone enters the vehicle, because of some bug that I couldn't get rid of (prevented people from exiting the PT).

Marker_ID - The Daves Arrow or whatever is to recieve the message.
Message - The message to send (like 1045255).

SUR_New_PT_Reciever: This is attached to whatever is supposed to recieve the msg from the SUR_New_PT script.

Preset_Name - Name of the preset to create, creates the preset a little above the object running this script.
Message - This is the message that the object running this script needs to recieve before it will create the object.

NH/Tech Assault Scripts
-----------------------

NH_Spawn_Object_On_Poke - Falsely named (changed lots since the begining) but works. Operates on damage instead of poke. Any object on the object with this script's team can shoot it to have it spawn something. There's a built in delay so something like a chaingun can't cause the whole thing to break. Note: If this object recieves a messsage, it automaticlly applies 9999 BlamoKiller apon itself to kill itself. Delay after being shot at by a teammate is 5 seconds.

Preset_Name: Name of the preset to spawn.
Add_Vectors: Vectors (XYZ) to add to current position, for the spawn location.

NH_SetTeam_OnLeave - This is meant for vehicles, every time a player leaves the vehicle it will switch its team to the player's team. No params needed for this script.

NH_Deploy_Building_On_Poke - Again falsely named (I have a bad habit of doing that don't I?) but works! This deploys an object when the owner of this script dies (MCVs!!!). Params are the same of that of NH_Spawn_Object_On_Poke in every way possible....

NH_Create_Buy_Panels - This is the same as NH_Deploy_Building_On_Poke but lets you deploy 4 objects in one script.

README

1st you need the standard model this must have your Model_Name.
Then you need the Fire anim its model name must be the same as the standard but with the ending _f and it must be an animation of the deployed model.
You need the deploy animation which ends with _d the moveanim _m the backwards moveanim _b and the deployed model _dd.
so your modelnames look like this:
model		(standard)
model_f		(fireanim the anim is called model_dd.model_f)
model_d		(deployanim the anim is called model_d.model_d)
model_dd	(deployed model the model which is used if the tank/mech is deployed)
model_m		(forward moveanim the anim is called model.model_m) THIS IS ONLY NEEDED FOR Reborn_IsDeployableTank if Enable_Moveanim = 1
model_b		(backwards moveanim the anim is called model.model_b) THIS IS NOT NEEDED FOR Reborn_IsDeployableTank
 
Reborn_IsMech

StompWAVSound:		The WAV file which gets played whenever the mechs feet hits the ground
Stomp1Frame_Forward:	The Frame where the first Foot hits the Ground (forward moveanim)
Stomp2Frame_Forward:	The Frame where the second Foot hits the Ground (forward moveanim)
Stomp1Frame_Backward:	The Frame where the first Foot hits the Ground (backward moveanim)
Stomp2Frame_Backward:	The Frame where the second Foot hits the Ground (backward moveanim)

Reborn_IsDeployableMech

Model_Name:		The Name of the w3d model
Weapon_Name:		The Preset Name of the Mechs weapon
Weapon_Powerup_Name:	The Powerup which includes the weapon
Last_Deploy_Frame:	The Last Frame of the Animated Deploy Model
StompWAVSound:		The WAV file which gets played whenever the mechs feet hits the ground
Stomp1Frame_Forward:	The Frame where the first Foot hits the Ground (forward moveanim)
Stomp2Frame_Forward:	The Frame where the second Foot hits the Ground (forward moveanim)
Stomp1Frame_Backward:	The Frame where the first Foot hits the Ground (backward moveanim)
Stomp2Frame_Backward:	The Frame where the second Foot hits the Ground (backward moveanim)

Reborn_IsDeployableTank

Model_Name:		The Name of the w3d Model
Weapon_Name:		The Preset Name of the Tanks weapon
Weapon_Powerup_Name:	The Powerup which includes the weapon
Last_Deploy_Frame:	The Last Frame of the Animated Deploy Model
Enable_Moveanim:	1 for enabled 0 for disabled moveanim

Reborn_Deployable_Vehicle_Player
Key:			What key to use for deploy
ID:			What object should recieve the message
Message:		What message to send
