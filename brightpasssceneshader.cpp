/* Renegade Scripts.dll
	BrightPassSceneShaderClass
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader_scene.h"
#include "brightpasssceneshader.h"

BrightPassSceneShaderClass::BrightPassSceneShaderClass()
{
 	Luminance = 0.08f;
#ifndef SDBEDIT
	Validated = false;
	ValidationResult = false;
	LuminanceHandle = 0;
  	SceneTextureHandle = 0;
	InputTextureHandle = 0;
#endif
	SceneShaderClass::SceneShaderClass();
}
BrightPassSceneShaderClass::~BrightPassSceneShaderClass()
{
	SceneShaderClass::~SceneShaderClass();
}
void BrightPassSceneShaderClass::Load(ChunkLoadClass& cload)
{
 	while (cload.Open_Micro_Chunk())
	{
		switch(cload.Cur_Micro_Chunk_ID())
		{
			case MC_BRIGHTPASSSCENESHADER_LUMINANCE:
				cload.Read(&Luminance,sizeof(float));
				break;
			case MC_SCENESHADER_UID:
				cload.Read(&UID,sizeof(unsigned int));
				break;
			case MC_SCENESHADER_NAME:
				Name = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(Name,cload.Cur_Micro_Chunk_Length());
				break;
			case MC_SCENESHADER_FXFILENAME:
				FXFilename = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(FXFilename,cload.Cur_Micro_Chunk_Length());
				#ifndef SDBEDIT
				Effect = new EffectClass(FXFilename);			
				#endif
				break;
		}
		cload.Close_Micro_Chunk();
	}	
}
void BrightPassSceneShaderClass::Save(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_BRIGHTPASSSCENESHADER);
	csave.Begin_Micro_Chunk(MC_BRIGHTPASSSCENESHADER_LUMINANCE);
	csave.Write(&Luminance,sizeof(float));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MC_SCENESHADER_UID);
	csave.Write(&UID,sizeof(unsigned int));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MC_SCENESHADER_NAME);
	csave.Write(Name,strlen(Name) + 1);
	csave.End_Micro_Chunk();
 	csave.Begin_Micro_Chunk(MC_SCENESHADER_FXFILENAME);
	csave.Write(FXFilename,strlen(FXFilename) + 1);
	csave.End_Micro_Chunk();
	csave.End_Chunk();
}
#ifndef SDBEDIT
void BrightPassSceneShaderClass::Initialize()
{
	SceneShaderClass::Initialize();
}
bool BrightPassSceneShaderClass::Validate()
{
	if (Validated) 
	{
		return ValidationResult;
	}
	if (!Effect->Initialized)
	{
		if (Effect->EffectLoadTask)
		{
			return false;
		}
		else
		{
			ValidationResult = false;
			Validated = true;
			return false;
		}
	}
	D3DXHANDLE TechniqueHandle = Effect->GetTechniqueByName("SceneShader");
	HRESULT hr = Effect->ValidateTechnique(TechniqueHandle);
	if (FAILED(hr))
	{
		ValidationResult = false;
	} 
	else 
	{
		Effect->SetTechnique("SceneShader");
		InputTextureHandle = Effect->GetParameterBySemantic(NULL,"InputTexture");
		SceneTextureHandle = Effect->GetParameterBySemantic(NULL,"SceneTexture");
		LuminanceHandle = Effect->GetParameterBySemantic(NULL,"Luminance");
		ValidationResult = true;
	}
	Validated = true;
	return ValidationResult;
}
void BrightPassSceneShaderClass::Render(SceneShaderRenderInfo *render)
{
	unsigned int cPasses;
	Effect->Begin(&cPasses,0);
	Effect->SetFloat(LuminanceHandle,Luminance);
	Effect->SetTexture(SceneTextureHandle,render->SceneBuffer);
	for (unsigned int pass = 0; pass < cPasses; pass++)
	{
		if (pass > 0) 
		{
			SceneShaderController->RequestNewBuffer(&render);
		}
		StateManager->SetRenderTarget(0,render->OutputSurface);
		Direct3DDevice->Clear(0,NULL,D3DCLEAR_TARGET,D3DCOLOR_XRGB(0,0,0),0,0);
		Effect->SetTexture(InputTextureHandle,render->InputBuffer);
		Effect->BeginPass(pass);
		Effect->CommitChanges();
		Direct3DDevice->BeginScene();
		RenderQuad->Draw(0,0,render->Width,render->Height,render->UScale,render->VScale);
		Direct3DDevice->EndScene();
		Effect->EndPass();
	}
	Effect->End();
}
#endif

SceneShaderRegistrant<BrightPassSceneShaderClass> BrightPassSceneShaderRegistrant(CHUNK_BRIGHTPASSSCENESHADER,"Bright Pass Scene Shader");
