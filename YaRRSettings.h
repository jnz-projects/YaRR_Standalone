

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


struct ChannelData
{
	char *Name;
	char *Key;
	char Access;
	char Threshold;

};

struct IRCData
{
	char *Host;
	int Port;
	char *NSPassword;
	char *Nick;
	char *Backup_Nick;
	Stacker<char *> *ExtraCommands;

	Stacker<ChannelData *> *Channels;
};

struct Moderator
{
	char Nick[40];
	char IRCNick[70];
	char Access;
	
};

struct Sound
{
	char Trigger[128];
	char File[128];
};

struct ObjectWorth
{
	char Preset[128];
	unsigned int Worth;
};

struct WavSound
{
	char Name[256];
	float Duration;
	bool Public;
};

struct PromoteInfo
{
	char Name[128];
	int Health;
	int Armor;
	int Regeneration;
	int Level;
	Stacker<char *> *Powerups;
};

class YaRRSettings : public YaRRFunctions
{
public:
	
	class Medals
	{
	public:
		static int Killer_Threshold;
		static int ArmsDestroyer_Threshold;
		static int ArmorDestroyer_Threshold;
		static int Demolition_Threshold;
		static int Sabatage_Threshold;
		static int BeaconSniffer_Threshold;

		static char Killer_Name[256];
		static char ArmsDestroyer_Name[256];
		static char ArmorDestroyer_Name[256];
		static char Demolition_Name[256];
		static char Sabatage_Name[256];
		static char BeaconSniffer_Name[256];
	};

	class Awards
	{
	public:
		static int Killer_Threshold;
		static int ArmsDestroyer_Threshold;
		static int ArmorDestroyer_Threshold;
		static int Demolition_Threshold;
		static int Sabatage_Threshold;
		static int BeaconSniffer_Threshold;
		static int Medals_Threshold;

		static char Killer_Name[256];
		static char ArmsDestroyer_Name[256];
		static char ArmorDestroyer_Name[256];
		static char Demolition_Name[256];
		static char Sabatage_Name[256];
		static char BeaconSniffer_Name[256];
		static char Medals_Name[256];
	};

	static void Load();
	static void Unload();
	static void MapLoad(const char *Map);

	static void LoadGlob();
	static void LoadIRC(const char *ini);
	static char GetChannelType(const char *Channel);
	static bool GetCommandInfo(const char *Command, char *Buffer, int Size, char *Access);
	static bool GetPlayerInGameNick(const char *nick, char *Buffer);
	static void Change(const char *Section, const char *Entry, const char *Data);
	static void UnloadGlob();
	static void LoadSound(Stacker<WavSound> **Stack, const char *Name, INIClass *ini);
	static void LoadSoundSettings(int *Threshold, char *Name, const char *SoundName, const char *Default, const char *Section, INIClass *ini);
	static const char *GetPremotionName(int Level);
	static const PromoteInfo *GetPremotionInfo(int Level);

	static IRCData *IRCd;
	static Stacker<char *> *AnnounceList;
	static Stacker<Moderator> *ModeratorList;
	static Stacker<Vector3> *NodDefences;
	static Stacker<Vector3> *GDIDefences;
	static Stacker<Sound> *Sounds;
	static Stacker<Vector3> *CrateSpawns;
	static Stacker<ObjectWorth> *ObjectWorthList;
	static Stacker<PromoteInfo> *PremotionNames;
	
	static unsigned int ObjectWorthDefault;
	static int AnnounceTimeout;
	static int DefenceChance;
	static float Minimum_BHS;
	static bool NoPending;
	static bool StartButtonFix;
	static char GlobRules[256];
	static char MapRules[256];
	static int Slots;
	static char *SlotList[100];
	static char Website[128];
	static char CrateModel[128];
	static Colour::CRGB MessageColour;
	static Colour::CRGB MessageColourError;
	static char PPageSound[64];
	static bool SuicideAllowed;
	static bool SuicideTakeMoney;
	static char SuicideMessage[256];
	static int MaxVeteranLevel;
	static bool Ban_Rooted;
	static bool Stop_Output;

	static Stacker<WavSound> *Award_Killer_Sound;
	static Stacker<WavSound> *Award_ArmsDestroyer_Sound;
	static Stacker<WavSound> *Award_ArmorDestroyer_Sound;
	static Stacker<WavSound> *Award_Demolition_Sound;
	static Stacker<WavSound> *Award_Sabatage_Sound;
	static Stacker<WavSound> *Award_BeaconSniffer_Sound;
	static Stacker<WavSound> *Award_Medals_Sound;

	static Stacker<WavSound> *Medal_Killer_Sound;
	static Stacker<WavSound> *Medal_ArmsDestroyer_Sound;
	static Stacker<WavSound> *Medal_ArmorDestroyer_Sound;
	static Stacker<WavSound> *Medal_Demolition_Sound;
	static Stacker<WavSound> *Medal_Sabatage_Sound;
	static Stacker<WavSound> *Medal_BeaconSniffer_Sound;
};