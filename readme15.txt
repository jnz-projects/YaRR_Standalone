*************************** Scripts by Andrew "Kamuix" Jones *************************

MSN/Email - Obelisklaser@hotmail.com


============================
Kamuix_Announce_Preset_Buy
============================

This is like JFW_Preset_Buy Exept when bought, It will be announced when purchased. If you do not have enouph money 
to purchase It you will be paged by the console telling you that you do not have enouph credits.
Preset_Name (what preset to buy)
Cost (what to charge)
location (where to spawn it)
Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
DisplayName (what name to display in the announcement)

============================
Kamuix_PAMSG_Zone
============================

This is like JFW_PPAGE_Zone but instead of paging the player it just sends an individual AdminMsg to a player.
Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

============================
Kamuix_Send_FDS_MSG_Zone
============================

On zone entry, this sends a specified message to the console input parser.
Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

============================
Kamuix_Team_Change_Zone
============================

Changes a players team of a team specified on zone entry.
Player_Type (what player type to trigger on, 0 = Nod, 1 = GDI, 2 = any)
Team (what team to change to, 0 = Nod, 1 = GDI)

============================
Kamuix_Damaged_Send_MsgFds
============================

When the object this is attached to gets damaged, this sends a specified message to the console input parser.
Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

============================
Kamuix_Death_Send_MsgFds
============================

When the object this is attached to gets destroyed, this sends a specified message to the console input parser.
Message (what message to send)
Avoid using a comma in the message because the script might choke and fail to send the message correctly

============================
Kamuix_Death_Announce
============================

When the object this is attached to dies, A message gets displayed telling all who destroyed what.
DestroyedName (What the destroyed object gets displayed as)

============================
Kamuix_Death_Team_Win
============================

When the object this is attached to dies, The team specified wins.

============================
Kamuix_Kill_Change
============================

When the spawner this is atatched to does, the player of that spawner will change to the team you specify.

============================
Kamuix_Player_Announce_Zone
============================

When a player enters, a message will a appear with the player name first than a message that you specify.

============================
Kamuix_Kick/Ban_Zone
============================

Simply sents a kick/ban message on the player that enters to the fds.

============================
Zone_Kamuix_Destroy_Object
============================

When entered, it will destroy an object by the ID that you specify. This is good for if you want a zone to only spring once,
just attach this to the zone and have it destroy itself.

============================
Kamuix_Set_Type
============================

this works like M09_Mutant, exept you can choose what team you want an object to be.

============================
Kamuix_Revive_Building_Zone
============================

This will recover a building when entered.

============================
Kamuix_Zone_Set_Health
============================

On entered it will set the object/building health to what you specify.

============================
Kamuix_Rebuild_Structure_Zone
============================

This is like JFW_Preset_Buy but instead it can recover an existing building you have placed. It also
has a bunch of other functions like a pamsg message when you don't have enouph money for it, and a host message when you do.

Location: Location of preset
Cost: Cost of Preset/build
Type: Player type that zone will trigger on
ID: ID of building that you want to recover
Health: Amount of health to gve that building, ussually building health is 500
Preset: Preset to spawn
page: Page you get when you are too low on funds
Popmsg: Pamsg message that you get when you are too low on funds
Announcement: Host message sent when purchased
Teamsound: Plays this sound when purchased

When a building is recovered the building will not work, which is what makes this script more towards the useless side.

============================
Kamuix_Death_Destroy_Building
============================

When whatever this attached to dies, a building specified by id gets destroyed.

============================
Kamuix_Control_Spawner (never tested)
============================

When whatever this is attached to dies, it disabled/anables a spawner, at least it's suppost to.

============================
Kamuix_Zone_Destroy_Building
============================

This will destroy a building when entered.

============================
Kamuix_Set_Model
============================

The object this is atatched to will have the model you specify. When you enter the model name do not put .w3d at the end.