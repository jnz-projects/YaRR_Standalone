

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


#ifdef _YaRRDebug
#define Alloc(Type) YaRRMemory::Allocate<Type>(__LINE__, __FILE__); 
#define CAlloc(Size) YaRRMemory::Allocate2(Size, __LINE__, __FILE__);
#define RAlloc(OldPtr, NewSize) YaRRMemory::ReAllocate(OldPtr, NewSize, __LINE__, __FILE__)
#define Dealloc(Type, Ptr) YaRRMemory::Deallocate<Type>(Ptr, __LINE__, __FILE__)
#define CDealloc(Ptr) YaRRMemory::Deallocate2((void *)Ptr, __LINE__, __FILE__)

class YaRRMemory
{
	static void *Heap;
	
	static FILE *LogFile;
public:
	static bool LogMem;
	static bool LogMem_;
	static void Load();
	static void Unload();

	template <class T> static T *Allocate(int l, const char *s)
	{
		T *tmp = (T *)Allocate2(sizeof(T), l, s);
		new(tmp) T();
		return tmp;
	}
	template <class T> static void Deallocate(T *mem, int l, const char *s)
	{
		mem->~T();
		Deallocate2(mem, l, s);
	}

	static void *Allocate2(size_t Size, int l, const char *s);
	static void *ReAllocate(void *Ptr, size_t newsize, int l, const char *s);
	static void Deallocate2(void *Ptr, int l, const char *s);

	static int Usage(bool PrivHeap);
	static void Dump(bool PrivHeap);
	static void LogMemory(bool log);
};


#else

#define Alloc(Type) YaRRMemory::Allocate<Type>()
#define CAlloc(Size) YaRRMemory::Allocate2(Size)
#define RAlloc(OldPtr, NewSize) YaRRMemory::ReAllocate(OldPtr, NewSize)
#define Dealloc(Type, Ptr) YaRRMemory::Deallocate<Type>(Ptr)
#define CDealloc(Ptr) YaRRMemory::Deallocate2((void *)Ptr)


class YaRRMemory
{
	static void *Heap;
	static bool LogMem;
	 
public:
	
	static void Load();
	static void Unload();

	template <class T> static T *Allocate()
	{
		T *tmp = (T *)Allocate2(sizeof(T));
		new(tmp) T();
		return tmp;
	}
	template <class T> static void Deallocate(T *mem)
	{
		if(mem)
		{
			mem->~T();
			Deallocate2(mem);
		}
	}

	static void *Allocate2(size_t Size);
	static void *ReAllocate(void *Ptr, size_t newsize);
	static void Deallocate2(void *Ptr);

	static int Usage(bool PrivHeap);
	static void Dump(bool PrivHeap);
	static void LogMemory(bool log);
};

#endif