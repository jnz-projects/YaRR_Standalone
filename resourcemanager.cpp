/*	Renegade Scripts.dll
	Resource loader class
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <process.h> /* _beginthread, _endthread */
#include "scripts.h"
#include "shadereng.h"
#include "resourcemanager.h"

ResourceLoaderClass *ResourceFactory;

bool RunLoaderThread;
ResourceLoadTask::ResourceLoadTask()
{
	Data = 0;
}
ResourceLoadTask::ResourceLoadTask(void *data)
{
	Data = data;
}
#ifdef RESOURCEMANGER_USETHREAD
bool ResourceLoadTask::RunResourceLoadTask(void *arg)
{
	if (GetModuleHandle("Pixhelper.dll"))
	{
	 	this->LoadResource();
	}
	else
	{
		DWORD dwWaitResult; 
		dwWaitResult = WaitForSingleObject(arg,250);
		switch (dwWaitResult) 
		{
			case WAIT_OBJECT_0: 
 				this->LoadResource();
				ReleaseMutex(arg);
				break; 
			case WAIT_TIMEOUT: 
				return false; 
				break;
		}
	}
	return true;
}
#else
bool ResourceLoadTask::RunResourceLoadTask(void *arg)
{
 	this->LoadResource();
	return true;
}
#endif

void ResourceLoadTask::LoadResource()
{

}

ResourceLoaderClass::ResourceLoaderClass()
{
#ifdef RESOURCEMANGER_USETHREAD
	LoadTasks = new LoadTaskList();
	RunLoaderThread = true;
	Mutex = CreateMutex(NULL,false,NULL);
	TaskThreadHandle = (HANDLE)_beginthreadex(NULL,0,ResourceLoaderClass::ProcessTasks,this,0,&TaskThreadID);
#endif
}

ResourceLoaderClass::~ResourceLoaderClass()
{
#ifdef RESOURCEMANGER_USETHREAD
	delete LoadTasks;
#endif
}

#ifdef RESOURCEMANGER_USETHREAD
void ResourceLoaderClass::SetPriority(int priority)
{
	SetThreadPriority(TaskThreadHandle,priority);
}
unsigned int WINAPI ResourceLoaderClass::ProcessTasks(void *data)
{

	SetThreadName((unsigned long)-1,"ResourceLoaderThread");
	ResourceLoaderClass *loader = (ResourceLoaderClass *) data;
	LoadTaskListNode *node;
	while (RunLoaderThread)
	{
		node = loader->LoadTasks->Head;
		if (node != NULL)
		{
			node->Data->RunResourceLoadTask(loader->Mutex);
			delete node->Data;
			loader->LoadTasks->RemoveNode(loader->LoadTasks->Head);
			delete node;
			node = 0;
		}
		else 
		{
			Sleep(100);
		}
	}
	_endthreadex(0);
	return 0;
}

void ResourceLoaderClass::EnqueueTask(ResourceLoadTask *task, ResourceLoadPriority priority)
{
	if (GetModuleHandle("Pixhelper.dll"))
	{
		task->RunResourceLoadTask(NULL);
	}
	else
	{
		if (priority == RESOURCELOADPRIORITY_HIGH)
		{
			LoadTasks->PrependNode(new LoadTaskListNode(task));
		}
		else
		{
			LoadTasks->AppendNode(new LoadTaskListNode(task));
		}
	}
}
void ResourceLoaderClass::ProcessTaskImmediate(ResourceLoadTask *task)
{
	task->RunResourceLoadTask(Mutex);
}

#else
void ResourceLoaderClass::EnqueueTask(ResourceLoadTask *task, ResourceLoadPriority priority)
{
	task->RunResourceLoadTask(NULL);
}
void ResourceLoaderClass::ProcessTaskImmediate(ResourceLoadTask *task)
{
	task->RunResourceLoadTask(NULL);
}
#endif

