/*	Renegade Scripts.dll
	Shader Manager Class
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

class ShaderManagerClass {
private:
	SimpleDynVecClass<ProgrammableShaderClass *> *Shaders;
	SimpleDynVecClass<ShaderFactory *> *ShaderFactories;
	unsigned long CurrentCRC32;
	int MapShaderDatabaseOffset;
	bool MapShaderDatabaseLoaded;
public:
	ShaderManagerClass();
	~ShaderManagerClass();

	unsigned long Get_Current_CRC32();
	void Set_Current_Name(const char *name);

	void Load_Database(FileClass *file);
	void Load_Map_Database(FileClass *file);
	void Unload_Database();
	void Unload_Map_Database();
	void Reset_Cache();
	void Release_Resources();
	void Reload_Resources();
	bool Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
	void Register_Shader_Factory(ShaderFactory *loader);
	ProgrammableShaderClass *Load(ChunkLoadClass& cload);
	ID3DXEffectStateManager *StateManager;
};

extern ShaderManagerClass *TheShaderManager;
