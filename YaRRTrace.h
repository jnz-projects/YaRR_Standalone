

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRTrace
{
public:
	static void Startup();
	static void Shutdown();
	static const char *Trace(const char *Host, Stacker<char *> *Result);
};

typedef const char *(*_Trace)(char *, Stacker<char *> *);
typedef void (*_SetFunctions)(void *(*)(int), void *(*)(void *, int));