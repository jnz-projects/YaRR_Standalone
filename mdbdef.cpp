/*	Renegade Scripts.dll
	Base Defense Scripts by WhiteDragon(MDB)
	Copyright 2007 Jonathan Wilson, WhiteDragon(MDB)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "engine.h"
#include "mdbdef.h"

void MDB_Base_Defense_Popup_No_VTOL::Created(GameObject *obj)
{
	State = 1;
	Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,0,false);
	Commands->Enable_Hibernation(obj,false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj,true);
	LastAttack = The_Game()->GameDuration_Seconds;
	LastSeen = 0;
	MaxDis = Get_Float_Parameter("MaxAttackDistance");
	ReturnTime = Get_Int_Parameter("ReturnTime");
	AdjustInf = Get_Int_Parameter("Adjust_Aim_For_Infantry");
	Commands->Start_Timer(obj,this,3.0f,2);
}

void MDB_Base_Defense_Popup_No_VTOL::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 9813199)
	{
		GameObject *o = Get_Vehicle(sender);
		if (o)
		{
			sender = o;
		}
		if (Get_Vehicle_Mode(sender) != FLYING)
		{
			if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(sender)) <= MaxDis)
			{
				if (State == 1)
				{
					State = 2;
					Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,Get_Float_Parameter("LastFrame"),false);
					if (_stricmp(Get_Parameter("Sound"),"0"))
					{
						Commands->Create_Sound(Get_Parameter("Sound"),Commands->Get_Position(obj),obj);
					}
					LastSeen = Commands->Get_ID(sender);
					LastAttack = The_Game()->GameDuration_Seconds;
				}
			}
		}
	}
}

void MDB_Base_Defense_Popup_No_VTOL::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	GameObject *o = Get_Vehicle(seen);
	if (o)
	{
		seen = o;
	}
	if (Get_Vehicle_Mode(seen) != FLYING && State == 3)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(seen)) <= MaxDis)
		{
			ActionParamsStruct params;
			params.Set_Basic(this,100,1);
			if (AdjustInf && Is_Soldier(seen))
			{
				params.Set_Attack_Position(Commands->Get_Bone_Position(seen,"c R Foot"),MaxDis,0.0,true);
				params.Hold_Action = true;
				LastSeen = Commands->Get_ID(seen);
				Commands->Start_Timer(obj,this,1.0f,3);
			}
			else
			{
				params.Set_Attack_Hold(seen,MaxDis,0.0,true,true);
			}
			Commands->Action_Attack(obj,params);
			LastAttack = The_Game()->GameDuration_Seconds;
			Commands->Start_Timer(obj,this,(float)ReturnTime,1);
		}
		else
		{
			Commands->Start_Timer(obj,this,(float)ReturnTime,1);
		}
	}
}

void MDB_Base_Defense_Popup_No_VTOL::Animation_Complete(GameObject *obj,const char *anim)
{
	if (State == 2)
	{
		State = 3;
		Enemy_Seen(obj,Commands->Find_Object(LastSeen));
	}
	else if (State == 4)
	{
		State = 1;
	}
}

void MDB_Base_Defense_Popup_No_VTOL::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	Commands->Action_Reset(obj,100);
}

void MDB_Base_Defense_Popup_No_VTOL::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		if (State == 3)
		{
			if ((int)(The_Game()->GameDuration_Seconds - LastAttack)+1 >= Get_Int_Parameter("ReturnTime"))
			{
				Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,Get_Float_Parameter("LastFrame"),0,false);
				if (_stricmp(Get_Parameter("Sound"),"0"))
				{
					Commands->Create_Sound(Get_Parameter("Sound"),Commands->Get_Position(obj),obj);
				}
				Commands->Action_Reset(obj,100);
				State = 4;
			}
		}
	}
	else if (number == 2)
	{
		char params[40];
		sprintf(params,"9813199,%d",Commands->Get_ID(obj));
		Commands->Attach_Script(Commands->Create_Object(Get_Parameter("Sensor_Preset"),Commands->Get_Bone_Position(obj,Get_Parameter("Sensor_Creation_Bone"))),"MDB_Send_Custom_Enemy_Seen",params);
	}
	else if (number == 3)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(Commands->Find_Object(LastSeen))) > MaxDis)
		{
			Commands->Action_Reset(obj,100);
		}
	}
}

void MDB_Base_Defense_Popup_No_VTOL::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&State);
	Auto_Save_Variable(2,4,&LastSeen);
	Auto_Save_Variable(3,4,&LastAttack);
	Auto_Save_Variable(4,4,&ReturnTime);
	Auto_Save_Variable(5,4,&MaxDis);
	Auto_Save_Variable(6,1,&AdjustInf);
}

void MDB_Base_Defense_Popup_VTOL_Only::Created(GameObject *obj)
{
	State = 1;
	Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,0,false);
	Commands->Enable_Hibernation(obj,false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj,true);
	LastAttack = The_Game()->GameDuration_Seconds;
	LastSeen = 0;
	MaxDis = Get_Float_Parameter("MaxAttackDistance");
	ReturnTime = Get_Int_Parameter("ReturnTime");
	Commands->Start_Timer(obj,this,3.0f,2);
}

void MDB_Base_Defense_Popup_VTOL_Only::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 9813199)
	{
		GameObject *o = Get_Vehicle(sender);
		if (o)
		{
			sender = o;
		}
		if (Get_Vehicle_Mode(sender) == FLYING)
		{
			if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(sender)) <= MaxDis)
			{
				if (State == 1)
				{
					State = 2;
					Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,Get_Float_Parameter("LastFrame"),false);
					if (_stricmp(Get_Parameter("Sound"),"0"))
					{
						Commands->Create_Sound(Get_Parameter("Sound"),Commands->Get_Position(obj),obj);
					}
					LastSeen = Commands->Get_ID(sender);
					LastAttack = The_Game()->GameDuration_Seconds;
				}
			}
		}
	}
}

void MDB_Base_Defense_Popup_VTOL_Only::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	GameObject *o = Get_Vehicle(seen);
	if (o)
	{
		seen = o;
	}
	if (Get_Vehicle_Mode(seen) == FLYING && State == 3)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(seen)) <= MaxDis)
		{
			ActionParamsStruct params;
			params.Set_Basic(this,100,1);
			params.Set_Attack_Hold(seen,MaxDis,0.0,true,true);
			Commands->Action_Attack(obj,params);
			LastAttack = The_Game()->GameDuration_Seconds;
			Commands->Start_Timer(obj,this,(float)ReturnTime,1);
		}
		else
		{
			Commands->Start_Timer(obj,this,(float)ReturnTime,1);
		}
	}
}

void MDB_Base_Defense_Popup_VTOL_Only::Animation_Complete(GameObject *obj,const char *anim)
{
	if (State == 2)
	{
		State = 3;
		Enemy_Seen(obj,Commands->Find_Object(LastSeen));
	}
	else if (State == 4)
	{
		State = 1;
	}
}

void MDB_Base_Defense_Popup_VTOL_Only::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	Commands->Action_Reset(obj,100);
}

void MDB_Base_Defense_Popup_VTOL_Only::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		if (State == 3)
		{
			if ((int)(The_Game()->GameDuration_Seconds - LastAttack)+1 >= Get_Int_Parameter("ReturnTime"))
			{
				Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,Get_Float_Parameter("LastFrame"),0,false);
				if (_stricmp(Get_Parameter("Sound"),"0"))
				{
					Commands->Create_Sound(Get_Parameter("Sound"),Commands->Get_Position(obj),obj);
				}
				Commands->Action_Reset(obj,100);
				State = 4;
			}
		}
	}
	else if (number == 2)
	{
		char params[40];
		sprintf(params,"9813199,%d",Commands->Get_ID(obj));
		Commands->Attach_Script(Commands->Create_Object(Get_Parameter("Sensor_Preset"),Commands->Get_Bone_Position(obj,Get_Parameter("Sensor_Creation_Bone"))),"MDB_Send_Custom_Enemy_Seen",params);
	}
}

void MDB_Base_Defense_Popup_VTOL_Only::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&State);
	Auto_Save_Variable(2,4,&LastSeen);
	Auto_Save_Variable(3,4,&LastAttack);
	Auto_Save_Variable(4,4,&ReturnTime);
	Auto_Save_Variable(5,4,&MaxDis);
}

void MDB_Base_Defense_Popup::Created(GameObject *obj)
{
	State = 1;
	Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,0,false);
	Commands->Enable_Hibernation(obj,false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj,true);
	LastAttack = The_Game()->GameDuration_Seconds;
	LastSeen = 0;
	MaxDis = Get_Float_Parameter("MaxAttackDistance");
	ReturnTime = Get_Int_Parameter("ReturnTime");
	AdjustInf = Get_Int_Parameter("Adjust_Aim_For_Infantry");
	Commands->Start_Timer(obj,this,3.0f,2);
}

void MDB_Base_Defense_Popup::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == 9813199)
	{
		GameObject *o = Get_Vehicle(sender);
		if (o)
		{
			sender = o;
		}
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(sender)) <= MaxDis)
		{
			if (State == 1)
			{
				State = 2;
				Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,Get_Float_Parameter("LastFrame"),false);
				if (_stricmp(Get_Parameter("Sound"),"0"))
				{
					Commands->Create_Sound(Get_Parameter("Sound"),Commands->Get_Position(obj),obj);
				}
				LastSeen = Commands->Get_ID(sender);
				LastAttack = The_Game()->GameDuration_Seconds;
			}
		}
	}
}

void MDB_Base_Defense_Popup::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	GameObject *o = Get_Vehicle(seen);
	if (o)
	{
		seen = o;
	}
	if (State == 3)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(seen)) <= MaxDis)
		{
			ActionParamsStruct params;
			params.Set_Basic(this,100,1);
			if (AdjustInf && Is_Soldier(seen))
			{
				params.Set_Attack_Position(Commands->Get_Bone_Position(seen,"c R Foot"),MaxDis,0.0,true);
				params.Hold_Action = true;
				LastSeen = Commands->Get_ID(seen);
				Commands->Start_Timer(obj,this,1.0f,3);
			}
			else
			{
				params.Set_Attack_Hold(seen,MaxDis,0.0,true,true);
			}
			Commands->Action_Attack(obj,params);
			LastAttack = The_Game()->GameDuration_Seconds;
			Commands->Start_Timer(obj,this,(float)ReturnTime,1);
		}
		else
		{
			Commands->Start_Timer(obj,this,(float)ReturnTime,1);
		}
	}
}

void MDB_Base_Defense_Popup::Animation_Complete(GameObject *obj,const char *anim)
{
	if (State == 2)
	{
		State = 3;
		Enemy_Seen(obj,Commands->Find_Object(LastSeen));
	}
	else if (State == 4)
	{
		State = 1;
	}
}

void MDB_Base_Defense_Popup::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	Commands->Action_Reset(obj,100);
}

void MDB_Base_Defense_Popup::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		if (State == 3)
		{
			if ((int)(The_Game()->GameDuration_Seconds - LastAttack)+1 >= Get_Int_Parameter("ReturnTime"))
			{
				Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,Get_Float_Parameter("LastFrame"),0,false);
				if (_stricmp(Get_Parameter("Sound"),"0"))
				{
					Commands->Create_Sound(Get_Parameter("Sound"),Commands->Get_Position(obj),obj);
				}
				Commands->Action_Reset(obj,100);
				State = 4;
			}
		}
	}
	else if (number == 2)
	{
		char params[40];
		sprintf(params,"9813199,%d",Commands->Get_ID(obj));
		Commands->Attach_Script(Commands->Create_Object(Get_Parameter("Sensor_Preset"),Commands->Get_Bone_Position(obj,Get_Parameter("Sensor_Creation_Bone"))),"MDB_Send_Custom_Enemy_Seen",params);
	}
	else if (number == 3)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(Commands->Find_Object(LastSeen))) > MaxDis)
		{
			Commands->Action_Reset(obj,100);
		}
	}
}

void MDB_Base_Defense_Popup::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&State);
	Auto_Save_Variable(2,4,&LastSeen);
	Auto_Save_Variable(3,4,&LastAttack);
	Auto_Save_Variable(4,4,&ReturnTime);
	Auto_Save_Variable(5,4,&MaxDis);
	Auto_Save_Variable(6,1,&AdjustInf);
}

void MDB_Base_Defense::Created(GameObject *obj)
{
	Commands->Enable_Hibernation(obj,false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj,true);
	Vector3 pos = Commands->Get_Position(obj);
	V[0].X = pos.X - 10;
	V[0].Y = pos.Y - 10;
	V[0].Z = pos.Z + 2;
	V[1].X = pos.X + 10;
	V[1].Y = pos.Y;
	V[1].Z = pos.Z + 2;
	V[2].X = pos.X + 10;
	V[2].Y = pos.Y - 10;
	V[2].Z = pos.Z + 2;
	V[3].X = pos.X;
	V[3].Y = pos.Y + 10;
	V[3].Z = pos.Z + 4;
	ActionParamsStruct var;
	var.Set_Basic(this,1,2);
	var.Set_Attack_Position(V[Commands->Get_Random_Int(0,4)],0.0,0.0,true);
	MaxDis = Get_Float_Parameter("MaxAttackDistance");
	LastSeen = 0;
	AdjustInf = Get_Int_Parameter("Adjust_Aim_For_Infantry");
	Commands->Start_Timer(obj,this,Commands->Get_Random(5.0f,15.0f),1);
}

void MDB_Base_Defense::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	GameObject *o = Get_Vehicle(seen);
	if (o)
	{
		seen = o;
	}
	ActionParamsStruct params;
	params.Set_Basic(this,100,1);
	if (AdjustInf && Is_Soldier(seen))
	{
		params.Set_Attack_Position(Commands->Get_Bone_Position(seen,"c R Foot"),MaxDis,0.0,true);
		params.Hold_Action = true;
		LastSeen = Commands->Get_ID(seen);
		Commands->Start_Timer(obj,this,1.0f,3);
	}
	else
	{
		params.Set_Attack_Hold(seen,MaxDis,0.0,true,true);
	}
	Commands->Action_Attack(obj,params);
}

void MDB_Base_Defense::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	if (action == 1)
	{
		Commands->Action_Reset(obj,100);
	}
}

void MDB_Base_Defense::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		ActionParamsStruct var;
		var.Set_Basic(this,1,2);
		var.Set_Attack_Position(V[Commands->Get_Random_Int(0,4)],0.0,0.0,true);
		Commands->Start_Timer(obj,this,Commands->Get_Random(5.0f,15.0f),1);
	}
	else if (number == 2)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(Commands->Find_Object(LastSeen))) > MaxDis)
		{
			Commands->Action_Reset(obj,100);
		}
	}
}

void MDB_Base_Defense::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&MaxDis);
	Auto_Save_Variable(2,1,&AdjustInf);
	Auto_Save_Variable(3,4,&LastSeen);
	Auto_Save_Variable(4,4,&V[0].X);
	Auto_Save_Variable(5,4,&V[0].Y);
	Auto_Save_Variable(6,4,&V[0].Z);
	Auto_Save_Variable(7,4,&V[1].X);
	Auto_Save_Variable(8,4,&V[1].Y);
	Auto_Save_Variable(9,4,&V[1].Z);
	Auto_Save_Variable(10,4,&V[2].X);
	Auto_Save_Variable(11,4,&V[2].Y);
	Auto_Save_Variable(12,4,&V[2].Z);
	Auto_Save_Variable(13,4,&V[3].X);
	Auto_Save_Variable(14,4,&V[3].Y);
	Auto_Save_Variable(15,4,&V[3].Z);
}

void MDB_Base_Defense_No_VTOL::Created(GameObject *obj)
{
	Commands->Enable_Hibernation(obj,false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj,true);
	Vector3 pos = Commands->Get_Position(obj);
	V[0].X = pos.X - 10;
	V[0].Y = pos.Y - 10;
	V[0].Z = pos.Z + 2;
	V[1].X = pos.X + 10;
	V[1].Y = pos.Y;
	V[1].Z = pos.Z + 2;
	V[2].X = pos.X + 10;
	V[2].Y = pos.Y - 10;
	V[2].Z = pos.Z + 2;
	V[3].X = pos.X;
	V[3].Y = pos.Y + 10;
	V[3].Z = pos.Z + 4;
	ActionParamsStruct var;
	var.Set_Basic(this,1,2);
	var.Set_Attack_Position(V[Commands->Get_Random_Int(0,4)],0.0,0.0,true);
	MaxDis = Get_Float_Parameter("MaxAttackDistance");
	LastSeen = 0;
	AdjustInf = Get_Int_Parameter("Adjust_Aim_For_Infantry");
	Commands->Start_Timer(obj,this,Commands->Get_Random(5.0f,15.0f),1);
}

void MDB_Base_Defense_No_VTOL::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	GameObject *o = Get_Vehicle(seen);
	if (o)
	{
		seen = o;
	}
	if (Get_Vehicle_Mode(seen) != FLYING)
	{
		ActionParamsStruct params;
		params.Set_Basic(this,100,1);
		if (AdjustInf && Is_Soldier(seen))
		{
			params.Set_Attack_Position(Commands->Get_Bone_Position(seen,"c R Foot"),MaxDis,0.0,true);
			params.Hold_Action = true;
			LastSeen = Commands->Get_ID(seen);
			Commands->Start_Timer(obj,this,1.0f,3);
		}
		else
		{
			params.Set_Attack_Hold(seen,MaxDis,0.0,true,true);
		}
		Commands->Action_Attack(obj,params);
	}
}

void MDB_Base_Defense_No_VTOL::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	if (action == 1)
	{
		Commands->Action_Reset(obj,100);
	}
}

void MDB_Base_Defense_No_VTOL::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		ActionParamsStruct var;
		var.Set_Basic(this,1,2);
		var.Set_Attack_Position(V[Commands->Get_Random_Int(0,4)],0.0,0.0,true);
		Commands->Start_Timer(obj,this,Commands->Get_Random(5.0f,15.0f),1);
	}
	else if (number == 2)
	{
		if (Commands->Get_Distance(Commands->Get_Position(obj),Commands->Get_Position(Commands->Find_Object(LastSeen))) > MaxDis)
		{
			Commands->Action_Reset(obj,100);
		}
	}
}

void MDB_Base_Defense_No_VTOL::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&MaxDis);
	Auto_Save_Variable(2,1,&AdjustInf);
	Auto_Save_Variable(3,4,&LastSeen);
	Auto_Save_Variable(4,4,&V[0].X);
	Auto_Save_Variable(5,4,&V[0].Y);
	Auto_Save_Variable(6,4,&V[0].Z);
	Auto_Save_Variable(7,4,&V[1].X);
	Auto_Save_Variable(8,4,&V[1].Y);
	Auto_Save_Variable(9,4,&V[1].Z);
	Auto_Save_Variable(10,4,&V[2].X);
	Auto_Save_Variable(11,4,&V[2].Y);
	Auto_Save_Variable(12,4,&V[2].Z);
	Auto_Save_Variable(13,4,&V[3].X);
	Auto_Save_Variable(14,4,&V[3].Y);
	Auto_Save_Variable(15,4,&V[3].Z);
}

void MDB_Base_Defense_VTOL_Only::Created(GameObject *obj)
{
	Commands->Enable_Hibernation(obj,false);
	Commands->Innate_Enable(obj);
	Commands->Enable_Enemy_Seen(obj,true);
	Vector3 pos = Commands->Get_Position(obj);
	V[0].X = pos.X - 10;
	V[0].Y = pos.Y - 10;
	V[0].Z = pos.Z + 2;
	V[1].X = pos.X + 10;
	V[1].Y = pos.Y;
	V[1].Z = pos.Z + 2;
	V[2].X = pos.X + 10;
	V[2].Y = pos.Y - 10;
	V[2].Z = pos.Z + 2;
	V[3].X = pos.X;
	V[3].Y = pos.Y + 10;
	V[3].Z = pos.Z + 4;
	ActionParamsStruct var;
	var.Set_Basic(this,1,2);
	var.Set_Attack_Position(V[Commands->Get_Random_Int(0,4)],0.0,0.0,true);
	MaxDis = Get_Float_Parameter("MaxAttackDistance");
	Commands->Start_Timer(obj,this,Commands->Get_Random(5.0f,15.0f),1);
}

void MDB_Base_Defense_VTOL_Only::Enemy_Seen(GameObject *obj,GameObject *seen)
{
	GameObject *o = Get_Vehicle(seen);
	if (o)
	{
		seen = o;
	}
	if (Get_Vehicle_Mode(seen) == FLYING)
	{
		ActionParamsStruct params;
		params.Set_Basic(this,100,1);
		params.Set_Attack_Hold(seen,MaxDis,0.0,true,true);
		Commands->Action_Attack(obj,params);
	}
}

void MDB_Base_Defense_VTOL_Only::Action_Complete(GameObject *obj,int action,ActionCompleteReason reason)
{
	if (action == 1)
	{
		Commands->Action_Reset(obj,100);
	}
}

void MDB_Base_Defense_VTOL_Only::Timer_Expired(GameObject *obj,int number)
{
	if (number == 1)
	{
		ActionParamsStruct var;
		var.Set_Basic(this,1,2);
		var.Set_Attack_Position(V[Commands->Get_Random_Int(0,4)],0.0,0.0,true);
		Commands->Start_Timer(obj,this,Commands->Get_Random(5.0f,15.0f),1);
	}
}

void MDB_Base_Defense_VTOL_Only::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&MaxDis);
	Auto_Save_Variable(2,4,&V[0].X);
	Auto_Save_Variable(3,4,&V[0].Y);
	Auto_Save_Variable(4,4,&V[0].Z);
	Auto_Save_Variable(5,4,&V[1].X);
	Auto_Save_Variable(6,4,&V[1].Y);
	Auto_Save_Variable(7,4,&V[1].Z);
	Auto_Save_Variable(8,4,&V[2].X);
	Auto_Save_Variable(9,4,&V[2].Y);
	Auto_Save_Variable(10,4,&V[2].Z);
	Auto_Save_Variable(11,4,&V[3].X);
	Auto_Save_Variable(12,4,&V[3].Y);
	Auto_Save_Variable(13,4,&V[3].Z);
}

ScriptRegistrant<MDB_Base_Defense_Popup_No_VTOL> MDB_Base_Defense_Popup_No_VTOL_Registrant("MDB_Base_Defense_Popup_No_VTOL","MaxAttackDistance=10.0:float,ReturnTime=10:int,Animation=0:string,LastFrame=-1.0:float,Sensor_Preset=0:string,Sensor_Creation_Bone=0:string,Sound=0:string,Adjust_Aim_For_Infantry=1:int");
ScriptRegistrant<MDB_Base_Defense_Popup_VTOL_Only> MDB_Base_Defense_Popup_VTOL_Only_Registrant("MDB_Base_Defense_Popup_VTOL_Only","MaxAttackDistance=10.0:float,ReturnTime=10:int,Animation=0:string,LastFrame=-1.0:float,Sensor_Preset=0:string,Sensor_Creation_Bone=0:string,Sound=0:string");
ScriptRegistrant<MDB_Base_Defense_Popup> MDB_Base_Defense_Popup_Registrant("MDB_Base_Defense_Popup","MaxAttackDistance=10.0:float,ReturnTime=10:int,Animation=0:string,LastFrame=-1.0:float,Sensor_Preset=0:string,Sensor_Creation_Bone=0:string,Sound=0:string,Adjust_Aim_For_Infantry=1:int");
ScriptRegistrant<MDB_Base_Defense> MDB_Base_Defense_Registrant("MDB_Base_Defense","MaxAttackDistance=10.0:float,Adjust_Aim_For_Infantry=1:int");
ScriptRegistrant<MDB_Base_Defense_No_VTOL> MDB_Base_Defense_No_VTOL_Registrant("MDB_Base_Defense_No_VTOL","MaxAttackDistance=10.0:float,Adjust_Aim_For_Infantry=1:int");
ScriptRegistrant<MDB_Base_Defense_VTOL_Only> MDB_Base_Defense_VTOL_Only_Registrant("MDB_Base_Defense_VTOL_Only","MaxAttackDistance=10.0:float,Adjust_Aim_For_Infantry=1:int");
