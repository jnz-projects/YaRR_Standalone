

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/



#define RegisterCommand(Class, Use, Name, registrant) Class *registrant = Alloc(Class); registrant->Startup(Name, Use)

#define IRCArgs char *arstr =  strdup2(YaRRStrings::Fetch(IRC_ARGS)); IRC::Send("PRIVMSG %s :%s\n", Channel, Replace(arstr, ":command:", (*Tokens)[0])); CDealloc(arstr); return
#define Args char *arstr =  strdup2(YaRRStrings::Fetch(GAME_ARGS)); PPageErr(ID, "%s", Replace(arstr, ":command:", (*Tokens)[0])); CDealloc(arstr); return
#define IRCPNOT char *arstr =  strdup2(YaRRStrings::Fetch(IRC_PNOTFIND)); IRC::Send("PRIVMSG %s :%s\n", Channel, Replace(arstr, ":command:", (*Tokens)[0])); CDealloc(arstr); return
#define IRCPMANY char *arstr =  strdup2(YaRRStrings::Fetch(IRC_PFMANY)); IRC::Send("PRIVMSG %s :%s\n", Channel, Replace(arstr, ":command:", (*Tokens)[0])); CDealloc(arstr); return
#define PNOT char *arstr =  strdup2(YaRRStrings::Fetch(GAME_PNOTFIND)); PPageErr(ID, "%s", Replace(arstr, ":command:", (*Tokens)[0])); CDealloc(arstr); return
#define PMANY char *arstr =  strdup2(YaRRStrings::Fetch(GAME_PFMANY)); PPageErr(ID, "%s", Replace(arstr, ":command:", (*Tokens)[0])); CDealloc(arstr); return

#define FLAG_IRC 2
#define FLAG_GAME 1

#define Find_Check_Arguments(s, n, a) YaRRCommands::Find_Output(s, Tokens, n, a); if(!p) return;  


class ChatCommand;

class ChatCommand : public YaRRLink
{
public:
	int Use;
	Stacker<char *> Triggers;
	char Access;
	char Name[256];
	void Startup(const char *_Name, int _Use);
	virtual void Activate(int ID, int Type, Stacker<char *> *Tokens);
	virtual void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};


class YaRRCommands : public YaRRLink
{
	
public:
	static void Startup();
	static void Shutdown();

	static Stacker<ChatCommand *> *CommandList;
	static void IRCCallback(ircdata *irc, const char *Packet);
	static bool __stdcall GameChat(int ID, int Type, WideStringClass &_Message, int Target);
	static void IRCChat(ircdata *irc, User *user, const char *Channel, const char *Message);

	static void Add(ChatCommand *Command);

	static Player *YaRRCommands::Find_Output(const char *Channel, Stacker<char *> *Tokens, int Number, int Arguments);
	static Player *YaRRCommands::Find_Output(int ID, Stacker<char *> *Tokens, int Number, int Arguments);
};

class Debug : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Message : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Gameinfo : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Ban : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Help : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Auth : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Save : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class Setjoin : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class FDS : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Boot : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Unban : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Logsearch : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Edit : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class ShowMods : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class ListPlayers : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Mute : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Killme : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class Kill : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Block : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Unblock : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Version : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Gameover : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Restart : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Quit : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Allow : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Buildings : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Vehicles : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class ForceTeamChange : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Bandwidth : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class VehicleLimit : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class MineLimit : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class AdminMessage : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Spectate : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class PlayerAdminMessage : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Eject : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Stats : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Rotation : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Nextmap : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Refund : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class DestroyVehicle : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class DeleteTempModerator : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class AddTempModerator : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class PlayerInfo : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class ModList : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Rules : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Shun : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Unshun : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class SetNextMap : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Banlog : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Donate : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class SellVehicle : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class Rehash : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Raw : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Stop : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Start : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Zero : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Server : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Website_ : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Page : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Viewjoin : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Time : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Eteam : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Ping : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Tdonate : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class SetColour : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class SetSound : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class Disarm : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class RequestTeamChange : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
};

class NodeBan : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class FindNodeBan : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class DeleteNodeBan : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class SetGlobal : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class GetGlobal : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Rank : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class RankIgnore : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class RankAllow : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class SQL : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class RankDetail : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Log : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Root : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Protect : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class _PlaySound : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Calculate : public ChatCommand
{
public:
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class Say : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};

class LockDown : public ChatCommand
{
public:
	void Activate(int ID, int Type, Stacker<char *> *Tokens);
	void ActivateIRC(ircdata *irc, User *user, const char *Channel, Stacker<char *> *Tokens);
};