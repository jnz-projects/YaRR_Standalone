/*	Renegade Scripts.dll
	Scripts for RenAlert
	Copyright 2007 Joshua "NeoSaber" Kraft, Tom "Dante" Anderson, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

class RA_Repair_Controller : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class RA_Repair_Script : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
	void Timer_Expired(GameObject *obj,int number);
};

class RA_Infantry_Spy : public ScriptImpClass {
	void Created(GameObject *obj);
};

class RA_Infantry_NotSpy : public ScriptImpClass {
	void Created(GameObject *obj);
};

class RA_Helipad_Zone : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
	void Timer_Expired(GameObject *obj,int number);
};

class RA_MineLayer : public ScriptImpClass {
	int driverID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int mesage,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Register_Auto_Save_Variables();
};

class RA_Mine : public ScriptImpClass {
	bool AP_mine;
	bool AT_mine;
	int ownerID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int mesage,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Destroyed(GameObject *obj);
	void Register_Auto_Save_Variables();
};

class RA_Mine_Manager : public ScriptImpClass {
	int all_mines;
	int sov_mines;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class RA_ObjectDeath_OnCreate : public ScriptImpClass {
	void Created(GameObject *obj);
};

class RA_Naval_Yard : public ScriptImpClass {
	int team_count;
	bool active;
	bool ready;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class RA_Naval_PT_Pokable : public ScriptImpClass {
	int storedID;
	void Created(GameObject *obj);
	void Poked(GameObject *obj,GameObject *poker);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class RA_Naval_Unit : public ScriptImpClass {
	void Destroyed(GameObject *obj);
};

class RA_Naval_Zone : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class RA_Damaged_Credits : public ScriptImpClass {
	void Damaged(GameObject *obj,GameObject *damager,float damage);
};

class RA_GameStart_Detector : public ScriptImpClass {
	void Created(GameObject *obj);
	void Killed (GameObject *obj,GameObject *shooter);
	void Timer_Expired (GameObject *obj,int number);
};

class RA_DriverDeath : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class RA_Conyard_Repair : public ScriptImpClass {
	bool Enabled;
	int objID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int timer);
	void Register_Auto_Save_Variables();
};

class RA_ObjectDeath_OnDeath : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class RA_Demolition_Truck_Improved :public ScriptImpClass {
	int driverID;
	bool triggered;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class RA_CTF_Zone:public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int timer);
	void Created(GameObject *obj);
	void Register_Auto_Save_Variables();
	int NumCaptured;
	bool GotFlag;
	unsigned int DecorFlag;
	unsigned int FlagId;
	unsigned int PlayerId;
	bool DecorFlagPresent;
};

class RA_MAD_Tank_Improved : public ScriptImpClass {
	int driverID;
	bool deployed;
	int newobjID;
	int ownerID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Animation_Complete(GameObject *obj,const char *anim);
	void Destroyed(GameObject *obj);
	void Register_Auto_Save_Variables();
};

class RA_Conyard_Controller_Improved : public ScriptImpClass {
	void Created(GameObject *obj);
	void Killed(GameObject *obj,GameObject *shooter);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class RA_Building_DeathSound : public ScriptImpClass {
	bool halfdead;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj, GameObject *damager, float damage);
	void Killed(GameObject *obj, GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class RA_Base_Defense_Simple : public ScriptImpClass {
private:
	unsigned long targetID;
	bool attacking;
	int current_priority;
	float attack_timer;
	int loop_count;
	void Attack(GameObject *obj, GameObject *target); 
	GameObject* Select_Target(GameObject *obj, GameObject *target);
	GameObject* Set_Target(GameObject *target);
	GameObject* Tie_Breaker(GameObject *obj, GameObject *new_target, GameObject *old_target);
	bool Adjust_For_Infantry(GameObject *target);
public:
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj, GameObject *seen);
	void Damaged(GameObject *obj, GameObject *damager, float damage);
	void Timer_Expired(GameObject *obj, int number);
	void Action_Complete(GameObject *obj, int action, ActionCompleteReason reason);
	void Register_Auto_Save_Variables();
};

class RA_Base_Defense_Powered : public ScriptImpClass {
private:
	unsigned long targetID;
	bool attacking;
	int current_priority;
	float attack_timer;
	int loop_count;
	void Attack(GameObject *obj, GameObject *target); 
	GameObject* Select_Target(GameObject *obj, GameObject *target);
	GameObject* Set_Target(GameObject *target);
	GameObject* Tie_Breaker(GameObject *obj, GameObject *new_target, GameObject *old_target);
	bool Adjust_For_Infantry(GameObject *target);
public:
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj, GameObject *seen);
	void Damaged(GameObject *obj, GameObject *damager, float damage);
	void Timer_Expired(GameObject *obj, int number);
	void Action_Complete(GameObject *obj, int action, ActionCompleteReason reason);
	void Register_Auto_Save_Variables();
};

class RA_Vehicle_Regen : public ScriptImpClass {
	bool healing;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj, GameObject *damager, float damage);
	void Timer_Expired(GameObject *obj, int number);
	void Register_Auto_Save_Variables();
};

class RA_Thief : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class RA_Thief_Improved : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class RA_Credit_Theft_Zone : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
};

class RA_DestroyNearest_OnDeath : public ScriptImpClass {
	void Killed(GameObject *obj, GameObject *shooter);
};

class RA_Ore_Truck : public ScriptImpClass {
	int scoops;
	float value;
	bool harvesting;
	int field;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Animation_Complete(GameObject *obj,const char *anim);
	void Timer_Expired(GameObject *obj, int number);
	void Register_Auto_Save_Variables();
};

class RA_Ore_Field : public ScriptImpClass { 
	void Entered(GameObject *obj,GameObject *enter);
	void Exited(GameObject *obj,GameObject *exit);
};
	
class RA_Ore_Delivery_Zone : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
};

class RA_Vehicle_Team_Set : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class RA_Vehicle_Team_Timer : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
};

class RA_Visible_Driver : public ScriptImpClass {
	unsigned long modelID;
	unsigned long driverID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj, int number);
	void Killed(GameObject *obj,GameObject *shooter);
	void Destroyed(GameObject *obj);
	void Register_Auto_Save_Variables();
};

class RA_Vision_Control : public ScriptImpClass {
	bool allowed;
	bool underwater;
	bool driving;
	bool blackout;
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Register_Auto_Save_Variables();
};

class RA_Fog_Level_Settings : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class RA_Missile_Beacon : public ScriptImpClass {
	void Created(GameObject *obj); //send message to team's RA_Missile_Control
};

class RA_Beacon_Terminal : public ScriptImpClass {
	bool active; //true if beacon is available
	bool reset; //true when spy pokes terminal
	void Created(GameObject *obj); //Add Beacon_Timer
	void Custom(GameObject *obj, int message, int param, GameObject *sender); 
	void Poked(GameObject *obj, GameObject *poker); //check if poker is friend or enemy spy
	void Timer_Expired(GameObject *obj, int number); //Slight time delay after spy disable, before timer script re-added
	void Register_Auto_Save_Variables();
};

class RA_Beacon_Timer : public ScriptImpClass { 
	//acts as timer for Beacon_Terminal
	//This script will be removed & replaced if a spy pokes the terminal
	//This should reset the timer
	void Created(GameObject *obj); //Start Timer
	void Timer_Expired(GameObject *obj, int number); //Send message to Terminal and remove self
};

class RA_Missile_Control : public ScriptImpClass {
	bool launching; //true when animating
	bool alive; //false when building dies
	void Created(GameObject *obj); //initialize variables
	void Custom(GameObject *obj, int message, int param, GameObject *sender); 
	void Animation_Complete(GameObject *obj,const char *anim);
	void Timer_Expired(GameObject *obj, int number); //Time delay to kill beacons
	void Register_Auto_Save_Variables();
};

class RA_Missile_Controller : public ScriptImpClass {
	void Killed(GameObject *obj, GameObject *shooter); //inform Missile_Control and Beacon_Terminal
};

class RA_Demolition_Truck_Retarded :public ScriptImpClass {
	int driverID;
	bool triggered;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class RA_MAD_Tank_Devolved : public ScriptImpClass {
	int driverID;
	bool deployed;
	int newobjID;
	int ownerID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Animation_Complete(GameObject *obj,const char *anim);
	void Destroyed(GameObject *obj);
	void Register_Auto_Save_Variables();
};
