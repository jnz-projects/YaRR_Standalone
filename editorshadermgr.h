/*	Renegade Scripts.dll
	Shader Database Editor Shader Manager Class
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class EditorShaderManagerClass {
public:
	SimpleDynVecClass<EditorShaderClass *> *EditorShaders;
	SimpleDynVecClass<EditorShaderFactory *> *EditorShaderFactories;
	EditorShaderManagerClass();
	~EditorShaderManagerClass();
	void Unload_Database();
	void Load_Database(FileClass *file);
	void Save_Database(FileClass *file);
	void Register_Editor_Shader_Factory(EditorShaderFactory *loader);
	EditorShaderClass *Load(ChunkLoadClass& cload);
};

extern EditorShaderManagerClass *TheEditorShaderManager;
