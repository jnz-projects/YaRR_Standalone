; ************************* Scripts by Matt Bailey "Whitedragon" *************************

 - whitedragonbc@gmail.com


====================================================================================================

; ************************* [Script Name] MDB_Change_Spawn_Char_Custom

====================================================================================================

 [Description]

 - Changes the spawner character for the specified team when a custom is recieved.
   The team to change the spawn character of is the custom message param.

 [Parameters]

 - Message: Custom message number.

 - Character: New spawn character preset.

====================================================================================================

; ************************* [Script Name] MDB_Change_Spawn_Char_Timer

====================================================================================================

 [Description]

 - Changes the spawner character for the specified team x seconds after the script is created.

 [Parameters]

 - Time: How long after the script is created to change the spawn character.

 - Character: New spawn character preset.

 - Team: Team whos spawn character is to be changed.

====================================================================================================

; ************************* [Script Name] MDB_ConYard

====================================================================================================

 [Description]

 - Construction Yard repair script, repairs buildings on the team of the object it's attached to.
   Requires no scripts on the other buildings or per map parameters.

 [Parameters]

 - Interval: How often to repair.

 - Heal_Amount: Amount to repair.

 - Power_Mode: What to do when the conyard loses power.
   0 = Repairs completely stop.
   1 = Repairs remain the same.
   2 = Changes Heal_Amount, Heal_Amount is multiplied by Power_Amount. Used to reduce the amount repaired by.
   3 = Changes Interval, Interval is multiplied by Power_Amount. Used to increase the time between repairs.

 - Power_Amount: Amount to multiply Heal_Amount or Interval by, depending on what Power_Mode is set to.
   For example: If Power_Mode is set to 2 you could set Power_Amount to 0.5, which would multiply Heal_Amount by 0.5, cutting repairs in half.
   Another example: If Power_Mode is set to 3 you could set Power_Amount to 2, which would multiply Interval by 2, doubling time between repairs.

 - Repair_Self: Should the conyard repair itself. 0 = no, 1 = yes.

====================================================================================================

; ************************* [Script Name] MDB_Weapon_Scope

====================================================================================================

 [Description]

 - Script to give a different scope for each weapon
   When using, should put this on every infantry unit in your mod

 [Parameters]

 - Default_Scope: Scope to use when the current weapon has no entry in the weaponscope.cfg file

  At startup, a file called weaponscope.cfg is opened. If the file exists in the data folder, it will be read, otherwise it will be read from the
  mix files of your mod. If no weaponscope.cfg file exists, nothing happens. If the file exists, it should contan lines like the following:
  Weapon_AutoRifle_Player=0.
  The first entry is the weapon the scope belongs to.
  The second entry is the scope to use for that weapon.
  Pass -1 to say "I want this weapon to have no custom scope" (either because it has no scope or because it has the normal renegade scope).
  Do not put JFW_Scope on any infantry unit which has MDB_Weapon_Scope on it.
  If you wish the weapon to use the Default_Scope setting, put no entry (or you can put -2 if you want to be explicit)
  Then, on a timer, the code checks the scope assigned to the current weapon. If it doesnt match what the current scope for that player is, a
  new scope is set.

====================================================================================================

; ************************* [Script Name] MDB_Weapon_Scope_Global

====================================================================================================

 [Description]

 - Attaches MDB_Weapon_Scope to every player when they spawn.
   Attach to a Dave's Arrow and put it on every map in your mod.

 [Parameters]

 - Default_Scope: See MDB_Weapon_Scope.

====================================================================================================

; ************************* [Script Name] MDB_Base_Defense

====================================================================================================

 [Description]

 - Basic base defense script that will fire at any unit of the opposite team.

 [Parameters]

 - MaxAttackDistance: Maximum range of the weapon this base defense uses.

 - Adjust_Aim_For_Infantry: Normally base defense aim at the head of infantry. With some types of weapons, like shells or lasers, this greatly
   decreases their accuracy. If this is set to 1 the base defense will instead aim at the feet of the infantry.

====================================================================================================

; ************************* [Script Name] MDB_Base_Defense_No_VTOL

====================================================================================================

 [Description]

 - Base defense script that will fire at any ground unit of the opposite team.

 [Parameters]

 - MaxAttackDistance: Maximum range of the weapon this base defense uses.

 - Adjust_Aim_For_Infantry: Normally base defense aim at the head of infantry. With some types of weapons, like shells or lasers, this greatly
   decreases their accuracy. If this is set to 1 the base defense will instead aim at the feet of the infantry.

====================================================================================================

; ************************* [Script Name] MDB_Base_Defense_VTOL_Only

====================================================================================================

 [Description]

 - Basic base defense script that will fire at any VTOL unit of the opposite team.

 [Parameters]

 - MaxAttackDistance: Maximum range of the weapon this base defense uses.

====================================================================================================

; ************************* [Script Name] MDB_Base_Defense_Popup

====================================================================================================

 [Description]

 - Base defense script designed to "hide" underground and then popup to attack enemies when they come within range.
   Attacks all units.

 [Parameters]

 - MaxAttackDistance: Maximum range of the weapon this base defense uses.

 - ReturnTime: How long the base defense should stay above ground after it last attacked something.

 - Animation: The animation of the base defense going from underground to above ground. The first frame(0) should be the base defense completely underground.
   The last frame should be the base defense completely above ground, ready to attack.

 - LastFrame: The last frame of the animation.

 - Sensor_Preset: When your base defense is underground it cannot see units above ground, therefore it needs a "Sensor" object above ground 
   which spots units for it.
   This should be an exact copy of your base defense preset, except with it's model set to NULL and ALL scripts removed from it.

 - Sensor_Creation_Bone: Bone to create the Sensor object at. This bone should always be above gound(aka not part of the animation) and should be at about 
   the height of the muzzle bone.

 - Sound: Sound preset to play when the base defense goes from underground to above ground, or vise versa.

 - Adjust_Aim_For_Infantry: Normally base defense aim at the head of infantry. With some types of weapons, like shells or lasers, this greatly
   decreases their accuracy. If this is set to 1 the base defense will instead aim at the feet of the infantry.

====================================================================================================

; ************************* [Script Name] MDB_Base_Defense_Popup_No_VTOL

====================================================================================================

 [Description]

 - Base defense script designed to "hide" underground and then popup to attack enemies when they come within range.
   Attacks all ground units.

 [Parameters]

 - MaxAttackDistance: Maximum range of the weapon this base defense uses.

 - ReturnTime: How long the base defense should stay above ground after it last attacked something.

 - Animation: The animation of the base defense going from underground to above ground. The first frame(0) should be the base defense completely underground.
   The last frame should be the base defense completely above ground, ready to attack.

 - LastFrame: The last frame of the animation.

 - Sensor_Preset: When your base defense is underground it cannot see units above ground, therefore it needs a "Sensor" object above ground 
   which spots units for it.
   This should be an exact copy of your base defense preset, except with it's model set to NULL and ALL scripts removed from it.

 - Sensor_Creation_Bone: Bone to create the Sensor object at. This bone should always be above gound(aka not part of the animation) and should be at about 
   the height of the muzzle bone.

 - Sound: Sound preset to play when the base defense goes from underground to above ground, or vise versa.

 - Adjust_Aim_For_Infantry: Normally base defense aim at the head of infantry. With some types of weapons, like shells or lasers, this greatly
   decreases their accuracy. If this is set to 1 the base defense will instead aim at the feet of the infantry.

====================================================================================================

; ************************* [Script Name] MDB_Base_Defense_Popup_VTOL_Only

====================================================================================================

 [Description]

 - Base defense script designed to "hide" underground and then popup to attack enemies when they come within range.
   Attacks only VTOL units.

 [Parameters]

 - MaxAttackDistance: Maximum range of the weapon this base defense uses.

 - ReturnTime: How long the base defense should stay above ground after it last attacked something.

 - Animation: The animation of the base defense going from underground to above ground. The first frame(0) should be the base defense completely underground.
   The last frame should be the base defense completely above ground, ready to attack.

 - LastFrame: The last frame of the animation.

 - Sensor_Preset: When your base defense is underground it cannot see units above ground, therefore it needs a "Sensor" object above ground 
   which spots units for it.
   This should be an exact copy of your base defense preset, except with it's model set to NULL and ALL scripts removed from it.

 - Sensor_Creation_Bone: Bone to create the Sensor object at. This bone should always be above gound(aka not part of the animation) and should be at about 
   the height of the muzzle bone.

 - Sound: Sound preset to play when the base defense goes from underground to above ground, or vise versa.

====================================================================================================

; ************************* [Script Name] MDB_Send_Custom_Enemy_Seen

====================================================================================================

 [Description]

 - Sends a custom to the specified object when the object the script is attached to sees an enemy.

 [Parameters]

 - Message: Custom to send.

 - ID: ID of the object to send the custom to.

====================================================================================================

; ************************* [Script Name] MDB_Water_Zone

====================================================================================================

 [Description]

 - Damages any object that does NOT have the MDB_Water_Unit script attached to it that enters the script zone.

 [Parameters]

 - Damage: How much damage to do.

 - Warhead: What warhead to use when damaging an object.

====================================================================================================

; ************************* [Script Name] MDB_Water_Unit

====================================================================================================

 [Description]

 - Attach this to objects you do not want to be damaged when entering a zone with MDB_Water_Zone attached to it.

====================================================================================================

; ************************* [Script Name] MDB_Vehicle_Limit

====================================================================================================

 [Description]

 - Sets the vehicle limit on creation.
   NOTE: DO NOT USE THIS SCRIPT IF YOU ARE USING THE VEHICLE TYPE LIMITS. (See readme15.txt)

 [Parameters]

 - Limit: New vehicle limit.

====================================================================================================

; ************************* [Script Name] MDB_Mine_Limit

====================================================================================================

 [Description]

 - Sets the mine limit on creation.

 [Parameters]

 - Limit: New mine limit.

====================================================================================================

; ************************* [Script Name] MDB_Unit_Limit

====================================================================================================

 [Description]

 - This allows only a certain number of this unit to exist at a time. When the limit is reached the PT icon for this 
   unit is disabled.

 [Parameters]

 - Limit: Unit limit.

 - Sidebar: Does your mod use the sidebar feature in bhs.dll? 1 for yes or 0 for no.

====================================================================================================

; ************************* [Script Name] MDB_Remote_Controlled_Vehicle

====================================================================================================

 [Description]

 - Allows a vehicle to be remote controlled by players. Meaning that the player controls the vehicle without actually being
   in it. The player will automatically take control of the vehicle when it is first bought.
   Note: For technical reasons you cannot "get back in"/take control of a vehicle once you "get out". This may change in a
   later release.
   This script will not work if you have "VehicleOwnershipDisable=true" set in hud.ini.

 [Parameters]

 None.

====================================================================================================

; ************************* [Script Name] MDB_Remote_Controlled_Vehicle_Bot

====================================================================================================

 [Description]

 - Used internally. Handles the bot that is created at the player's position when they take remote control of a vehicle.

 [Parameters]

 - ID: ID of the player.

====================================================================================================

; ************************* [Script Name] MDB_Send_Custom_On_Key

====================================================================================================

 [Description]

 - Sends a custom to the specified object when the player presses a key.

 [Parameters]

 - Key: Name of the logical key that triggers the script.

 - ID: ID of the object to send the custom to.

 - Message: Custom message to send.

 - Once: Should this script only be allowed to trigger once? 1 for yes or 0 for no.

====================================================================================================

; ************************* [Script Name] MDB_Sidebar_Key

====================================================================================================

 [Description]

 - Displays the sidebar of the player's team when the player presses a key. Can be enabled and disabled with a custom. Default is disabled.

 [Parameters]

 - Key: Name of the logical key that triggers the script.

 - Enable_Custom: Custom message that enables the script.

 - Disable_Custom: Custom message that disables the script.

 - Start_Enabled: Should the script start off enabled when its attached to a player? 1 for yes or 0 for no.

====================================================================================================

; ************************* [Script Name] MDB_Set_Ammo_Granted_Weapon_On_Pickup

====================================================================================================

 [Description]

 - Attach this to a powerup which grants a weapon. When picked up it will set the ammo of the weapon it grants to the 
   values below.

 [Parameters]

 - Ammo: The amount to set the ammo to. -1 means infinite and -2 means don't change the ammo.

 - Clip_Ammo: The amount to set the ammo of the clip to. -1 means infinite and -2 means don't change the ammo.

====================================================================================================

; ************************* [Script Name] MDB_Set_Ammo_Current_Weapon_On_Pickup

====================================================================================================

 [Description]

 - Attach this to a powerup. When picked up it will set the ammo of the players current weapon to the values below.

 [Parameters]

 - Ammo: The amount to set the ammo to. -1 means infinite and -2 means don't change the ammo.

 - Clip_Ammo: The amount to set the ammo of the clip to. -1 means infinite and -2 means don't change the ammo.

====================================================================================================

; ************************* [Script Name] MDB_Set_Ammo_On_Pickup

====================================================================================================

 [Description]

 - Attach this to a powerup. When picked up it will set the ammo of the specified weapon to the values below.

 [Parameters]

 - Weapon: Preset name of the weapon.

 - Ammo: The amount to set the ammo to. -1 means infinite and -2 means don't change the ammo.

 - Clip_Ammo: The amount to set the ammo of the clip to. -1 means infinite and -2 means don't change the ammo.


