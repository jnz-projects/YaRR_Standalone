/*	Renegade Scripts.dll
	Renegade Role Play 2 Scripts
	Copyright 2007 Jerad Gray, Jonathan Wilson, WhiteDragon(MDB)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class JMG_Slot_Mahine_On_Poked : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Poked(GameObject *obj,GameObject *poker);
	void Animation_Complete(GameObject *obj,const char *anim);
	void Register_Auto_Save_Variables();
};

class JMG_Visible_Infantry_In_Vehicle : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JMG_Attach_Turret_And_Send_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JMG_Flash_Light_Toggle : public ScriptImpClass {
	int CurrentCustom;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JMG_Dual_Weapon_Script : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JMG_Free_For_All_Script : public ScriptImpClass {
	void Created(GameObject *obj);
};

class JMG_Buy_Soldier_On_Preset_Entry : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
};

class MDB_Block_Refill_RP2 : public ScriptImpClass {
	void Created(GameObject *obj);
	void Damaged(GameObject *obj, GameObject *damager, float damage);
	void Timer_Expired(GameObject *obj, int number);
	float currhealth,currshield;
	int LastDamage;
};
