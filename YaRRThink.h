
/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/

class YaRRThink
{
	struct ThinkEvent
	{
		void *Data;
		int Number;
		float Timeout;
		clock_t Clock_Start;
		void (*Callback)(void *, int);
		
		ThinkEvent *Next;

	};

	static ThinkEvent *Events;
public:
	static void Startup();
	static void Shutdown();
	static void Think();
	static void Add_Timer(float Timeout_s, void (*Callback)(void *, int), void *Data, int Number);
	static void DeleteLinkList(ThinkEvent *EventList);
};