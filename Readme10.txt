; ************************* Scripts by Joshua "NeoSaber" Kraft *************************



====================================================================================================

; ************************* [Script Name] Neo_CreateObject_RandomTimer

====================================================================================================

 [Description]

 - This script creates a preset at the position of the object it is attached to, and sets the newly created preset to face the same way as the attached object does. It does so at random intervals, that come between the Min and Max parameters.

 [Parameters]

 - Preset_Name (The name of the preset to create. Default is 'blank')

 - Min (Minimum value that random time could be. Default is 0.0)

 - Max (Maximum value that random time could be. Default is 60.0)

 - Repeat (Determines if timer starts again when it runs out. 1 = yes, 0 = no. Default is 1)

 [NOTES]

 - This script should probably be attached to a Daves Arrow that is placed on the map. If set to repeat, when the timer runs out the preset will be created and it will choose a new random number to set the timer to. It will continue to do so indefinitely.

====================================================================================================

; ************************* [Script Name] Neo_Vehicle_AI

====================================================================================================

 [Description]

 - Early version of an AI script for vehicles. It can guard allies, hunt down enemies, and does a basic threat assessment on targets. It checks if targets are vehicles or soldiers, players or not players, and distance to determine which targets are a priority to attack.

 [Parameters]

 - MaxRange (The maximum range, in meters, that the vehicle will attack enemies. Default is 0.0)

 - vsSoldier (Ammo to use against Soldiers. Primary = 1, Secondary = 0. Default is 1)

 - vsVehicle (Ammo to use against ground Vehicles. Primary = 1, Secondary = 0. Default is 1)

 - vsAircraft (Ammo to use against Aircraft. Primary = 1, Secondary = 0. Default is 1)

 [NOTES]

 - This script should be attached to a vehicle. It doesn't yet account for players getting in and out of a vehicle so transitions should be disabled on any vehicle this is attached to. Restrictions on the vehicle's turret rotation can cause the AI to get confused in combat. 

 - The script currently responds to damage, sounds, and sighting enemies. When idle, if it is shot by an ally it will attempt to guard that ally. If instead it is shot by or sees an enemy it will hunt that enemy. When its target dies (whether it be an ally to guard or an enemy to kill) it will return to idle. If hunting and it finds a higher priority target, it will attack the new target instead. An example of this would be if it was hunting an AI soldier and it sees a player. Players rank as a greater threat than bots so it would turn its attention towards the player. However, if it was hunting an AI vehicle and saw a player walking around it would continue to attack the vehicle since a player walking around ranks as a lower threat than a vehicle.

 - This script can also respond to several customs. The code that checks for customs may not be working right yet, so make use of it at your own risk. Here are 4 of the customs it may respond to:

 - 127127: hunt at location of sender
 - 781781: hunt a new player (sender)
 - 329329: guard location of sender
 - 983983: guard a new player (sender)

 - This script is still a beta, so future versions of the script may be incompatible with this version.