/*	Renegade Scripts.dll
	Misc. Scripts by WhiteDragon(MDB)
	Copyright 2007 Jonathan Wilson, WhiteDragon(MDB)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

class MDB_Weapon_Scope : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
	int CurrScope;
};

class MDB_Weapon_Scope_Global : public JFW_Object_Created_Hook_Base {
	void Created(GameObject *obj);
	void ObjectCreateHook(GameObject *obj);
	char Params[10];
};

class MDB_Change_Spawn_Char_Timer : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
};

class MDB_Change_Spawn_Char_Custom : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class MDB_ConYard : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
	int PMode,created;
	float Time,Amount;
	bool Self;
};

class MDB_Send_Custom_Enemy_Seen : public ScriptImpClass {
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
};

class MDB_Water_Zone : public ScriptImpClass {
	void Entered(GameObject *obj,GameObject *enter);
};

class MDB_Water_Unit : public ScriptImpClass {
};

class MDB_Vehicle_Limit : public ScriptImpClass {
	void Created(GameObject *obj);
};

class MDB_Mine_Limit : public ScriptImpClass {
	void Created(GameObject *obj);
};

class MDB_Unit_Limit : public ScriptImpClass {
	bool Free;
	unsigned int Position;
	unsigned int Type;
	unsigned int Team;
	unsigned int Cost;
	unsigned int StringID;
	void Created(GameObject *obj);
	void Detach(GameObject *obj);
	void Destroyed(GameObject *obj);
	void Register_Auto_Save_Variables();
	void ReEnable();
};

class MDB_Send_Custom_On_Key : public JFW_Key_Hook_Base {
	void Created(GameObject *obj);
	void KeyHook();
};

class MDB_Remote_Controlled_Vehicle : public ScriptImpClass {
	int BotID;
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
	void Destroyed(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Register_Auto_Save_Variables();
};


class MDB_Remote_Controlled_Vehicle_Bot : public ScriptImpClass {
	int ID,VehID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class MDB_Sidebar_Key : public JFW_Key_Hook_Base {
	void Created(GameObject *obj);
	void KeyHook();
	void Register_Auto_Save_Variables();
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	bool Enabled;
};

class MDB_Set_Ammo_Granted_Weapon_On_Pickup : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class MDB_Set_Ammo_Current_Weapon_On_Pickup : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class MDB_Set_Ammo_On_Pickup : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};
