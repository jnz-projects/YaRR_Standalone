/*	Renegade Scripts.dll
	Renegade Role Play 2 Scripts
	Copyright 2007 Jerad Gray, Jonathan Wilson, WhiteDragon(MDB)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "jmgrp2.h"
#include "engine.h"

void JMG_Slot_Mahine_On_Poked::Created(GameObject *obj)
{
	if (1 == Get_Int_Parameter("StartEnabled"))
	{
		enabled = true;
	}
	else if (0 == Get_Int_Parameter("StartEnabled"))
	{
		enabled = false;
	}
}

void JMG_Slot_Mahine_On_Poked::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("EnableMessage"))
	{
		enabled = true;
	}
	if (message == Get_Int_Parameter("DisableMessage"))
	{
		enabled = false;
	}
}

void JMG_Slot_Mahine_On_Poked::Poked(GameObject *obj,GameObject *poker)
{
	if (enabled)
	{
		int cost;
		cost = Get_Int_Parameter("Cost");
		if (cost <= Commands->Get_Money(poker))
		{
			int winnings = Get_Int_Parameter("Winnings");
			int percent = Get_Int_Parameter("Percent");
			int chance = Commands->Get_Random_Int(0,99);
			if (chance < percent)
			{
				Commands->Give_Money(poker,(float)winnings,0);
			}
			else
			{
				cost = -cost;
				Commands->Give_Money(poker,(float)cost,0);
			}
			Commands->Set_Animation(obj,Get_Parameter("Animation"),false,0,0,-1,false);
			Vector3 v = Commands->Get_Position(obj);
			Commands->Create_Sound(Get_Parameter("Sound"),v,obj);
			enabled = false;
		}
	}
}

void JMG_Slot_Mahine_On_Poked::Animation_Complete(GameObject *obj,const char *anim)
{
	enabled = true;
}

void JMG_Slot_Mahine_On_Poked::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&enabled);
}

void JMG_Visible_Infantry_In_Vehicle::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		GameObject *object = Commands->Find_Object(param);
		Commands->Set_Model(object,Get_Parameter("ModelName"));
		const char *subobject = Get_Parameter("Subobject");
		if (!_stricmp(subobject,"0"))
		{
			subobject = 0;
		}
		float firstframe = Get_Float_Parameter("FirstFrame");
		if (firstframe == -1)
		{
			firstframe = Get_Animation_Frame(obj);
		}
		Commands->Disable_Physical_Collisions(object);
		Commands->Set_Animation(object,Get_Parameter("Animation"),true,subobject,firstframe,Get_Float_Parameter("LastFrame"),Get_Int_Parameter("Blended"));
	}
}

void JMG_Attach_Turret_And_Send_Custom::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	float facing = Commands->Get_Facing(obj);
	Vector3 position = Commands->Get_Position(obj);
	if (message == Get_Int_Parameter("CreateMessage"))
	{
		GameObject *object = Commands->Create_Object(Get_Parameter("Preset"),position);
		Commands->Set_Facing(object,facing);
		Commands->Attach_To_Object_Bone(object,obj,Get_Parameter("BoneName"));
	}
	if (message == Get_Int_Parameter("SendMessageMessage"))
	{
		Send_Custom_All_Objects_Area(Get_Int_Parameter("SendMessage"),Commands->Get_Position(obj),0.1f,obj,2);
	}
}

void JMG_Flash_Light_Toggle::Created(GameObject *obj)
{
	CurrentCustom = 0;
}

void JMG_Flash_Light_Toggle::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("TriggerMessage"))
	{
		Commands->Send_Custom_Event(obj,Commands->Find_Object(Get_Int_Parameter(CurrentCustom)),Get_Int_Parameter(CurrentCustom+2),0,0);
		CurrentCustom++;
		if (CurrentCustom > 1)
		{
			CurrentCustom = 0;
		}
	}
}

void JMG_Flash_Light_Toggle::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&CurrentCustom);
}

void JMG_Dual_Weapon_Script::Created(GameObject *obj)
{
	Commands->Set_Is_Rendered(obj,0);
}

void JMG_Dual_Weapon_Script::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("Message"))
	{
		if (!_stricmp(Get_Current_Weapon(sender),Get_Parameter("Weapon")))
		{
			Commands->Set_Is_Rendered(obj,1);
		}
		else
		{
			Commands->Set_Is_Rendered(obj,0);
		}
	}
}

void JMG_Free_For_All_Script::Created(GameObject *obj)
{
	Attach_Script_Once(obj,"MDB_Block_Refill_RP2","0");
}

void JMG_Buy_Soldier_On_Preset_Entry::Entered(GameObject *obj,GameObject *enter)
{
	const char *preset;
	const char *preset2;
	const char *c;
	int cost;
	c = Get_Parameter("Enter_Preset");
	preset = Commands->Get_Preset_Name(enter);
	if (!_stricmp(preset,c))
	{
		cost = Get_Int_Parameter("Cost");
		preset2 = Get_Parameter("Preset_Name");
		if (cost <= Commands->Get_Money(enter))
		{
			cost = -cost;
			Commands->Give_Money(enter,(float)cost,0);
			Change_Character(enter,preset2);
		}
	}

}

/* Script copied from SSAOW and added to this because if JFW's script don't have it, the refil blocker would not have worked, and this way its settings can't be changed, and time extended so it can almost never heal*/
void MDB_Block_Refill_RP2::Created(GameObject *obj)
{
	currhealth = Commands->Get_Health(obj);
	currshield = Commands->Get_Shield_Strength(obj);
	Commands->Start_Timer(obj,this,0.5f,59);
	LastDamage = The_Game()->GameDuration_Seconds;
}

void MDB_Block_Refill_RP2::Damaged(GameObject *obj, GameObject *damager, float damage)
{
	currhealth = Commands->Get_Health(obj);
	currshield = Commands->Get_Shield_Strength(obj);
	if (damage > 0.0) LastDamage = The_Game()->GameDuration_Seconds;
}

void MDB_Block_Refill_RP2::Timer_Expired(GameObject *obj, int number)
{
	if (number == 59)
	{
		if (Commands->Get_Health(obj) > currhealth || Commands->Get_Shield_Strength(obj) > currshield)
		{
			if (Get_Int_Parameter("All") == 1)
			{
				Commands->Set_Health(obj,currhealth);
				Commands->Set_Shield_Strength(obj,currshield);
			}
			else if (Get_Int_Parameter("All") == 0 && (The_Game()->GameDuration_Seconds - LastDamage) <= 99999999)
			{
				Commands->Set_Health(obj,currhealth);
				Commands->Set_Shield_Strength(obj,currshield);
			}
		}
		Commands->Start_Timer(obj,this,0.5f,59);
	}
}

ScriptRegistrant<JMG_Slot_Mahine_On_Poked> JMG_Slot_Mahine_On_Poked("JMG_Slot_Mahine_On_Poked","Cost:int,Winnings:int,Percent:int,Animation:string,Sound:string,EnableMessage:int,DisableMessage:int,StartEnabled=0:int");
ScriptRegistrant<JMG_Visible_Infantry_In_Vehicle> JMG_Visible_Infantry_In_Vehicle("JMG_Visible_Infantry_In_Vehicle","ModelName:string,Message=200:int,Animation=s_a_human.rp2driveranim:string,SubObject=0:string,FirstFrame=0:float,LastFrame=-1.000:float,Blended=0:int");
ScriptRegistrant<JMG_Attach_Turret_And_Send_Custom> JMG_Attach_Turret_And_Send_Custom("JMG_Attach_Turret_And_Send_Custom","Preset=Master_Chief_Flash_Light_Beam:string,CreateMessage=959595:int,SendMessageMessage=595959:int,SendMessage=5656478:int,SendParam=1:int,BoneName=origin:string");
ScriptRegistrant<JMG_Flash_Light_Toggle> JMG_Flash_Light_Toggle("JMG_Flash_Light_Toggle","SendID1=100334:int,SendID2=100334:int,SendMessage1=959595:int,SendMessage2=595959:int,TriggerMessage=898524:int");
ScriptRegistrant<JMG_Dual_Weapon_Script> JMG_Dual_Weapon_Script("JMG_Dual_Weapon_Script","Message=987456:int,Weapon=Weapon_Tanaj:string");
ScriptRegistrant<JMG_Free_For_All_Script> JMG_Free_For_All_Script("JMG_Free_For_All_Script","");
ScriptRegistrant<MDB_Block_Refill_RP2> MDB_Block_Refill_RP2("MDB_Block_Refill_RP2","All=1:int");
