/*	Renegade Scripts.dll
	Glow Shader Class for the Shader Database Editor
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <direct.h>
#include "scripts.h"
#include "shadereng.h"
#include <commdlg.h>
#include "shader.h"
#include "editorshader.h"
#include "editorshadermgr.h"
#include "glowshader.h"
#include "editorglowshader.h"
#include "resource1.h"
#pragma warning(disable: 6031)

float GetDlgItemFloat(HWND hDlg, int id);
BOOL SetDlgItemFloat(HWND hDlg, int id, float f);
EditorGlowShaderClass *CurrentShader = 0;

BOOL CALLBACK GlowDlgProc(HWND hwnd,UINT Message,WPARAM wParam,LPARAM lParam)
{
	OPENFILENAME ofn;
	char sfile[MAX_PATH] = "";
	char c[MAX_PATH];
	switch(Message)
	{
	case WM_INITDIALOG:
		SetDlgItemFloat(hwnd,IDC_THICKNESS,CurrentShader->shader->GlowThickness);
		SetDlgItemFloat(hwnd,IDC_LIGHTDIRX,CurrentShader->shader->LightDir.X);
		SetDlgItemFloat(hwnd,IDC_LIGHTDIRY,CurrentShader->shader->LightDir.Y);
		SetDlgItemFloat(hwnd,IDC_LIGHTDIRZ,CurrentShader->shader->LightDir.Z);
		SetDlgItemFloat(hwnd,IDC_COLORX,CurrentShader->shader->GlowColor.X);
		SetDlgItemFloat(hwnd,IDC_COLORY,CurrentShader->shader->GlowColor.Y);
		SetDlgItemFloat(hwnd,IDC_COLORZ,CurrentShader->shader->GlowColor.Z);
		SetDlgItemFloat(hwnd,IDC_COLORW,CurrentShader->shader->GlowColor.W);
		SetDlgItemFloat(hwnd,IDC_AMBIENTX,CurrentShader->shader->GlowAmbient.X);
		SetDlgItemFloat(hwnd,IDC_AMBIENTY,CurrentShader->shader->GlowAmbient.Y);
		SetDlgItemFloat(hwnd,IDC_AMBIENTZ,CurrentShader->shader->GlowAmbient.Z);
		SetDlgItemFloat(hwnd,IDC_AMBIENTW,CurrentShader->shader->GlowAmbient.W);
		if (CurrentShader->shader->Filename)
		{
			SetDlgItemText(hwnd,IDC_SHADERFILE,CurrentShader->shader->Filename);
		}
		break;
	case WM_CLOSE:
		EndDialog(hwnd, 0);
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			CurrentShader->shader->GlowThickness = GetDlgItemFloat(hwnd,IDC_THICKNESS);
			CurrentShader->shader->LightDir.X = GetDlgItemFloat(hwnd,IDC_LIGHTDIRX);
			CurrentShader->shader->LightDir.Y = GetDlgItemFloat(hwnd,IDC_LIGHTDIRY);
			CurrentShader->shader->LightDir.Z = GetDlgItemFloat(hwnd,IDC_LIGHTDIRZ);
			CurrentShader->shader->GlowColor.X = GetDlgItemFloat(hwnd,IDC_COLORX);
			CurrentShader->shader->GlowColor.Y = GetDlgItemFloat(hwnd,IDC_COLORY);
			CurrentShader->shader->GlowColor.Z = GetDlgItemFloat(hwnd,IDC_COLORZ);
			CurrentShader->shader->GlowColor.W = GetDlgItemFloat(hwnd,IDC_COLORW);
			CurrentShader->shader->GlowAmbient.X = GetDlgItemFloat(hwnd,IDC_AMBIENTX);
			CurrentShader->shader->GlowAmbient.Y = GetDlgItemFloat(hwnd,IDC_AMBIENTY);
			CurrentShader->shader->GlowAmbient.Z = GetDlgItemFloat(hwnd,IDC_AMBIENTZ);
			CurrentShader->shader->GlowAmbient.W = GetDlgItemFloat(hwnd,IDC_AMBIENTW);
			if (CurrentShader->shader->GlowColor.X > 1.0)
			{
				CurrentShader->shader->GlowColor.X = 1.0;
			}
			if (CurrentShader->shader->GlowColor.Y > 1.0)
			{
				CurrentShader->shader->GlowColor.Y = 1.0;
			}
			if (CurrentShader->shader->GlowColor.Z > 1.0)
			{
				CurrentShader->shader->GlowColor.Z = 1.0;
			}
			if (CurrentShader->shader->GlowColor.W > 1.0)
			{
				CurrentShader->shader->GlowColor.W = 1.0;
			}
			if (CurrentShader->shader->GlowAmbient.X > 1.0)
			{
				CurrentShader->shader->GlowAmbient.X = 1.0;
			}
			if (CurrentShader->shader->GlowAmbient.Y > 1.0)
			{
				CurrentShader->shader->GlowAmbient.Y = 1.0;
			}
			if (CurrentShader->shader->GlowAmbient.Z > 1.0)
			{
				CurrentShader->shader->GlowAmbient.Z = 1.0;
			}
			if (CurrentShader->shader->GlowAmbient.W > 1.0)
			{
				CurrentShader->shader->GlowAmbient.W = 1.0;
			}
			if (CurrentShader->shader->GlowColor.X < 0.0)
			{
				CurrentShader->shader->GlowColor.X = 0.0;
			}
			if (CurrentShader->shader->GlowColor.Y < 0.0)
			{
				CurrentShader->shader->GlowColor.Y = 0.0;
			}
			if (CurrentShader->shader->GlowColor.Z < 0.0)
			{
				CurrentShader->shader->GlowColor.Z = 0.0;
			}
			if (CurrentShader->shader->GlowColor.W < 0.0)
			{
				CurrentShader->shader->GlowColor.W = 0.0;
			}
			if (CurrentShader->shader->GlowAmbient.X < 0.0)
			{
				CurrentShader->shader->GlowAmbient.X = 0.0;
			}
			if (CurrentShader->shader->GlowAmbient.Y < 0.0)
			{
				CurrentShader->shader->GlowAmbient.Y = 0.0;
			}
			if (CurrentShader->shader->GlowAmbient.Z < 0.0)
			{
				CurrentShader->shader->GlowAmbient.Z = 0.0;
			}
			if (CurrentShader->shader->GlowAmbient.W < 0.0)
			{
				CurrentShader->shader->GlowAmbient.W = 0.0;
			}
			GetDlgItemText(hwnd,IDC_SHADERFILE,sfile,MAX_PATH);
			CurrentShader->shader->Set_Filename(sfile);
			EndDialog(hwnd, 1);
			break;
		case IDCANCEL:
			EndDialog(hwnd, 0);
			break;
		case IDC_OPEN:
			_getcwd(c,MAX_PATH);
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hwnd;
			ofn.lpstrFilter = "Shaders (*.fx)\0*.fx\0All Files (*.*)\0*.*\0";
			ofn.lpstrInitialDir = c;
			ofn.lpstrFile = sfile;
			ofn.lpstrTitle = "Open Shader";
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrDefExt = ".fx";
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_DONTADDTORECENT | OFN_NOCHANGEDIR;
			if (GetOpenFileName(&ofn))
			{
				strcpy(c,&strrchr(sfile,'\\')[1]);
				SetDlgItemText(hwnd,IDC_SHADERFILE,c);
			}
			break;
		}
	default:
		return FALSE;
	}
	return TRUE;
}

EditorGlowShaderClass::EditorGlowShaderClass()
{
	shader = new GlowShaderClass();
}

EditorGlowShaderClass::~EditorGlowShaderClass()
{
	delete shader;
}

void EditorGlowShaderClass::Load(ChunkLoadClass& cload)
{
	shader->Load(cload);
}

void EditorGlowShaderClass::Save(ChunkSaveClass& csave)
{
	shader->Save(csave);
}

void EditorGlowShaderClass::Edit(HWND ParentDialog)
{
	CurrentShader = this;
	DialogBox(hInst, MAKEINTRESOURCE(IDD_GLOW), ParentDialog,(DLGPROC)GlowDlgProc);
	CurrentShader = 0;
}

const char *EditorGlowShaderClass::Get_Name()
{
	return shader->Get_Name();
}

void EditorGlowShaderClass::Set_Name(const char *name)
{
	shader->Set_Name(name);
}

EditorShaderRegistrant<EditorGlowShaderClass> EditorGlowShaderRegistrant(CHUNK_GLOW_SHADER,"Glow Shader");
