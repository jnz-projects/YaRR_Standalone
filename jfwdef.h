/*	Renegade Scripts.dll
	Base defense scripts
	Copyright 2007 Olaf Van Der Spek, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class JFW_User_Controllable_Base_Defence : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool player;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_No_Aircraft : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Aircraft_Only : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Secondary : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_No_Aircraft_Secondary : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Aircraft_Only_Secondary : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_No_Aircraft : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Aircraft_Only : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_No_Aircraft_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Aircraft_Only_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_No_VTOL : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_VTOL_Only : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_No_VTOL_Secondary : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_VTOL_Only_Secondary : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_No_VTOL : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_VTOL_Only : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_No_VTOL_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_VTOL_Only_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_No_Aircraft : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_Aircraft_Only : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_No_Aircraft_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_Aircraft_Only_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_No_VTOL : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_VTOL_Only : public ScriptImpClass {
	bool popup;
	bool attack;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_No_VTOL_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_VTOL_Only_Secondary : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Swap : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_No_Aircraft_Swap : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Aircraft_Only_Swap : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_No_Aircraft_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Aircraft_Only_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_No_VTOL_Swap : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_VTOL_Only_Swap : public ScriptImpClass {
	unsigned int id1;
	unsigned int id2;
	unsigned int id3;
	unsigned int objtype;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_No_VTOL_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_VTOL_Only_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_No_Aircraft_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_Aircraft_Only_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_No_VTOL_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Base_Defence_Animated_Sound_VTOL_Only_Swap : public ScriptImpClass {
	bool popup;
	bool attack;
	bool primary;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Enemy_Seen(GameObject *obj,GameObject *seen);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};
