/*	Renegade Scripts.dll
	Definition of stuff inside d3d8.dll that shaders.dll needs
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#pragma push_macro("new")
#pragma push_macro("delete")
#undef new
#undef delete
#include <d3d9.h>
#include <d3d8caps.h>
#include <d3dx9.h>
#pragma pop_macro("new")
#pragma pop_macro("delete")

class myIDirect3DDevice8;
class myIDirect3D8 : public IUnknown {
public:
	IDirect3D9* d3d9;
	myIDirect3DDevice8* device8;
};

class myIDirect3DDevice8 : public IUnknown {
public:
	IDirect3DDevice9* device9;
	myIDirect3D8* d3d8;
	UINT ShadowWidth;
	UINT ShadowHeight;
	UINT vsync;
	UINT AA;
};

class myIDirect3DTexture8 : public IUnknown {
public:
	IDirect3DTexture9* texture9;
};

class myIDirect3DSurface8 : public IUnknown {
public:
	IDirect3DSurface9* surface9;
};
