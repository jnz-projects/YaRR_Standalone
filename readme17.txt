* z_Console_Input_Killed & z_Console_Input_Killed_2
Attach this script to objects, players or bots. When they are killed the script will execute
any whatever command you entered for Input. This can be any valid fds command, even quit or restart
but i doubt you would want to use that :P
Same applies to _2, it will do two console commands when the object dies.

* z_Console_Input_Custom_2
Attach this script to any object you want (or a Davearrow), when it receives the message
it will do two console commands.

* z_Play_Three_Cinematics_Custom
This is basically a copy from Jonwil's script that launches a cinematic. This script will
launch three cinematics at the same time (when it receives a message) at three definable
locations and will do a console command.

* z_Play_Cinematic_Console_Input_Custom
This script will launch a cinematic when it receives a message and will do a console command.

* z_Destroy_Three_Objects_Custom
Basically a clone from Jonwil's JFW_Destroy_Object_Custom, except this one will destroy 
three objects at once...

* z_Enable_Multiple_Spawners_Custom
Basically a clone from Jonwil's JFW_Enable_Spawner, except this one will en/disable 5 at once.

* z_Console_Input_Poke_2
This script can be attached to objects (switches for example) and when a player pokes it (press E)
it will do two console commands.

* z_Console_Input_Custom_Delay_1 & z_Console_Input_Custom_Delay_2
Attach this to a dave arrow for example. When it receives a message it will do a console
command and if repeat is enabled, it will loop until some other script destroys the dave arrow
it is attached to.

* z_RemoveWeaponModel
You can use this to remove the weapon model shown on the back of players.
IE: If you want to remove the Ramjet model from havocs back, simply attach this
script to the havoc presets and set WeaponModel to Weapon_RamjetRifle.

* z_rwk
This script will rebuild the object it is attached to when it gets killed.
You can for example use this on blamo objects that normally cant be killed, cheaters
for example, this script will recreate the object on the same location as soon as it is killed.

* z_GameoverCreated
When the object this script is attached to is created, it will gameover the map,
amsg "Mission Accomplished" and play an EVA sound saying the mission was accomplished..

* z_Created_Send_Custom_Param
As soon as the object you attached this to in leveledit for example is created in
the game it will send a message and parameter to any object you want.

* z_NoDamageMoneyPoints
This script will stop players from damaging the object it is attached to and they 
will not get points for attacking it neither.

* z_Set_Team
If you want GDI Turrets serverside for example you can place the Nod_Turret
on Field.lvl in GDI's base, put this script on the turret and set Team to 1.
0=NOD 1=GDI
You can use this script on all objects pretty much.

* z_Set_Skin_Created
This script will set the Skintype/Ammount of the object it is attached to as soon as
it gets created on the map.
When it is sent a message it will set the Skintype/Ammount to whatever you define
and it can also remove a script that is attached to the object. Was quite useful in coop...

* z_Set_Skin
This script will set the Skintype/Ammount of the object it is attached to as soon
as it is created on the map.

* z_Death_Enable_Spawner & z_Death_Enable_3Spawners
When the object this script is attached to dies, it will enable (or disable) a spawner/3 spawners.

* z_UnkillableUntilEntered
Attach this to vehicles, the vehicle cannot be killed until someone entered it.

* z_DestroyVeh259
Attach this to vehicles, the vehicle this script is attached to will be destroyed
after 259 seconds (4m59s) unless someone enters it, then the script is removed and the
vehicle will live :)

* z_VehExit
Attach this to vehicles, when the player exits the vehicle it will attach the
z_DestroyVeh259 script to it, which will get removed again if someone enters that 
vehicle again.

* z_Spawn_When_Killed
Basically a clone of TFX_Spawn_When_Killed, only difference is that this script allows
you to set it to whatever you want instead of a percentage of its maxhealth.

* z_Remove_Script_Custom
Script will remove a script from the object it is attached to when it is sent the message.

* z_Teleport_Powerup
Attach this to a powerup preset or a spawner on the map, when a player picks it up, they will
be teleported to the location/ID you entered.

* z_blamo4sec
This script can be attached to player spawnpoints for example, it will make them invulnerable
for four seconds. To stop spawnkilling for example.

* z_ChangeTeamPowerup
Attach this to a powerup preset or a spawner on the map, when a player picks it up, their
team will be changed to the opposite team. A hostmessage will be made like:
Host: Playername team was changed to NOD/GDI by a powerup.
