; ************************* Scripts by NameHunter *************************



====================================================================================================

; ************************* [Script Name] NHP_Smart_Targetting

====================================================================================================

 [Description]

 - Uses user set weapons and targets and uses a weapon based on the enemy target.

 [Parameters]

 - Presets_1-4_Weapon (Weapon preset to use for target presets 1-4)

 - Presets_5-8_Weapon (Weapon preset to use for target presets 5-8)

 - Other_Weapon (Weapon preset to use for unknown enemy presets)

 - Preset_One (Target 1)

 - Preset_Two (Target 2)

 - Preset_Three (Target 3)

 - Preset_Four (Target 4)

 - Preset_Five (Target 5)

 - Preset_Six (Target 6)

 - Preset_Seven (Target 7)

 - Preset_Eight (Target 8)

 [Notes]

 - This can allow bots to do things like use rockets on enemy tanks and chaingun on infantry. It works together with innate enabled, for the script will not pursue or attack enemies on its own. The main purpose is to make the bot use other weapons instead of just its main gun.

====================================================================================================

; ************************* [Script Name] NHP_Guard_Bot

====================================================================================================

 [Description]

 - Makes a bot escort a teammate when shot.

 [Parameters]

 - Distance (Range to stay within target escort)

 - Speed (Speed to arrive at destination, can be more then the character's maximum speed)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] NHP_Construction_AI

====================================================================================================

 [Description]

 - Builds units after a timer goes off.

 [Parameters]

 - Money_Gen (Name of the unit to generate money for the team)

 - Guard_Inf (Name of the infantry that is to defend the overmind)

 - Attack_Unit (Name of the unit that is to attack the enemy)

 - Money_Limit (The maximum number of money units that can be built. Once built that slot is lost, so if you want unlimited, set a huge number)

 - Guard_Limit (The maximum number of guard units that can be built. Once built that slot is lost, so if you want unlimited, set a huge number)

 - Attack_Limit (The maximum number of attack units that can be built. Once built that slot is lost, so if you want unlimited, set a huge number)

 - Time_Delay (The delay between building units)

 - Guard_Path (The path ID for the guards to follow)

 - Attack_Path (The path ID for the guards to follow)

 - Money_Path (The path ID for the money unit to follow)

 [Notes]

 - While the parameter names are set up for Money, Defend, and Attack, it is also set up so you can have 2 attack, 1 defend, etc. Fill in whatever parameters you want to replace as if it were that other parameter. This can allow more AI buildings specialized functions such one building attacks, 2 buildings defend)

====================================================================================================

; ************************* [Script Name] NHP_Sensor_Array_Zone

====================================================================================================

 [Description]

 - This is a zone designed to turn a 3D sound on and off, automaticlly creating the sound. Make sure you also use the script NHP_Sensorable_Object in conjunction.

 [Parameters]

 - Sensor_List_File (this is the text file that contains all the preset names to look for. It will not work with presets that have spaces in their names. The first word/number should be the number of presets in the list minus one, and the rest should be the preset names. For example, you want the sensor to sound an alarm when an Orca, Medium Tank, or Stealth Tank enter the zone, the file with the list would look like: "2 CnC_GDI_Orca CnC_GDI_Medium_Tank CnC_Nod_Stealth_Tank". The presets do not have to be in any particular order)

 - Sound_Preset (3D sound preset to play when something is in the zone)

 - Sound_Position (Location to play the 3D sound)

 - Check_Team (The team to ignore. Say you have a sensor on GDI, you would want to set this to 1 so that it ignores GDI units. 1 = GDI, 2 = Nod)

 [Notes]

 - This script will not stop triggering the alarm on its own when a sensor is blown up. You must use a script to send the custom/message 901192102 in order to turn the alarm off. Check the JFW scripts for this.

====================================================================================================

; ************************* [Script Name] NHP_Sensorable_object

====================================================================================================

 [Description]

 - Attach this to objects that you want the sensors to detect. This is used because the sensors do not pick up dead objects in the leave event, so if you don't use this script, sensors may leave the alarm on if something they're supposed to detect blows up inside the zone. Warning, DO NOT OVERLAP SENSOR ZONES! This script WILL NOT work properly if it is inside 2 or more script zones with the sensor script at the same time! Some alarms will be left on!

 [Parameters]

 - NONE

 [Notes]

 - NONE