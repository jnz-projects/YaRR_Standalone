

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


struct ObjectDamaged
{
	int PlayerID;
	float Damage;
};


class IRCThinkScript : public ScriptImpClass, public YaRRLink
{
	bool extrad;
	bool ReconSent;
	int lastframecount;
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
	void Custom(GameObject *o, int message, int param, GameObject *sender);

public:
	static int sfps;
};

class YaRRboot : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
};

class YaRRPlayer : public ScriptImpClass, public YaRRLink
{
	bool wait;
	float Points;

	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
	void Damaged(GameObject *o, GameObject *attacker, float damage);
	void Killed(GameObject *o, GameObject *Killer);
	void Destroyed(GameObject *o);
};

class YaRRVehicle : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
	void Damaged(GameObject *o, GameObject *attacker, float damage);
	void Killed(GameObject *o, GameObject *killer);
	void Destroyed(GameObject *o);
};

class YaRRBuilding : public ScriptImpClass, public YaRRLink
{
	Stacker<ObjectDamaged *> DamageList;
	bool dwarning;
	bool dswarning;
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
	void Damaged(GameObject *o, GameObject *attacker, float damage);
	void Killed(GameObject *o, GameObject *killer);
	void Destroyed(GameObject *o);
};

class YaRRBeacon : public ScriptImpClass, public YaRRLink
{
	Stacker<ObjectDamaged *> DamageList;
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
	void Damaged(GameObject *o, GameObject *attacker, float damage);
	void Killed(GameObject *o, GameObject *killer);
	void Destroyed(GameObject *o);
};

class YaRRC4 : public ScriptImpClass, public YaRRLink
{
	Stacker<ObjectDamaged *> DamageList;
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
	void Damaged(GameObject *o, GameObject *attacker, float damage);
	void Killed(GameObject *o, GameObject *killer);
	void Destroyed(GameObject *o);
};


class YaRRRebuild : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Destroyed(GameObject *o);
};


class YaRRBeaconCtrl : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
};

class YaRR_Kill : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
};

class YaRR_PlaySound : public ScriptImpClass, public YaRRLink
{
	Stacker<WavSound> Sounds;
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int Number);
};

class YaRRProtect : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Destroyed(GameObject *o);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class YaRRRoot : public ScriptImpClass, public YaRRLink
{
	void Created(GameObject *o);
	void Destroyed(GameObject *o);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class YaRRVeteranScript : public ScriptImpClass, public YaRRLink
{
	const PromoteInfo *Promote;
	void Created(GameObject *o);
	void Timer_Expired(GameObject *o, int number);
};