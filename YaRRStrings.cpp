#include "YARRIncludes.h"


/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


char *YaRRStrings::Strings[100];

void YaRRStrings::Startup()
{
	DLOG;
	memset((void *)Strings, 0, 400);

	char path[128];
	GetCurrentDirectory(100, path);
	strcat(path, "\\config\\Strings.ini");
	INIClass *ini = Get_INI(path);
	if(!ini)
	{
		YaRRFunctions::YaRRError("\\config\\Strings.ini not found");
	}

	for(int i = 1; i < 100; i++)
	{
		char tmp[8];
		sprintf(tmp, "%.2d", i);

		char buffer[256];
		ini->Get_String("Strings", tmp, "", buffer, 256);
		if(strcmp(buffer, "") == 0)
		{
			break;
		}
		else
		{
			Strings[i-1] = YaRRFunctions::strdup2(buffer);
		}
	}

	Release_INI(ini);
}

void YaRRStrings::Shutdown()
{
	DLOG;
	for(int i = 0; Strings[i] != 0; i++)
	{
		CDealloc(Strings[i]);
		Strings[i] = 0;
	}
}

const char *YaRRStrings::Fetch(int sID)
{
	DLOG;
	return Strings[sID-1] == 0 ? "" : Strings[sID-1];
}
