#include "YaRRIncludes.h"

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/

GdiplusStartupInput YaRRPHP::strGDIPlus;
ULONG_PTR YaRRPHP::GdiStartup;
Image *YaRRPHP::image = 0;

DWORD __stdcall YaRRPHP::Upload(void *)
{
	printf("Start upload...\n");
	HINTERNET intern = InternetOpen("YaRR", INTERNET_OPEN_TYPE_DIRECT, 0, 0, 0);
	if(!intern)
	{
		printf("Unable to open an internet - %d\n", GetLastError());
		return 0;
	}
	HINTERNET intern2 = InternetConnect(intern, "ftp.host.comk", 21, "username", "password", INTERNET_SERVICE_FTP, 0, 0);

	if(!intern2)
	{
		printf("Unable to open a connection - %d\n", GetLastError());
		InternetCloseHandle(intern);
		return 0;
	}

	if(FtpPutFile(intern2, "test.png", "test.png", FTP_TRANSFER_TYPE_BINARY, 0))
	{
		printf("Upload complete\n");
	}
	else
	{
		printf("Upload failed - %d\n", GetLastError());
	}

	InternetCloseHandle(intern);
	return 0;
}

DWORD YaRRPHP::GetModulePath(HINSTANCE hInst, LPTSTR pszBuffer, DWORD dwSize ) //This function was created by microsoft
{
	DWORD dwLength = GetModuleFileName( hInst, pszBuffer, dwSize );
	if( dwLength )
	{
		while( dwLength && pszBuffer[ dwLength ] != '\\')
		{
			dwLength--;
		}

		if( dwLength )
			pszBuffer[ dwLength] = '\000';
	}
	return dwLength;
}

int YaRRPHP::GetEncoderClsid(const WCHAR* format, CLSID* pClsid) //This function was created by microsoft
{
   UINT  num = 0;          // number of image encoders
   UINT  size = 0;         // size of the image encoder array in bytes

   ImageCodecInfo* pImageCodecInfo = NULL;

   GetImageEncodersSize(&num, &size);
   if(size == 0)
      return -1;  // Failure

   pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
   if(pImageCodecInfo == NULL)
      return -1;  // Failure

   GetImageEncoders(num, size, pImageCodecInfo);

   for(UINT j = 0; j < num; ++j)
   {
      if( wcscmp(pImageCodecInfo[j].MimeType, format) == 0 )
      {
         *pClsid = pImageCodecInfo[j].Clsid;
         free(pImageCodecInfo);
         return j;  // Success
      }    
   }

   free(pImageCodecInfo);
   return -1;  // Failure
}


void YaRRPHP::Start()
{
	GdiplusStartup(&GdiStartup, &strGDIPlus, NULL);
	//YaRRThink::Add_Timer(10.0, YaRRPHP::Think, 0, 0);
}

void YaRRPHP::Stop()
{
	GdiplusShutdown(GdiStartup);
}

void YaRRPHP::Think(void *, int)
{
	Bitmap *bmp = new Bitmap(400, 400);
	Graphics *graphics = new Graphics(bmp);

	Draw(graphics);

	CLSID pngClsid;
	GetEncoderClsid(L"image/png", &pngClsid);
	bmp->Save(L"test.png", &pngClsid, NULL);

	delete graphics;
	delete bmp;

	CreateThread(0, 0, Upload, 0, 0, 0);
	YaRRThink::Add_Timer(20.0, YaRRPHP::Think, 0, 0);
}

void YaRRPHP::MapLoad()
{
	char img[256];
	if(sscanf(The_Game()->MapName, "%[^.].", img) == 0)
	{
		return;
	}
	char Path[MAX_PATH];
	GetModulePath(NULL, Path, MAX_PATH);
	char tmp[512];
	sprintf(tmp, "%s\\Maps\\%s.png", Path, img);

	const wchar_t *wcs = CharToWideChar(tmp);
	if(image)
	{
		delete image;
		image = 0;
	}
	image = new Image(wcs);
	RectF boundsRect;
	Unit unit;

	Status bounds = image->GetBounds(&boundsRect, &unit);
	if(bounds != 0)
	{
		delete image;
		image = 0;
	}
	delete []wcs;

}
void YaRRPHP::Draw(Graphics *graphics)
{
	if(!image)
	{
		return;
	}
	
	graphics->DrawImage(image, 0, 0, 400, 400);

	int Minutes = (int(The_Game()->TimeRemaining_Seconds) - (int(The_Game()->TimeRemaining_Seconds) % 60)) / 60;
	int Seconds = int(The_Game()->TimeRemaining_Seconds) % 60;

	wchar_t wcs[16];
	wsprintfW(wcs, L"%d.%d", Minutes, Seconds);

	Font font(L"Arial", 8);
	RectF layoutRect(0, 0, 400, 400);
	RectF boundRect;
	graphics->MeasureString(wcs, (int)wcslen(wcs), &font, layoutRect, &boundRect);

	PointF pos;
	pos.X = 400-boundRect.Width-10;
	pos.Y = boundRect.Height+45;
	SolidBrush brush(Color(0, 0, 0));
	SolidBrush sblack(Color(75, 255, 255, 255));
	graphics->FillRectangle(&sblack, pos.X-1, pos.Y-1, boundRect.Width+2, boundRect.Height+2);
	graphics->DrawString(wcs, (int)wcslen(wcs), &font, pos, &brush);
}