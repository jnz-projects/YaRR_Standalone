/*	Renegade Scripts.dll
	Base Class for shaders
	Copyright 2007 Jonathan Wilson, Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class EffectClass;
class ProgrammableShaderClass {
protected:
	bool Loaded;
	float PixelShaderVersion;
	float VertexShaderVersion;
	EffectClass *Effect;
public:
	char *Name;
	unsigned long CRC32;
	char *Filename;
	ProgrammableShaderClass();
	virtual ~ProgrammableShaderClass();
	void Set_Filename(char *filename);
	const char *Get_Name();
	unsigned long Get_Hash();
	void Set_Name(const char *name);
	virtual void Load(ChunkLoadClass& cload);
	virtual void Save(ChunkSaveClass& csave);
#ifndef SDBEDIT
	virtual void Reset_Cache();
	virtual bool Can_Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count);
	virtual void Release_Resources();
	virtual void Reload_Resources();
	virtual void Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count) = 0;
#endif
};

#ifndef SDBEDIT
class ShaderFactory {
public:
	ShaderFactory(unsigned int chunkid);
	unsigned int Chunk_ID;
	virtual ProgrammableShaderClass *Load(ChunkLoadClass& cload) = 0;
};

template <class T> class ShaderRegistrant : public ShaderFactory {
	public:
		ShaderRegistrant(unsigned int c) : ShaderFactory(c)
		{
		}
		ProgrammableShaderClass *Load(ChunkLoadClass& cload);
};

template <class T> ProgrammableShaderClass *ShaderRegistrant<T>::Load(ChunkLoadClass& cload)
{
	ProgrammableShaderClass *shader = new T;
	shader->Load(cload);
	shader->Reload_Resources();
	return shader;
}
#endif

#define CHUNK_SHADER 100
