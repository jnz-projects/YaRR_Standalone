/*	Renegade Scripts.dll
	Normal Map Shader Class for the Shader Database Editor
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <direct.h>
#include "scripts.h"
#include "shadereng.h"
#include <commdlg.h>
#include "shader.h"
#include "editorshader.h"
#include "editorshadermgr.h"
#include "normalmapshader.h"
#include "editornormalmapshader.h"
#include "resource1.h"
#pragma warning(disable: 6031)

float GetDlgItemFloat(HWND hDlg, int id);
BOOL SetDlgItemFloat(HWND hDlg, int id, float f);
EditorNormalMapShaderClass *CurrentShader = 0;

BOOL CALLBACK NormalMapDlgProc(HWND hwnd,UINT Message,WPARAM wParam,LPARAM lParam)
{
	OPENFILENAME ofn;
	char sfile[MAX_PATH] = "";
	char c[MAX_PATH];
	switch(Message)
	{
	case WM_INITDIALOG:
		if (CurrentShader->shader->Filename)
		{
			SetDlgItemText(hwnd,IDC_SHADERFILE,CurrentShader->shader->Filename);
		}
		SetDlgItemFloat(hwnd,IDC_LIGHTCOLORX,CurrentShader->shader->LightColor.X);
		SetDlgItemFloat(hwnd,IDC_LIGHTCOLORY,CurrentShader->shader->LightColor.Y);
		SetDlgItemFloat(hwnd,IDC_LIGHTCOLORZ,CurrentShader->shader->LightColor.Z);
		SetDlgItemFloat(hwnd,IDC_LIGHTCOLORW,CurrentShader->shader->LightColor.W);
		SetDlgItemFloat(hwnd,IDC_AMBCOLORX,CurrentShader->shader->AmbientColor.X);
		SetDlgItemFloat(hwnd,IDC_AMBCOLORY,CurrentShader->shader->AmbientColor.Y);
		SetDlgItemFloat(hwnd,IDC_AMBCOLORZ,CurrentShader->shader->AmbientColor.Z);
		SetDlgItemFloat(hwnd,IDC_AMBCOLORW,CurrentShader->shader->AmbientColor.W);
		SetDlgItemFloat(hwnd,IDC_SURFCOLORX,CurrentShader->shader->SurfaceColor.X);
		SetDlgItemFloat(hwnd,IDC_SURFCOLORY,CurrentShader->shader->SurfaceColor.Y);
		SetDlgItemFloat(hwnd,IDC_SURFCOLORZ,CurrentShader->shader->SurfaceColor.Z);
		SetDlgItemFloat(hwnd,IDC_SURFCOLORW,CurrentShader->shader->SurfaceColor.W);
		SetDlgItemFloat(hwnd,IDC_SPECCOLORX,CurrentShader->shader->SpecularColor.X);
		SetDlgItemFloat(hwnd,IDC_SPECCOLORY,CurrentShader->shader->SpecularColor.Y);
		SetDlgItemFloat(hwnd,IDC_SPECCOLORZ,CurrentShader->shader->SpecularColor.Z);
		SetDlgItemFloat(hwnd,IDC_SPECCOLORW,CurrentShader->shader->SpecularColor.W);
		SetDlgItemFloat(hwnd,IDC_SPECULARPOWER,CurrentShader->shader->SpecularPower);
		SetDlgItemText(hwnd,IDC_NORMAL,CurrentShader->shader->NormalMapFilename);
		CheckDlgButton(hwnd,IDC_SPECULAR,CurrentShader->shader->SpecularEnabled);
		break;
	case WM_CLOSE:
		EndDialog(hwnd, 0);
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			GetDlgItemText(hwnd,IDC_SHADERFILE,sfile,MAX_PATH);
			CurrentShader->shader->Set_Filename(sfile);
			GetDlgItemText(hwnd,IDC_NORMAL,sfile,MAX_PATH);
			CurrentShader->shader->NormalMapFilename = newstr(sfile);
			CurrentShader->shader->LightColor.X = GetDlgItemFloat(hwnd,IDC_LIGHTCOLORX);
			CurrentShader->shader->LightColor.Y = GetDlgItemFloat(hwnd,IDC_LIGHTCOLORY);
			CurrentShader->shader->LightColor.Z = GetDlgItemFloat(hwnd,IDC_LIGHTCOLORZ);
			CurrentShader->shader->LightColor.W = GetDlgItemFloat(hwnd,IDC_LIGHTCOLORW);
			if (CurrentShader->shader->LightColor.X > 1.0)
			{
				CurrentShader->shader->LightColor.X = 1.0;
			}
			if (CurrentShader->shader->LightColor.Y > 1.0)
			{
				CurrentShader->shader->LightColor.Y = 1.0;
			}
			if (CurrentShader->shader->LightColor.Z > 1.0)
			{
				CurrentShader->shader->LightColor.Z = 1.0;
			}
			if (CurrentShader->shader->LightColor.W > 1.0)
			{
				CurrentShader->shader->LightColor.W = 1.0;
			}
			if (CurrentShader->shader->LightColor.X < -1.0)
			{
				CurrentShader->shader->LightColor.X = -1.0;
			}
			if (CurrentShader->shader->LightColor.Y < -1.0)
			{
				CurrentShader->shader->LightColor.Y = -1.0;
			}
			if (CurrentShader->shader->LightColor.Z < -1.0)
			{
				CurrentShader->shader->LightColor.Z = -1.0;
			}
			if (CurrentShader->shader->LightColor.W < -1.0)
			{
				CurrentShader->shader->LightColor.W = -1.0;
			}
			CurrentShader->shader->AmbientColor.X = GetDlgItemFloat(hwnd,IDC_AMBCOLORX);
			CurrentShader->shader->AmbientColor.Y = GetDlgItemFloat(hwnd,IDC_AMBCOLORY);
			CurrentShader->shader->AmbientColor.Z = GetDlgItemFloat(hwnd,IDC_AMBCOLORZ);
			CurrentShader->shader->AmbientColor.W = GetDlgItemFloat(hwnd,IDC_AMBCOLORW);
			if (CurrentShader->shader->AmbientColor.X > 1.0)
			{
				CurrentShader->shader->AmbientColor.X = 1.0;
			}
			if (CurrentShader->shader->AmbientColor.Y > 1.0)
			{
				CurrentShader->shader->AmbientColor.Y = 1.0;
			}
			if (CurrentShader->shader->AmbientColor.Z > 1.0)
			{
				CurrentShader->shader->AmbientColor.Z = 1.0;
			}
			if (CurrentShader->shader->AmbientColor.W > 1.0)
			{
				CurrentShader->shader->AmbientColor.W = 1.0;
			}
			if (CurrentShader->shader->AmbientColor.X < -1.0)
			{
				CurrentShader->shader->AmbientColor.X = -1.0;
			}
			if (CurrentShader->shader->AmbientColor.Y < -1.0)
			{
				CurrentShader->shader->AmbientColor.Y = -1.0;
			}
			if (CurrentShader->shader->AmbientColor.Z < -1.0)
			{
				CurrentShader->shader->AmbientColor.Z = -1.0;
			}
			if (CurrentShader->shader->AmbientColor.W < -1.0)
			{
				CurrentShader->shader->AmbientColor.W = -1.0;
			}
			CurrentShader->shader->SurfaceColor.X = GetDlgItemFloat(hwnd,IDC_SURFCOLORX);
			CurrentShader->shader->SurfaceColor.Y = GetDlgItemFloat(hwnd,IDC_SURFCOLORY);
			CurrentShader->shader->SurfaceColor.Z = GetDlgItemFloat(hwnd,IDC_SURFCOLORZ);
			CurrentShader->shader->SurfaceColor.W = GetDlgItemFloat(hwnd,IDC_SURFCOLORW);
			if (CurrentShader->shader->SurfaceColor.X > 1.0)
			{
				CurrentShader->shader->SurfaceColor.X = 1.0;
			}
			if (CurrentShader->shader->SurfaceColor.Y > 1.0)
			{
				CurrentShader->shader->SurfaceColor.Y = 1.0;
			}
			if (CurrentShader->shader->SurfaceColor.Z > 1.0)
			{
				CurrentShader->shader->SurfaceColor.Z = 1.0;
			}
			if (CurrentShader->shader->SurfaceColor.W > 1.0)
			{
				CurrentShader->shader->SurfaceColor.W = 1.0;
			}
			if (CurrentShader->shader->SurfaceColor.X < -1.0)
			{
				CurrentShader->shader->SurfaceColor.X = -1.0;
			}
			if (CurrentShader->shader->SurfaceColor.Y < -1.0)
			{
				CurrentShader->shader->SurfaceColor.Y = -1.0;
			}
			if (CurrentShader->shader->SurfaceColor.Z < -1.0)
			{
				CurrentShader->shader->SurfaceColor.Z = -1.0;
			}
			if (CurrentShader->shader->SurfaceColor.W < -1.0)
			{
				CurrentShader->shader->SurfaceColor.W = -1.0;
			}
			CurrentShader->shader->SpecularColor.X = GetDlgItemFloat(hwnd,IDC_SPECCOLORX);
			CurrentShader->shader->SpecularColor.Y = GetDlgItemFloat(hwnd,IDC_SPECCOLORY);
			CurrentShader->shader->SpecularColor.Z = GetDlgItemFloat(hwnd,IDC_SPECCOLORZ);
			CurrentShader->shader->SpecularColor.W = GetDlgItemFloat(hwnd,IDC_SPECCOLORW);
			if (CurrentShader->shader->SpecularColor.X > 1.0)
			{
				CurrentShader->shader->SpecularColor.X = 1.0;
			}
			if (CurrentShader->shader->SpecularColor.Y > 1.0)
			{
				CurrentShader->shader->SpecularColor.Y = 1.0;
			}
			if (CurrentShader->shader->SpecularColor.Z > 1.0)
			{
				CurrentShader->shader->SpecularColor.Z = 1.0;
			}
			if (CurrentShader->shader->SpecularColor.W > 1.0)
			{
				CurrentShader->shader->SpecularColor.W = 1.0;
			}
			if (CurrentShader->shader->SpecularColor.X < -1.0)
			{
				CurrentShader->shader->SpecularColor.X = -1.0;
			}
			if (CurrentShader->shader->SpecularColor.Y < -1.0)
			{
				CurrentShader->shader->SpecularColor.Y = -1.0;
			}
			if (CurrentShader->shader->SpecularColor.Z < -1.0)
			{
				CurrentShader->shader->SpecularColor.Z = -1.0;
			}
			if (CurrentShader->shader->SpecularColor.W < -1.0)
			{
				CurrentShader->shader->SpecularColor.W = -1.0;
			}
			CurrentShader->shader->SpecularEnabled = IsDlgButtonChecked(hwnd,IDC_SPECULAR);
			CurrentShader->shader->SpecularPower = GetDlgItemFloat(hwnd,IDC_SPECULARPOWER);
			if (CurrentShader->shader->SpecularPower > 128)
			{
				CurrentShader->shader->SpecularPower = 128;
			}
			if (CurrentShader->shader->SpecularPower < 1)
			{
				CurrentShader->shader->SpecularPower = 1;
			}
			EndDialog(hwnd, 1);
			break;
		case IDCANCEL:
			EndDialog(hwnd, 0);
			break;
		case IDC_OPEN:
			_getcwd(c,MAX_PATH);
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hwnd;
			ofn.lpstrFilter = "Shaders (*.fx)\0*.fx\0All Files (*.*)\0*.*\0";
			ofn.lpstrInitialDir = c;
			ofn.lpstrFile = sfile;
			ofn.lpstrTitle = "Open Shader";
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrDefExt = ".fx";
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_DONTADDTORECENT | OFN_NOCHANGEDIR;
			if (GetOpenFileName(&ofn))
			{
				strcpy(c,&strrchr(sfile,'\\')[1]);
				SetDlgItemText(hwnd,IDC_SHADERFILE,c);
			}
			break;
		}
	default:
		return FALSE;
	}
	return TRUE;
}

EditorNormalMapShaderClass::EditorNormalMapShaderClass()
{
	shader = new NormalMapShaderClass();
}

EditorNormalMapShaderClass::~EditorNormalMapShaderClass()
{
	delete shader;
}

void EditorNormalMapShaderClass::Load(ChunkLoadClass& cload)
{
	shader->Load(cload);
}

void EditorNormalMapShaderClass::Save(ChunkSaveClass& csave)
{
	shader->Save(csave);
}

void EditorNormalMapShaderClass::Edit(HWND ParentDialog)
{
	CurrentShader = this;
	DialogBox(hInst, MAKEINTRESOURCE(IDD_NORMALMAP), ParentDialog,(DLGPROC)NormalMapDlgProc);
	CurrentShader = 0;
}

const char *EditorNormalMapShaderClass::Get_Name()
{
	return shader->Get_Name();
}

void EditorNormalMapShaderClass::Set_Name(const char *name)
{
	shader->Set_Name(name);
}

EditorShaderRegistrant<EditorNormalMapShaderClass> EditorNormalMapShaderRegistrant(SHADER_NORMALMAP,"Normal Map Shader");
