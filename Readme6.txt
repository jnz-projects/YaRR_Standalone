Vloktboky's scripts

**

Name: KAK_Warp_On_Pickup

Description: Attach to a powerup, warps player to location on pickup.

Parameters: Position - Location to warp player.

**

Name: KAK_Precipitation_Create

Description: Creates precipitation when the object the script is attached to is created or destroyed.

Parameters:
	Type - Precipitation to create, can be "Rain", "Snow" or "Ash".
	Density - How much precipitation should be drawn.
	Delay - Time in seconds before precipitation is created.
	Reset_Precipitation - Set to 1 to stop all other forms of precipitation.
	OnCreate - Set to 1 to execute script when object is created.
	OnDestroy - Set to 1 to execute script when object is destroyed.

**

Name: KAK_Precipitation_Zone

Description: Creates precipitation when object enters or leaves the zone script is attached to.

Parameters:
	Type - Precipitation to create, can be "Rain", "Snow" or "Ash". (string)
	Density - How much precipitation should be drawn. (float)
	Delay - Time in seconds before precipitation is created. (float)
	Reset_Precipitation - Set to 1 to stop all other forms of precipitation. (int)
	OnEnter - Set to 1 to execute script when object enters zone. (int)
	OnExit - Set to 1 to execute script when object leaves zone. (int)

**

Name: KAK_Wind_Create

Description: Changes wind levels when object script is attached to is created or destroyed.

Parameters:
	Wind_Heading - Direction wind should blow. Can be any value between 0 and 360. (float)
	Wind_Speed - How fast the wind should blow. (float)
	Wind_Variability - How often wind should blow.  Can be any value between 0 and 1. (float)
	Delay - Time in seconds before wind is created. (float)
	OnCreate - Set to 1 to execute script when object is created. (int)
	OnDestroy - Set to 1 to execute script when object is destroyed. (int)

**

Name: KAK_Wind_Zone

Description: Changes wind levels when object enters or leaves the zone script is attached to.

Parameters:
	Wind_Heading - Direction wind should blow. Can be any value between 0 and 360. (float)
	Wind_Speed - How fast the wind should blow. (float)
	Wind_Variability - How often wind should blow.  Can be any value between 0 and 1, where 1 is the longest time frame. (float)
	Delay - Time in seconds before wind is created. (float)
	OnEnter - Set to 1 to execute script when object enters zone. (int)
	OnExit - Set to 1 to execute script when object leaves zone. (int)

**

Name: KAK_Clouds_Create

Description: Changes cloud settings when object script is attached to is created or destroyed.

Parameters:
	Cloud_Cover - How many clouds should be drawn.  Can be any value between 0 and 1, where 1 is the most clouds drawn. (float)
	Cloud_Gloominess - How dark the clouds should appear.  Can be any value between 0 and 1, where 1 is very dark. (float)
	Delay - Time in seconds before wind is created. (float)
	OnCreate - Set to 1 to execute script when object is created. (int)
	OnDestroy - Set to 1 to execute script when object is destroyed. (int)

**

Name: KAK_Clouds_Zone

Description: Changes cloud settings when object enters or leaves the zone script is attached to.

Parameters:
	Cloud_Cover - How many clouds should be drawn.  Can be any value between 0 and 1, where 1 is the most clouds drawn. (float)
	Cloud_Gloominess - How dark the clouds should appear.  Can be any value between 0 and 1, where 1 is very dark. (float)
	Delay - Time in seconds before wind is created. (float)
	OnEnter - Set to 1 to execute script when object enters zone. (int)
	OnExit - Set to 1 to execute script when object leaves zone. (int)

**

Name: KAK_Lightning_Create

Description: Creates lightning when object script is attached to is created or destroyed.

Parameters:
	Lightning_Intensity - How strong the lightning should be.  Can be any value between 0 and 1, where 1 is the most intense. (float)
	Start_Distance - How close can the lightning be drawn.  Can be any value between 0 and 1, where 1 is the farthest. (float)
	End_Distance - How far can the lightning be drawn.  Can be any value between 0 and 1, where 1 is the farthest. (float)
	Lightning_Heading - The direction the lightning should be drawn.  Can be any value between 0 and 360. (float)
	Lightning_Distribution - How often the lightning should be drawn.  Can be any value between 0 and 1, where 1 is the longest time frame. (float)
	Delay - Time in seconds before wind is created. (float)
	OnCreate - Set to 1 to execute script when object is created. (int)
	OnDestroy - Set to 1 to execute script when object is destroyed. (int)

**

Name: KAK_Lightning_Zone

Description: Creates lightning when object enters or leaves the zone script is attached to.

Parameters:
	Lightning_Intensity - How strong the lightning should be.  Can be any value between 0 and 1, where 1 is the most intense. (float)
	Start_Distance - How close can the lightning be drawn.  Can be any value between 0 and 1, where 1 is the farthest. (float)
	End_Distance - How far can the lightning be drawn.  Can be any value between 0 and 1, where 1 is the farthest. (float)
	Lightning_Heading - The direction the lightning should be drawn.  Can be any value between 0 and 360. (float)
	Lightning_Distribution - How often the lightning should be drawn.  Can be any value between 0 and 1, where 1 is the longest time frame. (float)
	Delay - Time in seconds before wind is created. (float)
	OnEnter - Set to 1 to execute script when object enters zone. (int)
	OnExit - Set to 1 to execute script when object leaves zone. (int)

**

Name: KAK_Flowing_Weather

Description: Creates dynamic weather on the map.  Cloud level, wind, rain/snow/ash, and lightning are controlled to simulate real flowing weather.

Parameters:
	Precipitation_Type - Type of precipitation to use.  Can be "Rain", "Snow" or "Ash". (string)
	Minimum_Precipitation_Start - Area on line graph to start drawing the precipitation.  Can be any value between 0 and 1.  Suggested value: 0.500. (float)
	Max_Precipitation_Density - How much precipitation should be drawn at the maximum peek.  Suggested value: 5. (float)
	Minimum_Lightning_Start - Area on line graph to start drawing lightning.  Can be any value between 0 and 1.  Suggested value: 0.850. (float)
	Maximum_Lightning_Intensity - How strong should the lightning be at the maximum peek.  Can be any value between 0 and 1.  Suggested value: 1. (float)
	Wind_Variablity - What value should the wind speed change based off the current cloud level.  Suggested value: 10. (float)
	Speed - How often (in seconds) should the weather be changed.  Suggested value: 0.010. (float)

Notes: To disable precipitation or lightning, set the Minimum_Precipitation_Start and Minimum_Lightning_Start to 1.
