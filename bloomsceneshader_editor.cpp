/*	Renegade Scripts.dll
	SimpleSceneShaderEditorClass
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include <direct.h>
#include "scripts.h"
#include "shadereng.h"
#include <commdlg.h>
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader_scene.h"
#include "scene_shader_editor.h"
#include "bloomsceneshader.h"
#include "bloomsceneshader_editor.h"
#include "resource1.h"
#pragma warning(disable: 6031)

SceneShaderEditorClass* BloomSceneShaderClass::GetEditor()
{
	return new BloomSceneShaderEditor(this);
}

BloomSceneShaderClass *BloomSceneShaderEditor::CurrentShader;

BOOL CALLBACK BloomSceneShaderDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	OPENFILENAME ofn;
	char sfile[MAX_PATH] = "";
	char c[MAX_PATH];
	switch(Message)
	{
	case WM_INITDIALOG:
		if (BloomSceneShaderEditor::CurrentShader->FXFilename)
		{
			SetDlgItemText(hwnd,IDC_SHADERFILE,BloomSceneShaderEditor::CurrentShader->FXFilename);
		}
		SetDlgItemInt(hwnd,IDC_UID,BloomSceneShaderEditor::CurrentShader->UID,false);
		SetDlgItemFloat(hwnd,IDC_SCALE,BloomSceneShaderEditor::CurrentShader->BloomScale);
		break;
	case WM_CLOSE:
		EndDialog(hwnd, 0);
		break;
	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:
			GetDlgItemText(hwnd,IDC_SHADERFILE,sfile,MAX_PATH);
			BloomSceneShaderEditor::CurrentShader->SetFXFilename(newstr(sfile));
			BloomSceneShaderEditor::CurrentShader->UID = GetDlgItemInt(hwnd,IDC_UID,NULL,false);
			BloomSceneShaderEditor::CurrentShader->BloomScale = GetDlgItemFloat(hwnd,IDC_SCALE);
			EndDialog(hwnd, 1);
			break;
		case IDCANCEL:
			EndDialog(hwnd, 0);
			break;
		case IDC_OPEN:
			_getcwd(c,MAX_PATH);
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hwnd;
			ofn.lpstrFilter = "Shaders (*.fx)\0*.fx\0All Files (*.*)\0*.*\0";
			ofn.lpstrInitialDir = c;
			ofn.lpstrFile = sfile;
			ofn.lpstrTitle = "Open Shader";
			ofn.nMaxFile = MAX_PATH;
			ofn.lpstrDefExt = ".fx";
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_DONTADDTORECENT | OFN_NOCHANGEDIR;
			if (GetOpenFileName(&ofn))
			{
				strcpy(c,&strrchr(sfile,'\\')[1]);
				SetDlgItemText(hwnd,IDC_SHADERFILE,c);
			}
			break;
		}
		break;
	default:
		return FALSE;
	}
	return TRUE;
}

void BloomSceneShaderEditor::Edit(HWND ParentDialog)
{
	BloomSceneShaderEditor::CurrentShader = (BloomSceneShaderClass *)this->Shader;
	DialogBox(hInst, MAKEINTRESOURCE(IDD_BLOOMPPF), ParentDialog,BloomSceneShaderDlgProc);
	BloomSceneShaderEditor::CurrentShader = 0;
}
