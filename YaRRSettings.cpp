#include "YaRRIncludes.h"


/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


void YaRRSettings::Load()
{
	
	DLOG;
	ObjectWorthList = Alloc(Stacker<ObjectWorth>);

	IRCd = Alloc(IRCData);
	ModeratorList = Alloc(Stacker<Moderator>);
	char basepath[256];
	GetCurrentDirectory(64, basepath);
	strcat(basepath, "\\config\\IRC.ini");

	*Medals::ArmorDestroyer_Name = 0;
	*Medals::ArmsDestroyer_Name = 0;
	*Medals::Killer_Name = 0;
	*Medals::Sabatage_Name = 0;
	*Medals::BeaconSniffer_Name = 0;
	*Medals::Demolition_Name = 0;

	LoadGlob();
	LoadIRC(basepath);
	
}

void YaRRSettings::Unload()
{
	DLOG;
	if(IRCd)
	{
		ChannelData **c;
		IRCd->Channels->Reset();
		CDealloc(IRCd->Host);
		CDealloc(IRCd->Backup_Nick);
		CDealloc(IRCd->Nick);
		CDealloc(IRCd->NSPassword);
		while(IRCd->Channels->Iterate(&c))
		{
			CDealloc((*c)->Name);
			CDealloc((*c)->Key);
			Dealloc(ChannelData, *c);
		}
		DeleteList(IRCd->ExtraCommands);
		Dealloc(Stacker<char *>, IRCd->ExtraCommands);
		Dealloc(IRCData, IRCd);
	}

	DeleteList(AnnounceList);

	
	Dealloc(Stacker<char *>, AnnounceList);
	AnnounceList = 0;
	
	IRCd = 0;
	Dealloc(Stacker<Moderator>, ModeratorList);
	ModeratorList = 0;
	if(Sounds)
	{
		Dealloc(Stacker<Sound>, Sounds);
		Sounds = 0;
	}
	for(int i = 0; i < 100; i++)
	{
		if(SlotList[i] == 0)
		{
			break;
		}
		else
		{
			CDealloc((void *)SlotList[i]);
		}
	}
	Dealloc(Stacker<ObjectWorth>, ObjectWorthList);

	if(PremotionNames)
	{
		IterateStack(x, PromoteInfo, PremotionNames)
		{
			DeleteList((void *)x.Powerups);
			Dealloc(Stacker<char *>, x.Powerups);
		}
		Dealloc(Stacker<PromoteInfo>, PremotionNames);
		PremotionNames = 0;
	}

}

void YaRRSettings::MapLoad(const char *Map)
{
	DLOG;
	if(!NodDefences)
	{
		NodDefences = Alloc(Stacker<Vector3>);
	}
	else
	{
		NodDefences->Clear();
	}

	if(!GDIDefences)
	{
		GDIDefences = Alloc(Stacker<Vector3>);
	}
	else
	{
		GDIDefences->Clear();
	}


	char buffer[128];
	GetCurrentDirectory(50, buffer);
	strcat(buffer, "\\config\\YaRR.ini");

	INIClass *ini = Get_INI(buffer);

	if(!ini)
	{
		printf("Unable to load new map settings from YaRR.ini\n");
		return;
	}

	
	char nod[256];
	ini->Get_String(Map, "NodDefence", "ERR", nod, 256);
	if(strcmp(nod, "ERR") == 0)
	{
		printf("Unable to load \"NodDefence\" from [%s]\n", Map);
	}

	char gdi[256];
	ini->Get_String(Map, "GDIDefence", "ERR", gdi, 256);
	if(strcmp(gdi, "ERR") == 0)
	{
		printf("Unable to load \"GDIDefence\" from [%s]\n", Map);
	}


	Stacker<char *> Tokens;
	Explode(nod, ' ', &Tokens);

	IterateStack(raw, char *, (&Tokens))
	{
		float x,y,z;
		if(sscanf(raw, "(%f,%f,%f)", &x, &y, &z) != 3)
		{
			printf("Malformed defence string \"%s\"\n", raw);
		}
		else
		{
			Vector3 pos(x, y, z);
			NodDefences->Push(pos);
			CDealloc(raw);
		}
	}

	Tokens.Clear();

	Explode(gdi, ' ', &Tokens);

	IterateStack(raw2, char *, (&Tokens))
	{
		float x,y,z;
		if(sscanf(raw2, "(%f,%f,%f)", &x, &y, &z) != 3)
		{
			printf("Malformed defence string \"%s\"\n", raw2);
		}
		else
		{
			Vector3 pos(x, y, z);
			GDIDefences->Push(pos);
			CDealloc(raw2);
		}
	}

	ini->Get_String(Map, "Rules", "None", MapRules, 256);


	Release_INI(ini);
}

void YaRRSettings::LoadGlob()
{
	DLOG;
	char buffer[128];
	GetCurrentDirectory(50, buffer);
	strcat(buffer, "\\config\\YaRR.ini");

	INIClass *ini = Get_INI(buffer);
	if(!ini)
	{
		YaRRError("Unable to load global settings from YaRR.ini");
		return;
	}
	if(!AnnounceList)
	{
		AnnounceList = Alloc(Stacker<char *>);
	}

	for(int i = 1;; i++)
	{
		char tmp[8];
		sprintf(tmp, "%.2d", i);
		char buf[256];
		ini->Get_String("AutoAnnounce", tmp, "ERR", buf, 256);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}
		AnnounceList->Push(strdup2(buf));
	}

	AnnounceTimeout = ini->Get_Int("AutoAnnounce", "Time", 60);

	if(!ModeratorList)
	{
		ModeratorList = Alloc(Stacker<Moderator>);
	}

	for(int i = 1;; i++)
	{
		char tmp[8];
		sprintf(tmp, "%.2d", i);
		char buf[256];
		ini->Get_String("Moderators", tmp, "ERR", buf, 256);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}
		
		Stacker<char *> tok;
		Explode(buf, ' ', &tok);
		if(tok.Length() < 3)
		{
			printf("Error loading moderator #%.2d syntax error: <Game nick> <IRC nick> <access>\n", i); 
			DeleteList(&tok);
			continue;
		}
		
		(tok[2])[1] = '\0';

		if(strcspn(tok[2], "+%@&~") != 0)
		{
			printf("Error loading moderator #%.2d invalid access. Valid characters are: + % @ & ~\n", i); 
			DeleteList(&tok);
			continue;
		}

		Moderator m;
		strcpy(m.Nick, tok[0]);
		strcpy(m.IRCNick, tok[1]);
		m.Access = *(tok[2]);
		ModeratorList->Push(m);
		DeleteList(&tok);
	}
	
	DefenceChance = ini->Get_Int("YaRR", "DefenceChance", 0);
	Minimum_BHS = ini->Get_Float("YaRR", "Require_scripts", 0.0);

	char hexc[64];
	ini->Get_String("colour", "Message_Colour", "0000FF", hexc, 64);
	HexToRGB(&MessageColour, hexc);

	ini->Get_String("colour", "Message_Colour_Error", "FF0000", hexc, 64);
	HexToRGB(&MessageColourError, hexc);

	ini->Get_String("YaRR", "PPage_Sound", "yo1.wav", PPageSound, 64);

	NoPending = ini->Get_Bool("YaRR", "No_Pending", 0);
	StartButtonFix = ini->Get_Bool("YaRR", "Start_Button_Fix", 0);

	ini->Get_String("YaRR", "Rules", "None", GlobRules, 256);
	ini->Get_String("YaRR", "Website", "None", Website, 128);

	YaRRSettings::SuicideAllowed = ini->Get_Bool("YaRR", "Suicide_Allowed", 0);
	YaRRSettings::SuicideTakeMoney = ini->Get_Bool("YaRR", "Suicide_Takes_Money", 0);
	ini->Get_String("YaRR", "Suicide_Message", "", YaRRSettings::SuicideMessage, 256);

	Ban_Rooted = ini->Get_Bool("YaRR", "Ban_Rooted", 0);
	Stop_Output = ini->Get_Bool("YaRR", "Stop_Output", 0);


	memchr((void *)SlotList, 0, 100);
	for(int _i = 1; _i < 100; _i++)
	{
		char tmp[8];
		sprintf(tmp, "Slot%.2d", _i);
		char buf[256];
		ini->Get_String("YaRR", tmp, "ERR", buf, 256);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}
		SlotList[_i-1] = strdup2(buf);
		Slots++;
	}

	for(int _i = 1;; _i++)
	{
		char tmp[8];
		sprintf(tmp, "%.2d", _i);
		char buf[256];
		ini->Get_String("ObjectWorth", tmp, "ERR", buf, 256);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}
		
		Stacker<char *> Token;
		Explode(buf, ' ', &Token);
		
		if(Token.Length() < 2)
		{
			DeleteList((void *)&Token);
			YaRRFunctions::YaRRError("ObjectWorth syntax: xx = <Preset> <Worth>");
		}
		else
		{
			if(atoi(Token[1]) == 0)
			{
				YaRRFunctions::YaRRError("Objectworth: Invalid <Worth> for an item. It cannot be 0");
			}

			ObjectWorth o;
			strcpy(o.Preset, Token[0]);
			o.Worth = atoi(Token[1]);
			ObjectWorthList->Push(o);
		}
		DeleteList((void *)&Token);
	}



	Release_INI(ini);

	GetCurrentDirectory(50, buffer);
	strcat(buffer, "\\config\\Sounds.ini");
	
	ini = Get_INI(buffer);
	if(!ini)
	{
		return;
	}

	if(!Sounds)
	{
		Sounds = Alloc(Stacker<Sound>);
	}

	for(int _i = 1;; _i++)
	{
		char tmp[8];
		sprintf(tmp, "%.3d", _i);
		char buf[256];
		ini->Get_String("sounds", tmp, "ERR", buf, 256);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}

		Stacker<char *> tok;
		Explode(buf, ' ', &tok);
		if(tok.Length() < 2)
		{
			printf("[YaRR]: Malformed sound string %.3d \"%s\".\n", _i, buf);
			DeleteList(&tok);
			continue;
		}
		
		Sound s;
		strcpy(s.Trigger, tok[0]);
		strcpy(s.File, tok[1]);
		DeleteList(&tok);

		Sounds->Push(s);
	}
	
	LoadSound(&Award_Killer_Sound, "Award_Killer_Sound", ini);
	LoadSound(&Award_ArmsDestroyer_Sound, "Award_ArmsDestroyer_Sound", ini);
	LoadSound(&Award_ArmorDestroyer_Sound, "Award_ArmorDestroyer_Sound", ini);
	LoadSound(&Award_Demolition_Sound, "Award_Demolition_Sound", ini);
	LoadSound(&Award_Sabatage_Sound, "Award_Sabatage_Sound", ini);
	LoadSound(&Award_BeaconSniffer_Sound, "Award_BeaconSniffer_Sound", ini);

	LoadSound(&Medal_Killer_Sound, "Medal_Killer_Sound", ini);
	LoadSound(&Medal_ArmsDestroyer_Sound, "Medal_ArmsDestroyer_Sound", ini);
	LoadSound(&Medal_ArmorDestroyer_Sound, "Medal_ArmorDestroyer_Sound", ini);
	LoadSound(&Medal_Demolition_Sound, "Medal_Demolition_Sound", ini);
	LoadSound(&Medal_Sabatage_Sound, "Medal_Sabatage_Sound", ini);
	LoadSound(&Medal_BeaconSniffer_Sound, "Medal_BeaconSniffer_Sound", ini);

	Release_INI(ini);

	GetCurrentDirectory(50, buffer);
	strcat(buffer, "\\config\\Ranks.ini");
	
	ini = Get_INI(buffer);

	RankInfo::Player_Damage_CoEfficient = ini->Get_Float("CoEfficient", "Player_Damage_CoEfficient", (float)0.3);
	RankInfo::Vehicle_Damage_CoEfficient = ini->Get_Float("CoEfficient", "Vehicle_Damage_CoEfficient", (float)0.3);
	RankInfo::Building_Damage_CoEfficient = ini->Get_Float("CoEfficient", "Building_Damage_CoEfficient", (float)0.2);
	RankInfo::Beacon_Damage_CoEfficient = ini->Get_Float("CoEfficient", "Beacon_Damage_CoEfficient", (float)1.0);
	RankInfo::C4_Damage_CoEfficient = ini->Get_Float("CoEfficient", "C4_Damage_CoEfficient", (float)1.0);
	RankInfo::Player_Repair_CoEfficient = ini->Get_Float("CoEfficient", "Player_Repair_CoEfficient", (float)0.25);
	RankInfo::Vehicle_Repair_CoEfficient = ini->Get_Float("CoEfficient", "Vehicle_Repair_CoEfficient", (float)0.4);
	RankInfo::Building_Repair_CoEfficient = ini->Get_Float("CoEfficient", "Building_Repair_CoEfficient", (float)0.15);

	LoadSoundSettings(&Medals::Killer_Threshold, Medals::Killer_Name, "Killer", "0;Killer", "Medals", ini);
	LoadSoundSettings(&Medals::ArmsDestroyer_Threshold, Medals::ArmsDestroyer_Name, "ArmsDestroyer", "0;ArmsDestroyer", "Medals", ini);
	LoadSoundSettings(&Medals::ArmorDestroyer_Threshold, Medals::ArmorDestroyer_Name, "ArmorDestroyer", "0;ArmorDestroyer", "Medals", ini);
	LoadSoundSettings(&Medals::Demolition_Threshold, Medals::Demolition_Name, "Demolition", "0;Demolition", "Medals", ini);
	LoadSoundSettings(&Medals::Sabatage_Threshold, Medals::Sabatage_Name, "Sabatage", "0;Sabatage", "Medals", ini);
	LoadSoundSettings(&Medals::BeaconSniffer_Threshold, Medals::BeaconSniffer_Name, "BeaconSniffer", "0;BeaconSniffer", "Medals", ini);

	LoadSoundSettings(&Awards::Killer_Threshold, Awards::Killer_Name, "Killer", "0;Killer", "Awards", ini);
	LoadSoundSettings(&Awards::ArmsDestroyer_Threshold, Awards::ArmsDestroyer_Name, "ArmsDestroyer", "0;ArmsDestroyer", "Awards", ini);
	LoadSoundSettings(&Awards::ArmorDestroyer_Threshold, Awards::ArmorDestroyer_Name, "ArmorDestroyer", "0;ArmorDestroyer", "Awards", ini);
	LoadSoundSettings(&Awards::Demolition_Threshold, Awards::Demolition_Name, "Demolition", "0;Demolition", "Awards", ini);
	LoadSoundSettings(&Awards::Sabatage_Threshold, Awards::Sabatage_Name, "Sabatage", "0;Sabatage", "Awards", ini);
	LoadSoundSettings(&Awards::BeaconSniffer_Threshold, Awards::BeaconSniffer_Name, "BeaconSniffer", "0;BeaconSniffer", "Awards", ini);
	LoadSoundSettings(&Awards::Medals_Threshold, Awards::Medals_Name, "Medals", "0;Medals", "Awards", ini);

	if(!PremotionNames)
	{
		PremotionNames = Alloc(Stacker<PromoteInfo>);
	}

	for(int _i = 1;; _i++)
	{
		char tmp[8];
		sprintf(tmp, "%.2d", _i);
		char buf[128];

		ini->Get_String("Veteran", tmp, "ERR", buf, 128);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}
		PromoteInfo pre;
		pre.Health = ini->Get_Int(buf, "Health", 0);
		pre.Armor = ini->Get_Int(buf, "Armor", 0);
		pre.Regeneration = ini->Get_Int(buf, "Regeneration", 0);
		pre.Powerups = Alloc(Stacker<char *>);
		pre.Level = _i;
		strcpy(pre.Name, buf);

		printf("%d %d %d %s\n", pre.Health, pre.Armor, pre.Regeneration, pre.Name);


		for(int _y = 1;; _y++)
		{
			char tmp2[8];
			sprintf(tmp2, "%.2d", _y);
			char buf2[256];

			ini->Get_String(buf, tmp2, "ERR", buf2, 256);
			if(strcmp(buf2, "ERR") == 0)
			{
				break;
			}

			pre.Powerups->Push(strdup2(buf2));
		}


		PremotionNames->Push(pre);
		MaxVeteranLevel++;
	}
	

	Release_INI(ini);

}

void YaRRSettings::UnloadGlob()
{
	DLOG;
	if(PremotionNames)
	{
		IterateStack(x, PromoteInfo, PremotionNames)
		{
			DeleteList((void *)x.Powerups);
			Dealloc(Stacker<char *>, x.Powerups);
		}
		Dealloc(Stacker<PromoteInfo>, PremotionNames);
		PremotionNames = 0;
	}


	IterateStack(anl, char *, AnnounceList)
	{
		CDealloc(anl);
	}
	Dealloc(Stacker<char *>, AnnounceList);
	AnnounceList = 0;

	Dealloc(Stacker<Moderator>, ModeratorList);
	ModeratorList = 0;

	if(Sounds)
	{
		Dealloc(Stacker<Sound>, Sounds);
		Sounds = 0;
	}

	if(Award_Killer_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_Killer_Sound);
		Award_Killer_Sound = 0;
	}

	if(Award_ArmsDestroyer_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_ArmsDestroyer_Sound);
		Award_ArmsDestroyer_Sound = 0;
	}

	if(Award_ArmorDestroyer_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_ArmorDestroyer_Sound);
		Award_ArmorDestroyer_Sound = 0;
	}

	if(Award_Demolition_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_Demolition_Sound);
		Award_Demolition_Sound = 0;
	}

	if(Award_Sabatage_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_Sabatage_Sound);
		Award_Sabatage_Sound = 0;
	}

	if(Award_BeaconSniffer_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_BeaconSniffer_Sound);
		Award_BeaconSniffer_Sound = 0;
	}

	if(Award_Medals_Sound)
	{
		Dealloc(Stacker<WavSound>, Award_Medals_Sound);
		Award_Medals_Sound = 0;
	}

	if(Medal_Killer_Sound)
	{
		Dealloc(Stacker<WavSound>, Medal_Killer_Sound);
		Medal_Killer_Sound = 0;
	}

	if(Medal_ArmsDestroyer_Sound)
	{
		Dealloc(Stacker<WavSound>, Medal_ArmsDestroyer_Sound);
		Medal_ArmsDestroyer_Sound = 0;
	}

	if(Medal_ArmorDestroyer_Sound)
	{
		Dealloc(Stacker<WavSound>, Medal_ArmorDestroyer_Sound);
		Medal_ArmorDestroyer_Sound = 0;
	}

	if(Medal_Demolition_Sound)
	{
		Dealloc(Stacker<WavSound>, Medal_Demolition_Sound);
		Medal_Demolition_Sound = 0;
	}

	if(Medal_Sabatage_Sound)
	{
		Dealloc(Stacker<WavSound>, Medal_Sabatage_Sound);
		Medal_Sabatage_Sound = 0;
	}

	if(Medal_BeaconSniffer_Sound)
	{
		Dealloc(Stacker<WavSound>, Medal_BeaconSniffer_Sound);
		Medal_BeaconSniffer_Sound = 0;
	}
}



void YaRRSettings::LoadIRC(const char *ini)
{
	DLOG;
	INIClass *IRCini = Get_INI(ini);
	if(!IRCini)
	{
		printf("Could not load: %s\n", ini);
		return;
	}

	IRCData *irc = Alloc(IRCData);
	irc->Channels = Alloc(Stacker<ChannelData *>);
	irc->ExtraCommands = Alloc(Stacker<char *>);

	char tmp[128];

	IRCini->Get_String("IRC", "Host", "ERR", tmp, 128); 
	if(strcmp(tmp, "ERR") == 0)
	{
		printf("Couldn't load %s, \"Host\" not specified.\n", ini);
		return;
	}

	irc->Host = strdup2(tmp);

	int port = IRCini->Get_Int("IRC", "Port", -1);
	if(port > 65000 || port < 1)
	{
		printf("Couldn't load %s, \"Port\" was invalid or not specified.\n", ini);
		return;
	}

	irc->Port = port;

	IRCini->Get_String("IRC", "NSPassword", "ERR", tmp, 128);
	if(strcmp(tmp, "ERR") == 0)
	{
		printf("Couldn't load %s, \"NSPassword\" not specified.\n", ini);
		return;
	}
	
	irc->NSPassword = strdup2(tmp);

	IRCini->Get_String("IRC", "Nick", "ERR", tmp, 128);
	if(strcmp(tmp, "ERR") == 0)
	{
		printf("Couldn't load %s, \"Nick\" not specified.\n", ini);
		return;
	}
	
	irc->Nick = strdup2(tmp);

	IRCini->Get_String("IRC", "Backup_Nick", "ERR", tmp, 128);
	if(strcmp(tmp, "ERR") == 0)
	{
		printf("Couldn't load %s, \"Backup_Nick\" not specified.\n", ini);
		return;
	}
	
	irc->Backup_Nick = strdup2(tmp);

	char key[16];
	for(int i = 1; i < 64; i++)
	{
		sprintf(key, "C%02d", i);
		IRCini->Get_String("IRC", key, "ERR", tmp, 128);
		if(strcmp(tmp, "ERR") == 0)
		{
			break;
		}
		Stacker<char *> Tokens;
		Explode(tmp, ' ', &Tokens);

		Tokens.Reset();
		const char *Name = *Tokens.Iterate();
		const char *Key = *Tokens.Iterate();
		const char *Level = *Tokens.Iterate();

		if(!Name || !Key || !Level || (tolower(*Level) != 'p' && tolower(*Level) != 'a'))
		{
			printf("Couldn't load %s, a channel string (C%02d) was invalid. <Channel> <Key|None> <Access|P|N> [Threshold:~ & @ % +]\n", ini, i);
			DeleteList(&Tokens);
			continue;
		}
		
		ChannelData *c = (ChannelData *)CAlloc(sizeof(ChannelData));
		c->Name = strdup2(Name);
		c->Key = strdup2(Key);
		c->Access = *Level;
		//c->Threshold = *Threshold;
		DeleteList(&Tokens);
		irc->Channels->Push(c);
	}

	for(int i = 1; i < 64; i++)
	{
		sprintf(key, "EXC%02d", i);
		IRCini->Get_String("IRC", key, "ERR", tmp, 128);
		if(strcmp(tmp, "ERR") == 0)
		{
			break;
		}
		irc->ExtraCommands->Push(strdup2(tmp));
	}

	Release_INI(IRCini);

	if(irc->Channels->Empty())
	{
		printf("Couldn't load %s, no channels to join!\n", ini);

	}
	IRCd = irc;
}

char YaRRSettings::GetChannelType(const char *Channel)
{
	DLOG;
	IRCd->Channels->Reset();
	ChannelData *c;
	while(IRCd->Channels->Iterate(&c))
	{
		if(stricmp(c->Name, Channel) == 0)
		{
			return c->Access;
		}
	}
	return 0;
}

bool YaRRSettings::GetCommandInfo(const char *Command, char *Buffer, int Size, char *Access)
{
	DLOG;
	char basepath[256];
	GetCurrentDirectory(64, basepath);
	strcat(basepath, "\\config\\YaRR.ini");
	INIClass *ini = Get_INI(basepath);
	if(!ini)
	{
		return 0;
	}
	char tmp[32];

	ini->Get_String(Command, "Trigger", "ERR", Buffer, Size);
	ini->Get_String(Command, "Access", "ERR", tmp, 32);
	tmp[1] = 0;
	
	Release_INI(ini);
	if(strcspn(tmp, "+%@&~") != 0)
	{
		*Access = 0;
	}
	else
	{
		*Access = *tmp;
	}
	return (bool)strcmp(Buffer, "ERR");
}

bool YaRRSettings::GetPlayerInGameNick(const char *nick, char *Buffer)
{
	DLOG;
	IterateStack(m, Moderator, ModeratorList)
	{
		if(strcmp(m.IRCNick, nick) == 0)
		{
			strcpy(Buffer, m.Nick);
			return 1;
		}
	}
	return 0;
}

void YaRRSettings::Change(const char *Section, const char *Entry, const char *Data)
{
	DLOG;
	char basepath[256];
	GetCurrentDirectory(64, basepath);
	strcat(basepath, "\\config\\YaRR.ini");

	WritePrivateProfileString(Section, Entry, Data, basepath);
}

void YaRRSettings::LoadSound(Stacker<WavSound> **Stack, const char *Name, INIClass *ini)
{
	if(!*Stack)
	{
		*Stack = Alloc(Stacker<WavSound>);
	}
	for(int _i = 1;; _i++)
	{
		char tmp[8];
		sprintf(tmp, "%.3d", _i);
		char buf[256];
		ini->Get_String(Name, tmp, "ERR", buf, 256);
		if(strcmp(buf, "ERR") == 0)
		{
			break;
		}

		WavSound s;
		char pubt[128];
		if(sscanf(buf, "%s %f %s", s.Name, &s.Duration, pubt) != 3)
		{
			printf("Malformed sound string \"%s\" #%.3d in Sounds.ini\n", Name, _i);
			break;
		}
		if(stricmp(pubt, "yes") == 0 || stricmp(pubt, "1") == 0)
		{
			s.Public = 1;
		}
		else if(stricmp(pubt, "no") == 0 || stricmp(pubt, "0") == 0)
		{
			s.Public = 0;
		}
		else
		{
			s.Public = 0;
		}	
		(*Stack)->Push(s);		
	}
}

void YaRRSettings::LoadSoundSettings(int *Threshold, char *Name, const char *SoundName, const char *Default, const char *Section, INIClass *ini)
{
	char temp[256];
	Stacker<char *> tok;
	ini->Get_String(Section, SoundName, Default, temp, 256);
	Explode(temp, '/', &tok);
	if(tok.Length() < 2)
	{
		*Threshold = 0;
		strcpy(Name, SoundName);
	}
	else
	{
		*Threshold = atoi(*tok.At(0));
		strcpy(Name, *tok.At(1));
	}
	DeleteList((void *)&tok);
}

const char *YaRRSettings::GetPremotionName(int Level)
{
	if(PremotionNames->Length() <= Level)
	{
		return "ERROR";
	}

	return PremotionNames->At(Level)->Name;
}

const PromoteInfo *YaRRSettings::GetPremotionInfo(int Level)
{
	if(PremotionNames->Length() <= Level)
	{
		return 0;
	}

	return PremotionNames->At(Level);
}


Stacker<char *> *YaRRSettings::AnnounceList = 0;
IRCData *YaRRSettings::IRCd = 0;
int YaRRSettings::AnnounceTimeout;
Stacker<Moderator> *YaRRSettings::ModeratorList = 0;
Stacker<Vector3> *YaRRSettings::NodDefences = 0;
Stacker<Vector3> *YaRRSettings::GDIDefences = 0;
Stacker<PromoteInfo> *YaRRSettings::PremotionNames = 0;
int YaRRSettings::DefenceChance = 0;
float YaRRSettings::Minimum_BHS = 0;
bool YaRRSettings::NoPending = 0;
bool YaRRSettings::StartButtonFix = 0;
char YaRRSettings::GlobRules[256];
char YaRRSettings::MapRules[256];
int YaRRSettings::Slots = 0;
char *YaRRSettings::SlotList[100];
char YaRRSettings::Website[128];
char YaRRSettings::CrateModel[128];
Stacker<Sound> *YaRRSettings::Sounds = 0;
Colour::CRGB YaRRSettings::MessageColour;
Colour::CRGB YaRRSettings::MessageColourError;
char YaRRSettings::PPageSound[64];
Stacker<ObjectWorth> *YaRRSettings::ObjectWorthList = 0;
unsigned int YaRRSettings::ObjectWorthDefault = 1;
int YaRRSettings::MaxVeteranLevel = 0;
bool YaRRSettings::Ban_Rooted = 0;
bool YaRRSettings::Stop_Output = 1;

int YaRRSettings::Medals::Killer_Threshold = 0;
int YaRRSettings::Medals::ArmsDestroyer_Threshold = 0;
int YaRRSettings::Medals::ArmorDestroyer_Threshold = 0;
int YaRRSettings::Medals::Demolition_Threshold = 0;
int YaRRSettings::Medals::Sabatage_Threshold = 0;
int YaRRSettings::Medals::BeaconSniffer_Threshold = 0;
bool YaRRSettings::SuicideAllowed = 0;
bool YaRRSettings::SuicideTakeMoney = 0;
char YaRRSettings::SuicideMessage[256];

char YaRRSettings::Medals::Killer_Name[256];
char YaRRSettings::Medals::ArmsDestroyer_Name[256];
char YaRRSettings::Medals::ArmorDestroyer_Name[256];
char YaRRSettings::Medals::Demolition_Name[256];
char YaRRSettings::Medals::Sabatage_Name[256];
char YaRRSettings::Medals::BeaconSniffer_Name[256];

int YaRRSettings::Awards::Killer_Threshold = 0;
int YaRRSettings::Awards::ArmsDestroyer_Threshold = 0;
int YaRRSettings::Awards::ArmorDestroyer_Threshold = 0;
int YaRRSettings::Awards::Demolition_Threshold = 0;
int YaRRSettings::Awards::Sabatage_Threshold = 0;
int YaRRSettings::Awards::BeaconSniffer_Threshold = 0;
int YaRRSettings::Awards::Medals_Threshold = 0;

char YaRRSettings::Awards::Killer_Name[256];
char YaRRSettings::Awards::ArmsDestroyer_Name[256];
char YaRRSettings::Awards::ArmorDestroyer_Name[256];
char YaRRSettings::Awards::Demolition_Name[256];
char YaRRSettings::Awards::Sabatage_Name[256];
char YaRRSettings::Awards::BeaconSniffer_Name[256];
char YaRRSettings::Awards::Medals_Name[256];

Stacker<WavSound> *YaRRSettings::Award_Killer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Award_ArmsDestroyer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Award_ArmorDestroyer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Award_Demolition_Sound = 0;
Stacker<WavSound> *YaRRSettings::Award_Sabatage_Sound = 0;
Stacker<WavSound> *YaRRSettings::Award_BeaconSniffer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Award_Medals_Sound = 0;

Stacker<WavSound> *YaRRSettings::Medal_Killer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Medal_ArmsDestroyer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Medal_ArmorDestroyer_Sound = 0;
Stacker<WavSound> *YaRRSettings::Medal_Demolition_Sound = 0;
Stacker<WavSound> *YaRRSettings::Medal_Sabatage_Sound = 0;
Stacker<WavSound> *YaRRSettings::Medal_BeaconSniffer_Sound = 0;
