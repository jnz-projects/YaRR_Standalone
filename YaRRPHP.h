

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRPHP : public YaRRLink
{
	static GdiplusStartupInput strGDIPlus;
	static ULONG_PTR GdiStartup;
	static Image *image;
public:

	static void Start();
	static void Stop();
	static void Think(void *, int);
	static void Draw(Graphics *graphics);
	static void MapLoad();
	static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
	static DWORD GetModulePath(HINSTANCE hInst, LPTSTR pszBuffer, DWORD dwSize );
	static DWORD __stdcall YaRRPHP::Upload(void *);
};