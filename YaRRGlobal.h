

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


struct Global
{
	char Name[256];
	void *Data;
	bool Protected;
};


class YaRRGlobal
{
public:
	static void Startup();
	static void Shutdown();
	static void Mapload();
	static void Mapend();
	static bool NoOutput;
	static time_t StartTime;
	static ircdata *IRC;
	static Stacker<int> *GDITeamChange;
	static Stacker<int> *NodTeamChange;
	static Stacker<Global> *Globals;
};