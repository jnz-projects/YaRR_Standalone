/*	Renegade Scripts.dll
	Cube texture class and loader
	Copyright 2007 Mark Sararu

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class CubeTextureLoadTask; 
class CubeTextureClass: public RefCountClass
{
public:
	CubeTextureClass(const char* name);
	bool Initialized;
	char *Name;
	bool IsMissing;
	IDirect3DCubeTexture9* D3DTexture;
	CubeTextureLoadTask* TextureLoadTask;
	void Delete_This();
	void Initialize();
};

class CubeTextureLoadTask: public ResourceLoadTask
{
protected:
	void LoadResource();
public:	
	CubeTextureLoadTask(CubeTextureClass* texture);
};
