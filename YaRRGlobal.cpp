#include "YaRRIncludes.h"


/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


bool YaRRGlobal::NoOutput = 1;
time_t YaRRGlobal::StartTime = 0;
ircdata *YaRRGlobal::IRC = 0;
Stacker<int> *YaRRGlobal::GDITeamChange = 0;
Stacker<int> *YaRRGlobal::NodTeamChange = 0;
Stacker<Global> *YaRRGlobal::Globals = 0;

void YaRRGlobal::Startup()
{
	DLOG;
	StartTime = time(0);
	NodTeamChange = Alloc(Stacker<int>);
	GDITeamChange = Alloc(Stacker<int>);
	Globals = Alloc(Stacker<Global>);
}

void YaRRGlobal::Shutdown()
{
	DLOG;
	if(NodTeamChange)
	{
		Dealloc(Stacker<int>, NodTeamChange);
	}
	if(GDITeamChange)
	{
		Dealloc(Stacker<int>, GDITeamChange);
	}
	if(Globals)
	{
		Dealloc(Stacker<Global>, Globals);
	}
}

void YaRRGlobal::Mapload()
{
	DLOG;
	if(NodTeamChange)
	{
		NodTeamChange->Clear();
	}
	if(GDITeamChange)
	{
		GDITeamChange->Clear();
	}
}

void YaRRGlobal::Mapend()
{
	DLOG;
	if(Get_Player_Count() == 0)
	{
		return;
	}
	/*
	int pmk = 0, pmd = 0;
	cPlayer *pvd = 0;
	for(GenericSLNode *x = PlayerList->HeadNode; x != 0; x = x->NodeNext)
	{
		cPlayer *p = (cPlayer *)x->NodeData;
		if(!p)
		{
			continue;
		}
		if(pmk == 0)
		{
			pmk = p->PlayerId;
		}

		if(pmd == 0)
		{
			pmd = p->PlayerId;
		}

		if(pvd == 0)
		{
			pvd = p;
		}

		pmk = p->Kills.Get() > Get_Kills(pmk) ? p->PlayerId : pmk;
		pmd = p->Deaths.Get() > Get_Deaths(pmd) ? p->PlayerId : pmd;
		pvd = p->VehiclesDestroyed > pvd->VehiclesDestroyed ? p : pvd;
	}

	Global mk, md, mvp, vd;
	mk.Protected = 1;
	md.Protected = 1;
	mvp.Protected = 1;
	vd.Protected = 1;
	strcpy(mk.Name, "MostKills");
	strcpy(md.Name, "MostDeaths");
	strcpy(mvp.Name, "MVP");
	strcpy(vd.Name, "VehiclesDestroyed");

	const char *pname = Get_Player_Name_By_ID(pmk);
	mk.Data = 0;
	md.Data = 0;
	if(pname)
	{
		mk.Data = CAlloc(strlen(pname)+5);
		strcpy((char *)mk.Data+4, pname);
		*(int *)mk.Data = Get_Kills(pmk);
		delete []pname;
	}

	pname = Get_Player_Name_By_ID(pmd);
	if(pname)
	{
		md.Data = CAlloc(strlen(pname)+5);
		strcpy((char *)md.Data+4, pname);
		*(int *)md.Data = Get_Deaths(pmd);
		delete []pname;
	}

	pname = WideCharToChar((const wchar_t *)pvd->PlayerName);
	vd.Data = CAlloc(strlen(pname)+5);
	strcpy((char *)vd.Data+4, pname);
	*(int *)vd.Data = pvd->VehiclesDestroyed;
	delete []pname;

	pname = WideCharToChar((const wchar_t *)The_Game()->MVPName);
	mvp.Data = CAlloc(strlen(pname)+5);
	strcpy((char *)mvp.Data+4, pname);
	*(int *)mvp.Data = The_Game()->MVPCount;
	delete []pname;

	IterateStack(x, Global, Globals)
	{
		if(strcmp(x.Name, "MostKills") == 0)
		{
			CDealloc(x.Data);
			x.Data = mk.Data;
			mk.Data = 0;
		}
		if(strcmp(x.Name, "MostDeaths") == 0)
		{
			CDealloc(x.Data);
			x.Data = md.Data;
			md.Data = 0;
		}
		if(strcmp(x.Name, "MVP") == 0)
		{
			CDealloc(x.Data);
			x.Data = mvp.Data;
			mvp.Data = 0;
		}
		if(strcmp(x.Name, "VehiclesDestroyed") == 0)
		{
			CDealloc(x.Data);
			x.Data = vd.Data;
			vd.Data = 0;
		}
	}

	if(mk.Data)
	{
		Globals->Push(mk);
	}
	if(md.Data)
	{
		Globals->Push(md);
	}
	if(vd.Data)
	{
		Globals->Push(vd);
	}
	if(mvp.Data)
	{
		Globals->Push(mvp);
	}*/
}