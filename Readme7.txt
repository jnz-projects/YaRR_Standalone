; ************************* Scripts by Vloktboky *************************

 - Documented by Jonathan Wilson, revised by Napalmic.

====================================================================================================

; ************************* [Script Name] KAK_Give_Powerup_On_Pickup

====================================================================================================

 [Description]

 - When the collectable item this is attached to is collected, it grants a powerup.

 [Parameters]

 - Powerup_Preset (Powerup to give)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Select_Weapon_Zone

====================================================================================================

 [Description]

 - When the zone this is attached to is entered or exited, it selects a weapon on the character that entered/exited the zone

 [Parameters]

 - Weapon_Preset (Weapon to select)

 - OnEnter (0 = don't select, 1 = select)

 - OnExit (0 = don't select, 1 = select)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Select_Weapon_On_Pickup

====================================================================================================

 [Description]

 - When the collectable item this is attached to is picked up, it selects a weapon on the character that picked it up.

 [Parameters]

 - Weapon_Preset (Weapon to select)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Harvester_Logic

====================================================================================================

 [Description]

 - Script for an AI harvester controller.

 [Parameters]

 - Harvester_Preset (Harvester preset)

 - Explosion_Preset (Explosion used to clear the area before spawning the harvester)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Harvester_Attached

====================================================================================================

 [Description]

 - Script to attach to the harvester.

 [Parameters]

 - LogicID (ID of the object with KAK_Harvester_Logic attached)

 - Number_Of_Paths (How many waypaths there are)

 - Waypath1_Dock2Field (Waypath 1 from dock to field)

 - Waypath1_Field2Dock (Waypath 1 from field to dock)

 - Waypath1_Tib_Zone (Center point for the first tiberium zone)

 - Waypath2_Dock2Field (Waypath 2 from dock to field)

 - Waypath2_Field2Dock (Waypath 2 from field to dock)

 - Waypath2_Tib_Zone (Center point for the second tiberium zone)

 - Waypath3_Dock2Field (Waypath 3 from dock to field)

 - Waypath3_Field2Dock (Waypath 3 from field to dock)

 - Waypath3_Tib_Zone (Center point for the third tiberium zone)

 - Harvest_Animation (Animation for when it's harvesting, repeats throughout the whole harvesting time. 0 = disable effect)

 - Dock_Animation (Animation for when it docks, plays once. 0 = disable effect)

 - Tiberium_Idle_Time (How long it should harvest for)

 - Dock_Location (Location to dock at)

 - Dock_Idle_Time (How long to dock for)

 - Dock_Credits (How many credits to give when docked)

 - Harvester_Create_Idle_Time (How long to wait when a harvester is blown up before recreating it. Doesn't affect the harvester created at startup)

 - Crystal_Preset (Preset for the tiberium crystal effect. 0 = disable effect)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Harvester_Kill

====================================================================================================

 [Description]

 - Script attached to the refinery to stop harvesting when it's destroyed. Can also stick on the power plant or anything else that should stop the harvesting when it dies.

 [Parameters]

 - LogicID (ID of the object with KAK_Harvester_Logic attached)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Harvester_Spawn_Kill

====================================================================================================

 [Description]

 - Stick this on anything. When whatever it's on gets killed, no more harvesters will be spawned, but harvesting will continue.

 [Parameters]

 - LogicID (ID of the object with KAK_Harvester_Logic attached)

 [Notes]

 - These scripts are intended for an AI controlled harvester that has some advantages to the built in logic. The only difference is that the harvester spawns from the refinery dock instead of being created at the weapons factory (but to do the latter would require a bunch of difficult to code logic). Another thing to note is that the crystals only appear on the field that the harvester is heading to, and they are only visible from the time the harvester starts heading to the field.

How to set it up:

1. Create a clone of daves arrow.
2. Change the model from o_davesarrow.w3d to your harvester model. Leave all other settings unchanged (it will still remain invisible in game).
3. Attach KAK_Harvester_Logic to the script, using the correct harvester preset and explosion. Make sure the explosion does enough damage to kill anything within the harvester spawn area.
4. Place this object in the dock bay of the refinery. Make sure it's facing the way the harvester has to face when it unloads.
5. Attach KAK_Harvester_Kill to whatever objects are appropriate to attach it to, setting the parameters appropriately.
6. Create the tiberium field(s) in the usual way (with the texture and the material settings, this will make it deadly to infantry).
7. Create both waypoints for each field (or just have one).
8. Set up the harvester (with animations if you want them).
9. Set up the tiberium crystal preset if you want it.
10. Put KAK_Harvester_Attached on the harvester preset, with all parameters set correctly.

 - Note that these scripts don't have the regular credit trickle effect of the refinery. If you want that, there are other ways to get it (for example using GTH_Credit_Trickle).

====================================================================================================

; ************************* [Script Name] KAK_Prevent_Kill

====================================================================================================

 [Description]

 - Stick this on an object to prevent it from being killed.

 [Parameters]

 - NONE

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Convert_Visceroid

====================================================================================================

 [Description]

 - Stick this on a soldier to make that soldier a visceroid.

 [Parameters]

 - NONE

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Freeze_Object

====================================================================================================

 [Description]

 - Makes an object frozen for a certain amount of time.

 [Parameters]

 - Time (Time to keep frozen)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] KAK_Regen_Custom

====================================================================================================

 [Description]

 - Regenerates health.

 [Parameters]

 - Interval (How often to regenerate)

 - Health (How much health/shield strength to regenerate)

 [Notes]

 - Send this a custom of 3251 to stop the regeneration.