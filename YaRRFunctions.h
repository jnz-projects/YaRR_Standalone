

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRFunctions
{
public:

	struct BanInfo
	{
		char Nick[32];
		char Banner[32];
		char Reason[256];
		int Time;
	};

	static const int Backuplen = 0x00457312-0x0045730C;
	static unsigned char Backup[Backuplen];
	static unsigned char NoGameoverBackup[5];
	static unsigned char Pausetmp[6];
	static char BuildingDISounds[11][2][31];


	static void Startup();
	static void Shutdown();
	static void YaRRError(const char *Error, ...);
	static void ListFiles(Stacker<char *> *stack, const char *Path);
	static void DeleteList(void *stack);
#ifdef _YaRRDebug
	static char *strdup3(const char *string, int Line, const char *File);
#else
	static char *strdup2(const char *string);
#endif
	static const char *Implode(Stacker<char *> *Tokens, int Offset);
	static void Explode(const char *String, char Delimiter, Stacker<char *> *Array);
	static void ConsoleInputF(const char *Format, ...);
	static bool IsNickBanned(const char *Nick);
	static bool IsIPBanned(const char *IP);
	static bool GetBanInfo(BanInfo *ifo, const char *Nick, const char *IP);
	static void BootPlayer(int ID, const char *reason, ...);
	static int FindPlayer(const char *part);
	static const char *GetWeaponString(GameObject *obj, char *Buffer, int Size);
	static char *RemovePunct(char *str);
	static char *RemoveWhite(char *str);
	static char *ToLower(char *str);
	static int GetStatus(char access);
	static void GetStatusName(char Access, char *buffer);
	static void FormatTime(time_t t, char *buffer, int Length);
	static void FormatTime(time_t t, const char *Format, char *buffer, int Length);
	static const char *GetPlayerName(int ID);
	static void PPageErr(int ID, const char *Format, ...);
	static void PPage(int ID, const char *Format, ...);
	static const char *GetPresetScreenName(GameObject *obj);
	static const char *GetPresetScreenName(const char *preset);
	static void ApplyNoPendingPatch();
	static void ApplyStartButtonFix();
	static bool Vowel(char _c);
	static bool IsNumber(const char *_str);
	static bool sortalpha(char a, char b);
	static void Sort(Stacker<char> &stack, bool (*comparitor)(char a, char b));
	static void Sort(Stacker<char *> &stack, bool (*comparitor)(char a, char b), int offset);
	static bool HexToRGB(Colour::CRGB *rgb, const char *Hex);
	static char *Replace(char *Str, const char *At, const char *With);
	static void Pause();
	static void Resume();
	static int GetPCount(int team);
	static void RGBToHex(Colour::CRGB &rgb, char *buffer);
	static void CheckNodeBan(Player *p);
	static unsigned int GetWorth(const char *Preset);
	static const char *FindDISound(GameObject *obj);
	static int GetTeamPlayerCount(int team);
	static int GetTeamColour(int team);
	static void PlaySound(void *Sound, int Type, int Where, int Target);
	static void Send_Message(int ID, WideStringClass &Message, int _Type, bool Host, int Target);
	static void SendMessageF(int ID, int Type, int Host, int Target, const char *Format, ...);
	static void Send_SCAnnouncement(int ID, int a , int b, int Message, int c);
};

#define CopyStack(dest, source, type) type x; for(source->Reset(); source->Iterate(&x);) dest->Push(x)

#ifdef _YaRRDebug
#define strdup2(str) strdup3(str, __LINE__, __FILE__)
#endif