/*	Renegade Scripts.dll
	Hook scripts
	Copyright 2007 TheKGBspy, Jonathan Wilson, WhiteDragon(MDB)

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "engine.h"
#include "jfwhook.h"

void ObjectCreateHookBaseHook(void *data,GameObject *obj)
{
	JFW_Object_Created_Hook_Base *base = (JFW_Object_Created_Hook_Base *)data;
	base->ObjectCreateHook(obj);
}

JFW_Object_Created_Hook_Base::~JFW_Object_Created_Hook_Base()
{
	if (is_objecthook_set == 1337)
	{
		RemoveHook();
	}
}

void JFW_Object_Created_Hook_Base::Detach(GameObject *obj)
{
	ScriptImpClass::Detach(obj);
	if (is_objecthook_set == 1337)
	{
		RemoveHook();
	}
}

void JFW_Object_Created_Hook_Base::Created(GameObject *obj)
{
	hookid = 0;
	if (is_objecthook_set != 1337)
	{
		InstallHook();
		is_objecthook_set = 1337;
	}
}

void JFW_Object_Created_Hook_Base::InstallHook()
{
	if (AddObjectCreateHook)
	{
		h = new ObjectCreateHookStruct;
		h->data = (void *)this;
		h->hook = ObjectCreateHookBaseHook;
		if (is_objecthook_set == 1337)
		{
			RemoveHook();
		}
		hookid = AddObjectCreateHook(h);
	}
}

void JFW_Object_Created_Hook_Base::RemoveHook()
{
	if (RemoveObjectCreateHook && hookid)
	{
		RemoveObjectCreateHook(hookid);
		hookid = 0;
		is_objecthook_set = 0;
		if (h)
		{
			delete h;
			h = 0;
		}
	}
}

void JFW_Attach_Script_Preset_Created::ObjectCreateHook(GameObject *obj)
{
	const char *script;
	const char *paramx;
	char *params;
	char delim;
	script = Get_Parameter("Script");
	paramx = Get_Parameter("Params");
	params = newstr(paramx);
	delim = Get_Parameter("Delim")[0];
	unsigned int x = strlen(params);
	for (unsigned int i=0;i<x;i++)
	{
		if (params[i] == delim)
		{
			params[i] = ',';
		}
	}
	Attach_Script_Is_Preset(obj,Get_Parameter("Preset"),script,params,Get_Int_Parameter("Player_Type"));
	delete[] params;
}

void JFW_Attach_Script_Type_Created::ObjectCreateHook(GameObject *obj)
{
	const char *script;
	const char *paramx;
	char *params;
	char delim;
	script = Get_Parameter("Script");
	paramx = Get_Parameter("Params");
	params = newstr(paramx);
	delim = Get_Parameter("Delim")[0];
	unsigned int x = strlen(params);
	for (unsigned int i=0;i<x;i++)
	{
		if (params[i] == delim)
		{
			params[i] = ',';
		}
	}
	Attach_Script_Is_Type(obj,Get_Int_Parameter("Type"),script,params,Get_Int_Parameter("Player_Type"));
	delete[] params;
}

void JFW_Attach_Script_Player_Created::Created(GameObject *obj)
{
	hookid = 0;
	InstallHook();
	const char *script;
	const char *paramx;
	char *params;
	char delim;
	script = Get_Parameter("Script");
	paramx = Get_Parameter("Params");
	params = newstr(paramx);
	delim = Get_Parameter("Delim")[0];
	unsigned int x = strlen(params);
	for (unsigned int i=0;i<x;i++)
	{
		if (params[i] == delim)
		{
			params[i] = ',';
		}
	}
	Attach_Script_Player_Once(script,params,Get_Int_Parameter("Player_Type"));
	delete[] params;
}

void JFW_Attach_Script_Player_Created::ObjectCreateHook(GameObject *obj)
{
	const char *script;
	const char *paramx;
	char *params;
	char delim;
	script = Get_Parameter("Script");
	paramx = Get_Parameter("Params");
	params = newstr(paramx);
	delim = Get_Parameter("Delim")[0];
	unsigned int x = strlen(params);
	for (unsigned int i=0;i<x;i++)
	{
		if (params[i] == delim)
		{
			params[i] = ',';
		}
	}
	if (Commands->Is_A_Star(obj))
	{
		Attach_Script_Is_Type(obj,Soldier,script,params,Get_Int_Parameter("Player_Type"));
	}
	delete[] params;
}

void JFW_Attach_Script_Vehicle_Created::ObjectCreateHook(GameObject *obj)
{
	const char *script;
	const char *paramx;
	char *params;
	char delim;
	script = Get_Parameter("Script");
	paramx = Get_Parameter("Params");
	params = newstr(paramx);
	delim = Get_Parameter("Delim")[0];
	unsigned int x = strlen(params);
	for (unsigned int i=0;i<x;i++)
	{
		if (params[i] == delim)
		{
			params[i] = ',';
		}
	}
	Attach_Script_Is_Type(obj,Vehicle,script,params,Get_Int_Parameter("Player_Type"));
	delete[] params;
}

void KeyHookBaseHook(void *data)
{
	JFW_Key_Hook_Base *base = (JFW_Key_Hook_Base *)data;
	base->KeyHook();
}

void JFW_Key_Hook_Base::InstallHook(const char *keyname,GameObject *obj)
{
	k = new KeyHookStruct;
	k->data = this;
	k->hook = KeyHookBaseHook;
	k->PlayerID = Get_Player_ID(obj);
	k->key = newstr(keyname);
	hookid = AddKeyHook(k);
}

void JFW_Key_Hook_Base::RemoveHook()
{
	if (RemoveKeyHook && hookid)
	{
		RemoveKeyHook(hookid);
		hookid = 0;
		is_keyhook_set = 0;
		if (k)
		{
			if (k->key)
			{
				delete[] k->key;
			}
			delete k;
			k = 0;
		}
	}
}

void JFW_Key_Hook_Base::Destroyed(GameObject *obj)
{
	if (is_keyhook_set == 1337)
	{
		RemoveHook();
	}
}

void JFW_Key_Hook_Base::Detach(GameObject *obj)
{
	ScriptImpClass::Detach(obj);
	if (is_keyhook_set == 1337)
	{
		RemoveHook();
	}
}

JFW_Key_Hook_Base::~JFW_Key_Hook_Base()
{
	if (is_keyhook_set == 1337)
	{
		RemoveHook();
	}
}

void JFW_Jetpack::Detach(GameObject *obj)
{
	if (Get_Fly_Mode(obj))
	{
		Toggle_Fly_Mode(obj);
	}
	JFW_Key_Hook_Base::Detach(obj);
}

void JFW_Jetpack::Created(GameObject *obj)
{
	enabled = true;
	hookid = 0;
	k = 0;
	if (is_keyhook_set != 1337)
	{
		InstallHook("Jetpack",obj);
		is_keyhook_set = 1337;
	}
}

void JFW_Jetpack::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("EnableMessage"))
	{
		enabled = true;
	}
	if (message == Get_Int_Parameter("DisableMessage"))
	{
		enabled = false;
	}
}

void JFW_Jetpack::KeyHook()
{
	if (enabled)
	{
		Toggle_Fly_Mode(Owner());
	}
}

void JFW_Jetpack::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&enabled);
}

void JFW_Jetpack_Model::Detach(GameObject *obj)
{
	if (Get_Fly_Mode(obj))
	{
		Toggle_Fly_Mode(obj);
	}
	JFW_Key_Hook_Base::Detach(obj);
}

void JFW_Jetpack_Model::Created(GameObject *obj)
{
	enabled = true;
	hookid = 0;
	k = 0;
	if (is_keyhook_set != 1337)
	{
		InstallHook("Jetpack",obj);
		is_keyhook_set = 1337;
	}
}

void JFW_Jetpack_Model::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == Get_Int_Parameter("EnableMessage"))
	{
		enabled = true;
	}
	if (message == Get_Int_Parameter("DisableMessage"))
	{
		enabled = false;
	}
}

void JFW_Jetpack_Model::KeyHook()
{
	if (enabled)
	{
		Toggle_Fly_Mode(Owner());
	}
	if (Get_Fly_Mode(Owner()))
	{
		Commands->Set_Model(Owner(),Get_Parameter("OnModel"));
	}
	else
	{
		Commands->Set_Model(Owner(),Get_Parameter("OffModel"));
	}
}

void JFW_Jetpack_Model::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&enabled);
}

void JFW_Dplbl_Vhcls_Keyboard::Created(GameObject *obj)
{
	pilotID = 0;
	CanDestroyAnim = true;
	hookid = 0;
	k = 0;
}

void JFW_Dplbl_Vhcls_Keyboard::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == CUSTOM_EVENT_VEHICLE_ENTER)
	{
		if (!pilotID)
		{
			if (is_keyhook_set != 1337)
			{
				InstallHook("Deploy",sender);
				is_keyhook_set = 1337;
			}
			pilotID = Commands->Get_ID(sender);
		}
	}
	if (message == CUSTOM_EVENT_VEHICLE_EXIT)
	{
		if (pilotID == Commands->Get_ID(sender))
		{
			if (is_keyhook_set == 1337)
			{
				RemoveHook();
			}
			pilotID = 0;
		}
	}
}

void JFW_Dplbl_Vhcls_Keyboard::KeyHook()
{
	Force_Occupants_Exit(Owner());
	GameObject *Animation;
	Vector3 position;
	int TempCalcul;
	position.X = 0;
	position.Y = 0;
	position.Z = 0;
	Animation = Commands->Create_Object(Get_Parameter("Animation_Preset"),position);
	TempCalcul = (int)(Commands->Get_Max_Health(Animation)*(Commands->Get_Health(Owner())/Commands->Get_Max_Health(Owner())));
	Commands->Set_Health(Animation,(float)TempCalcul);
	TempCalcul = (int)(Commands->Get_Max_Shield_Strength(Animation)*(Commands->Get_Shield_Strength(Owner())/Commands->Get_Max_Shield_Strength(Owner())));
	Commands->Set_Shield_Strength(Animation,(float)TempCalcul);
	Commands->Attach_To_Object_Bone(Animation,Owner(),"origin");
	CanDestroyAnim = false;
	Commands->Apply_Damage(Owner(),Get_Float_Parameter("oldTnk_Dammage"),Get_Parameter("oldTnk_Warhead"),0);
}

void JFW_Dplbl_Vhcls_Keyboard::Killed(GameObject *obj,GameObject *shooter)
{
	Vector3 position;
	if (CanDestroyAnim)
	{
		position = Commands->Get_Bone_Position(obj,"origin");
		Commands->Create_Explosion(Get_Parameter("Explosion_preset"),position,0);
	}
}

void JFW_Dplbl_Vhcls_Keyboard::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,4,&pilotID);
	Auto_Save_Variable(2,1,&CanDestroyAnim);
}

void JFW_Underground_Logic::Created(GameObject *obj)
{
	underground = false;
	enable = true;
	hookid = 0;
	k = 0;
	pilotID = 0;
}

void JFW_Underground_Logic::Custom(GameObject *obj,int message,int param,GameObject *sender)
{
	if (message == CUSTOM_EVENT_VEHICLE_ENTER)
	{
		if (!pilotID)
		{
			if (is_keyhook_set != 1337)
			{
				InstallHook("Dig",sender);
				is_keyhook_set = 1337;
			}
			pilotID = Commands->Get_ID(sender);
		}
	}
	if (message == CUSTOM_EVENT_VEHICLE_EXIT)
	{
		if (pilotID == Commands->Get_ID(sender))
		{
			if (is_keyhook_set == 1337)
			{
				RemoveHook();
			}
			pilotID = 0;
			if (underground)
			{
				Commands->Apply_Damage(obj,100,"Death",0);
			}
		}
		if (underground)
		{
			Set_Screen_Fade_Color_Player(sender,0,0,0,0);
			Set_Screen_Fade_Opacity_Player(sender,0,0);
			Commands->Apply_Damage(sender,100,"Death",0);
		}
	}
	if (message == Get_Int_Parameter("EnableMessage"))
	{
		enable = true;
	}
	if (message == Get_Int_Parameter("DisableMessage"))
	{
		enable = false;
	}
	if (message == Get_Int_Parameter("IndicatorMessage"))
	{
		if (underground)
		{
			Vector3 v;
			v = Commands->Get_Position(obj);
			v.Z += Get_Float_Parameter("IndicatorZOffset");
			Commands->Create_Object(Get_Parameter("IndicatorObject"),v);
		}
	}
}

void JFW_Underground_Logic::KeyHook()
{
	if (enable)
	{
		if (underground)
		{
			underground = false;
			Set_Occupants_Fade(Owner(),0,0,0,0);
			Vector3 v = Commands->Get_Position(Owner());
			v.Z -= Get_Float_Parameter("UpZOffset");
			Commands->Create_Sound(Get_Parameter("DigSound"),v,Owner());
			Commands->Create_Object(Get_Parameter("SurfaceEffectObj"),v);
			Commands->Set_Position(Owner(),v);
		}
		else
		{
			underground = true;
			Set_Occupants_Fade(Owner(),Get_Float_Parameter("DigRed"),Get_Float_Parameter("DigBlue"),Get_Float_Parameter("DigGreen"),Get_Float_Parameter("DigOpacity"));
			Vector3 v = Commands->Get_Position(Owner());
			Commands->Create_Sound(Get_Parameter("DigSound"),v,Owner());
			Commands->Create_Object(Get_Parameter("DigEffectObj"),v);
			v.Z += Get_Float_Parameter("DownZOffset");
			Commands->Set_Position(Owner(),v);
		}
	}
}

void JFW_Underground_Logic::Killed(GameObject *obj,GameObject *shooter)
{
	if (underground)
	{
		Set_Occupants_Fade(obj,0,0,0,0);
		Kill_Occupants(obj);
	}
}

void JFW_Underground_Logic::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&underground);
	Auto_Save_Variable(2,1,&enable);
	Auto_Save_Variable(3,4,&pilotID);
}

void JFW_Suicide_Bomber::Created(GameObject *obj)
{
	enabled = true;
	hookid = 0;
	k = 0;
	if (is_keyhook_set != 1337)
	{
		InstallHook("BlowUp",obj);
		is_keyhook_set = 1337;
	}
}

void JFW_Suicide_Bomber::KeyHook()
{
	if (enabled)
	{
		Vector3 v = Commands->Get_Position(Owner());
		Commands->Create_Explosion(Get_Parameter("Explosion"),v,Owner());
		Commands->Apply_Damage(Owner(),99999,"Death",0);
	}
}

void JFW_Suicide_Bomber::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&enabled);
}

void JFW_Sidebar_Key_2::Created(GameObject *obj)
{
	if (is_keyhook_set != 1337)
	{
		InstallHook(Get_Parameter("Key"),obj);
		is_keyhook_set = 1337;
		Enabled = false;
	}
}

void JFW_Sidebar_Key_2::KeyHook()
{
	GameObject *obj = Owner();
	if (Enabled && !Get_Vehicle(obj))
	{
		if (!Get_Object_Type(obj))
		{
			Display_NOD_Sidebar(obj);
		}
		else
		{
			Display_GDI_Sidebar(obj);
		}
	}
}

void JFW_Sidebar_Key_2::Register_Auto_Save_Variables()
{
	Auto_Save_Variable(1,1,&Enabled);
}

void JFW_Sidebar_Key_2::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	if (message == Get_Int_Parameter("Disable_Custom"))
	{
		Enabled = false;
	}
	else if (message == Get_Int_Parameter("Enable_Custom"))
	{
		Create_2D_Sound_Player(obj,Get_Parameter("Sound"));
		Enabled = true;
	}
}

ScriptRegistrant<JFW_Attach_Script_Preset_Created> JFW_Attach_Script_Preset_Created_Registrant("JFW_Attach_Script_Preset_Created","Script:string,Params:string,Delim:string,Preset:string,Player_Type:int");
ScriptRegistrant<JFW_Attach_Script_Type_Created> JFW_Attach_Script_Type_Created_Registrant("JFW_Attach_Script_Type_Created","Script:string,Params:string,Delim:string,Type:int,Player_Type:int");
ScriptRegistrant<JFW_Attach_Script_Player_Created> JFW_Attach_Script_Player_Created_Registrant("JFW_Attach_Script_Player_Created","Script:string,Params:string,Delim:string,Player_Type:int");
ScriptRegistrant<JFW_Attach_Script_Vehicle_Created> JFW_Attach_Script_Vehicle_Created_Registrant("JFW_Attach_Script_Vehicle_Created","Script:string,Params:string,Delim:string,Player_Type:int");
ScriptRegistrant<JFW_Dplbl_Vhcls_Keyboard> JFW_Dplbl_Vhcls_Keyboard_Registrant("JFW_Dplbl_Vhcls_Keyboard","Animation_Preset:string,oldTnk_Warhead:string,oldTnk_Dammage:float,Explosion_preset:string");
ScriptRegistrant<JFW_Underground_Logic> JFW_Underground_Logic_Registrant("JFW_Underground_Logic","UpZOffset:float,DownZOffset:float,DigEffectObj:string,SurfaceEffectObj:string,DisableMessage:int,EnableMessage:int,IndicatorMessage:int,IndicatorObject:string,IndicatorZOffset:float,DigRed:float,DigGreen:float,DigBlue:float,DigOpacity:float,DigSound:string");
ScriptRegistrant<JFW_Jetpack> JFW_Jetpack_Registrant("JFW_Jetpack","DisableMessage:int,EnableMessage:int");
ScriptRegistrant<JFW_Jetpack_Model> JFW_Jetpack_Model_Registrant("JFW_Jetpack_Model","DisableMessage:int,EnableMessage:int,OnModel:string,OffModel:string");
ScriptRegistrant<JFW_Suicide_Bomber> JFW_Suicide_Bomber_Registrant("JFW_Suicide_Bomber","Explosion:string");
ScriptRegistrant<JFW_Sidebar_Key_2> JFW_Sidebar_Key_2_Registrant("JFW_Sidebar_Key_2","Key=Sidebar:string,Enable_Custom=0:int,Disable_Custom=0:int,Sound:string");
