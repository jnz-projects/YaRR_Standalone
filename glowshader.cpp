/*	Renegade Scripts.dll
	GlowShaderClass
	Copyright 2007 Jonathan Wilson
	glow.fx copyright Microsoft Corporation

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader.h"
#include "glowshader.h"

GlowShaderClass::GlowShaderClass() : ProgrammableShaderClass()
{
	LightDir = Vector3();
	GlowColor = Vector4();
	GlowAmbient = Vector4();
	GlowThickness = 0;
	PixelShaderVersion = 0;
	VertexShaderVersion = 1.1f;
}

void GlowShaderClass::Load(ChunkLoadClass& cload)
{
	while (cload.Open_Chunk())
	{
		switch (cload.Cur_Chunk_ID())
		{
		case CHUNK_GLOW_SHADER_VARIABLES:
			while (cload.Open_Micro_Chunk())
			{
				switch(cload.Cur_Micro_Chunk_ID())
				{
				case MICROCHUNK_GLOW_SHADER_VARIABLES_LIGHTDIRECTION:
					cload.Read(&LightDir,sizeof(LightDir));
					break;
				case MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWCOLOR:
					cload.Read(&GlowColor,sizeof(GlowColor));
					break;
				case MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWAMBIENT:
					cload.Read(&GlowAmbient,sizeof(GlowAmbient));
					break;
				case MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWTHICKNESS:
					cload.Read(&GlowThickness,sizeof(GlowThickness));
					break;
				}
				cload.Close_Micro_Chunk();
			}
			break;
		case CHUNK_SHADER:
			ProgrammableShaderClass::Load(cload);
			break;
		}
		cload.Close_Chunk();
	}
#ifndef SDBEDIT
	Effect = new EffectClass(this->Filename);
#endif
}

void GlowShaderClass::Save(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_GLOW_SHADER);
	csave.Begin_Chunk(CHUNK_GLOW_SHADER_VARIABLES);
	csave.Begin_Micro_Chunk(MICROCHUNK_GLOW_SHADER_VARIABLES_LIGHTDIRECTION);
	csave.Write(&LightDir,sizeof(LightDir));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWCOLOR);
	csave.Write(&GlowColor,sizeof(GlowColor));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWAMBIENT);
	csave.Write(&GlowAmbient,sizeof(GlowAmbient));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MICROCHUNK_GLOW_SHADER_VARIABLES_GLOWTHICKNESS);
	csave.Write(&GlowThickness,sizeof(GlowThickness));
	csave.End_Micro_Chunk();
	csave.End_Chunk();
	ProgrammableShaderClass::Save(csave);
	csave.End_Chunk();
}

#ifndef SDBEDIT
void GlowShaderClass::ApplyTexture(unsigned int stage) 
{
	if (!render_state->Textures[stage])
	{
		ApplyTextureNull(stage);
	}
	TextureClass *t = render_state->Textures[stage];
	if (!t->Initialized)
	{
		t->Init();
	}
	t->LastAccessed = *SyncTime;
	if (*TexturingEnabled)
	{
		Effect->SetTexture("Tex0",t->D3DTexture->texture9);
		Effect->SetTechnique("TGlowAndTexture");
	}
	else
	{
		ApplyTextureNull(stage);
	}
}

void GlowShaderClass::ApplyTextureNull(unsigned int stage)
{
	Effect->SetTexture("Tex0",0);
	Effect->SetTechnique("TGlowOnly");
}

void GlowShaderClass::ApplyRenderState()
{
	Effect->SetVector("LightDir",Vector4::FromVector3(&LightDir));
	Effect->SetVector("GlowColor",&GlowColor);
	Effect->SetVector("GlowAmbient",&GlowAmbient);
	Effect->SetFloat("GlowThickness",GlowThickness);
	Effect->SetMatrix("Projection",Get_Projection_Matrix());
	Effect->SetMatrix("World",&render_state->world);
	Effect->SetMatrix("View",&render_state->view);
	ApplyTexture(0);
	ShaderClass *s = &(render_state->shader);
	s->Apply();
}

void GlowShaderClass::Render(unsigned int primitive_type, unsigned short start_index, unsigned short polygon_count, unsigned short min_vertex_index, unsigned short vertex_count)
{
	unsigned int cPasses;
	ApplyRenderState();
	Effect->Begin(&cPasses,0);
	for (unsigned int iPass = 0; iPass < cPasses; iPass++)
	{
		Effect->BeginPass(iPass);
		Effect->CommitChanges();
		Buffers_Apply();
		Draw(primitive_type,start_index,polygon_count,min_vertex_index,vertex_count);
		Effect->EndPass();
	}
	Effect->End();
}
void GlowShaderClass::Release_Resources()
{
	if (!Loaded)
	{
		return;
	}
	Effect->OnDeviceLost(); 
	Loaded = false;
}

void GlowShaderClass::Reload_Resources()
{
	if ((PixelShaderVersion > ShaderCaps::PixelShaderVersion) || (VertexShaderVersion > ShaderCaps::VertexShaderVersion))
	{
		return;
	}
	if (Loaded)
	{
		return;
	}
	if (Effect->Initialized != true)
	{
		Effect->Init();
	}
	else 
	{
		Effect->OnDeviceReset();
	}
	Loaded = true;
}

ShaderRegistrant<GlowShaderClass> GlowShaderRegistrant(CHUNK_GLOW_SHADER);
#endif
