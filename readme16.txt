; ************************* Expanded Vehicle Factory set of scripts by Matt Bailey "Whitedragon" *************************

 - whitedragonbc@gmail.com

All scripts documented in this file are part of the Expanded Vehicle Factory set of scripts.
The purpose of these scripts is to create a proper implementation of helipad and naval buildings
where flying and naval vehicles are bought from the PT like normal vehicles but are created at their respective buildings.
You can download an example on how to setup these scripts on a level at http://black-cell.net/ExpVehFacExample.zip.

These scripts require the following settings to be enabled in hud.ini:

[General]
NewUnpurchaseableLogic=true
VehicleBuildingDisable=true


====================================================================================================

; ************************* [Script Name] MDB_ExpVehFac_Helipad

====================================================================================================

 [Description]

 - This should be attached to all helipads on your map. It handles removing flying units from the PT menu when all helipads
   die.
   This script works similar to an airstrip, it uses a cinematic to make newly bought flying vehicles fly in from off the map
   and land at the designated landing positions. When the vehicle lands, anything under it will be crushed and destroyed.
   All vehicles with a mode of flying that aren't listed as naval vehicles will be handled by this script.
   2 cinematic text files, ExpVehFac_1.txt and ExpVehFac_0.txt, are needed by this script. You can download them from
   http://black-cell.net/ExpVehFacCin.zip or make your own if you know how.
   When a flying unit is built, the custom CUSTOM_EVENT_VEHICLE_OWNER (value 1000000036) is sent to the vehicle to
   notify it of its new owner.

 [Parameters]

 - Landing_Preset: The name of the preset used to mark landing positions. Basically, you create a new preset(make it a copy
   of Dave's Arrow) and then input its name here. Then you place this preset on your level where ever you want a landing
   position to be. The script will cycle through all the positions as flying vehicles are bought. For example if you had
   3 positions, 1, 2, and 3, it would land the first vehicle bought at position 1, then the next at position 2, and so on,
   going back to the first position when it gets to the end. Each team needs their own preset, something like 
   "Landing_Pos_GDI" and "Landing_Pos_Nod".

 - Build_Time: How long, in seconds, the vehicle menu should be set to building when a flying vehicle is bought. This will
   effect the purchase of all vehicle types.

 - Flying_Build_Time: How long, in seconds, flying vehicles should be unpurchasable after a flying vehicle is bought. 
   This will only effect the purchase of flying vehicles.

 - Remove_on_Death: Decides what to do with the PT icons of vehicles when all their prerequisite buildings are killed.
   Set to 1 if you want the icons to be completely removed.
   Set to 0 and the icons will say "Unavailable" and be unpurchasable.

 - Support_Vehicles: A list of all support vehicles. Seperated by semi-colons, "Veh1;Veh2;etc."
   Support vehicles have their own limit seperate from the other 3 vehicle types, however they don't have their own building
   or creation positions.

 - Sidebar: Does your mod use the sidebar feature in bhs.dll? 1 for yes or 0 for no.

====================================================================================================

; ************************* [Script Name] MDB_ExpVehFac_Naval

====================================================================================================

 [Description]

 - This should be attached to all naval buildings on your map. It handles removing naval units from the PT menu when all 
   naval buildings die.
   This script works similar to a weapons factory, directly spawning the purchased vehicle at the designated positions.
   Supports vehicle construction zones just like weapons factories. You place a zone of type Vehicle Construction 
   around a creation position and all soldiers and vehicles in that zone will be killed when that position is used.
   All vehicles listed in the parameter below will be considered naval vehicles.
   When a naval unit is built, the custom CUSTOM_EVENT_VEHICLE_OWNER (value 1000000036) is sent to the vehicle to
   notify it of its new owner.

 [Parameters]

 - Naval_Vehicles: A list of all naval vehicles on this team. Seperated by semi-colons, "Veh1;Veh2;etc."

 - Creation_Preset: This parameter can be used in 2 different ways, I call them "Cycled Creation Positions" and 
   "Per Vehicle Creation Positions".

   Firstly, the "Cycled Creation Positions" way.
   Input the name of the preset used to mark creation positions. Basically, you create a new preset(make it a copy
   of Dave's Arrow) and then input its name here. Then you place this preset on your level where ever you want a creation
   position to be. The script will cycle through all the positions as naval vehicles are bought. For example if you had
   3 positions, 1, 2, and 3, it would create the first vehicle bought at position 1, then the next at position 2, and so on,
   going back to the first position when it gets to the end. Each team needs their own preset, something like 
   "Creation_Pos_GDI" and "Creation_Pos_Nod".

   Next, the "Per Vehicle Creation Position" way.
   This allows you to have a different creation position for each vehicle. It must be in the format 
   "Naval_Vehicle_Preset1=Creation_Preset1;Naval_Vehicle_Preset2=Creation_Preset2;etc." This way of using the creation
   positions only allows for 1 position per vehicle. Each naval vehicle will, of course, need its own creation preset.

 - Build_Time: How long, in seconds, the vehicle menu should be set to building when a naval vehicle is bought. This will
   effect the purchase of all vehicle types.

 - Naval_Build_Time: How long, in seconds, naval vehicles should be unpurchasable after a naval vehicle is bought. 
   This will only effect the purchase of naval vehicles.

 - Remove_on_Death: Decides what to do with the PT icons of vehicles when all their prerequisite buildings are killed.
   Set to 1 if you want the icons to be completely removed.
   Set to 0 and the icons will say "Unavailable" and be unpurchasable.

 - Support_Vehicles: A list of all support vehicles. Seperated by semi-colons, "Veh1;Veh2;etc."
   Support vehicles have their own limit seperate from the other 3 vehicle types, however they don't have their own building
   or creation positions.

 - Sidebar: Does your mod use the sidebar feature in bhs.dll? 1 for yes or 0 for no.

====================================================================================================

; ************************* [Script Name] MDB_ExpVehFac_Vehicle_Factory

====================================================================================================

 [Description]

 - This should be attached to all vehicle factories(weapons factories and airstrips) on your map. It handles removing 
   ground units from the PT menu when all vehicle factories die.
   Setup your vehicle factory like you would normally.
   All vehicles that have a mode other than flying and aren't listed as naval vehicles will be considered ground vehicles.

 [Parameters]

 - Build_Time: How long, in seconds, the vehicle menu should be set to building when a ground vehicle is bought. This will
   effect the purchase of all vehicle types.

 - Ground_Build_Time: How long, in seconds, ground vehicles should be unpurchasable after a ground vehicle is bought. 
   This will only effect the purchase of ground vehicles.

 - Remove_on_Death: Decides what to do with the PT icons of vehicles when all their prerequisite buildings are killed.
   Set to 1 if you want the icons to be completely removed.
   Set to 0 and the icons will say "Unavailable" and be unpurchasable.

 - Support_Vehicles: A list of all support vehicles. Seperated by semi-colons, "Veh1;Veh2;etc."
   Support vehicles have their own limit seperate from the other 3 vehicle types, however they don't have their own 
   prerequisite buildings or creation positions.

 - Sidebar: Does your mod use the sidebar feature in bhs.dll? 1 for yes or 0 for no.

====================================================================================================

; ************************* [Script Name] MDB_ExpVehFac_Cinematic

====================================================================================================

 [Description]
 
 - Used internally. Creates and attaches the desired flying vehicle to the cinematic.
 
 [Parameters]

 - Team: Team that is buying the flying vehicle.

====================================================================================================

; ************************* [Script Name] MDB_ExpVehFac_Cinematic_Vehicle

====================================================================================================

 [Description]
 
 - Used internally. Makes the vehicle that is flying in invincible.
 
 [Parameters]

 None.

====================================================================================================

; ************************* [Script Name] MDB_ExpVehFac_Limit

====================================================================================================

 [Description]

 - Used internally. Handles decreasing the count of currently existing vehicles when the vehicle the script is attached to 
   is destroyed.

 [Parameters]

 - Type: Type of this vehicle, either ground, flying, or naval.

====================================================================================================

; ************************* [Feature] Per Vehicle Type and Per Team Vehicle Limits

====================================================================================================

 The Expanded Vehicle Factory set of scripts supports a different vehicle limit for each team and for 
 ground, flying, naval, and support vehicles.
 If you want each team to use the same limit create a file called "ExpVehFac.cfg" and put it in the mix file of your map, 
 or the pkg, or always.dat file of your mod. Then put the following lines in it:
 "Ground_Limit=#"
 "Flying_Limit=#"
 "Naval_Limit=#"
 "Support_Limit=#"
 If you want each team to have their own limit then create 2 files, "ExpVehFac_0.cfg" and "ExpVehFac_1.cfg" instead. Put the
 same stuff in them as you would the "ExpVehFac.cfg" file.
 You must either give all or none of the vehicle types used in your map/mod their own limit. You can't give one a limit 
 and leave the others without a limit.
 The total vehicle limit will be set to the sum of all 4 type limits, meaning the vehicle menu will say "Limit Reached" 
 when all 4 limits have been reached, when the limit for only a type is reached all PT icons of that type will say 
 "Limit Reached" and be unpurchasable.
