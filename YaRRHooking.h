

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/



bool ReadMemory(int Location, void* Buffer, int BufferLen);
bool WriteMemory(int Location, const void* Buffer, int BufferLen);
void StartButton_Glue();
void NoGameover_Glue();

void SendGameSpyChallenge(int ID, StringClass &_str);

class YaRRHooking
{
public:
	static void Install();
	static void Uninstall();
	static int RunGame;
	static bool Locked;
};
