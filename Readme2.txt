Scripts By Dante

// Beta for enabling Stealth Armor *note, it may screw up SBH's stealth,
// so I would recommend only using it on maps without them for now until I
// can get a fix for it.
// TDA_Stealth_Armor
// Stealth_Length=60:int		'how long you want them to be stealth


// Use this to send messages on ZoneEnter and Zone Exit
//
//TDA_Send_Custom_Zone
//ID:int				'id of object to send to 
//EnterMessage:int			'message to send on ZoneEnter
//EnterParam:int			'Parameter on ZoneEnter
//ExitMessage:int			'message to send on ZoneExit
//ExitParam:int				'Parameter on ZoneExit
//Team_ID:int			'Team ID 0=Nod, 1=GDI, 2=Any


// Construction Yard Controllers, will send the appropriate messages to the specified objects
// to make them QUIT repairing the buildings
//
//TDA_Conyard_Controller
//
//Building1_ID=0:int			'1st building to disable
//Building2_ID=0:int			'2nd building to disable
//Building3_ID=0:int			'3rd building to disable
//Building4_ID=0:int			'4th building to disable
//Building5_ID=0:int			'5th building to disable
//Building6_ID=0:int			'6th building to disable
//Building7_ID=0:int			'7th building to disable
//Building8_ID=0:int			'8th building to disable
//Building9_ID=0:int			'9th building to disable
//Building10_ID=0:int			'10th building to disable


// Conyard Repair Script, Great working repair script :)
// Use the controller script to turn off.
//
// TDA_Conyard_Repair
// Repair_Frequency:int			how many ticks until it recieves 1 health
// Timer_ID:int				ID of the object it is attached to, will crash game if left blank.


// Use this to get the flying objects to appear on the Helipad...
//
//TDA_User_Purchased_VTOL_Object
//Preset_Name:string			flying objects name
//HelipadLocation:Vector		location to create the object


// This will disable helicopter purchases upon destruction
//
//TDA_Helipad_Controller
//VTOL_Controller1=0:int		VTOL Controller to kill on destruction
//VTOL_Controller2=0:int		VTOL Controller to kill on destruction
//VTOL_Controller3=0:int		VTOL Controller to kill on destruction
//VTOL_Controller4=0:int		VTOL Controller to kill on destruction


// This is the VTOL_Controller that actually creates the Helicopters

//TDA_VTOL_Controller
//Cinematic_Object:string		text cinematic object to create the helicopter



// This is used to grab the purchase from the PT and then send a request to build it 
//to the VTOL Controller
//
//TDA_VTOL_Object
//VTOL_Controller=0:int			ID of the VTOL Controller to bind to.


// Use this to enable/disable stealth effect.  Will activate stealth on enter, and deactivate stealth on exit
//
//TDA_Toggle_Stealth_Zone
//Trigger_Type:int			This is a new setting, you can set the following here.
//							0=only stealth IN zone
//							1=zone entry to enable
//							2=zone exit to disable
//Player_Type				Same as others 0=Nod, 1=GDI, 2=Any


// Use this to teleport a unit to a desired location when they enter this zone 
//	facing the same direction as when they entered the zone
//
//TDA_Teleport_Zone
//Location				Vector location of where to be sent
//Object_ID				Object to teleport to (use an editor only object for this, or else will teleport inside
//						the other object
//One thing to be aware of is that if 2 or more people use this at once, they will appear on top of each other
//So you may want to handle this some how in your map...

// This will Disable a building on ZoneEntry, it will not destroy, just disable.
//
// TDA_Disable_Building_Zone
// Building_ID:int			ID of the building to disable
// Team_ID:int				ID of the team to trigger the script (0=Nod,1=GDI,2=Any)

// These are the controller scripts for a stealth generator attached to a Zone. 
// when player x is in zone y, stealth is enabled, when they leave the zone, it is disabled.
//
//TDA_Stealth_Generator
//Zone_ID:int				ID of the Connecting Zone

//Zone Script for above
//
//TDA_Stealth_Generator_Zone
//Player_Type:int			player type (0=Nod, 1=GDI, 2=Any (unteamed))

//Attach this to a zone for CTF type game... (for this to work, the game 
//			HAS to be able to end on Destroy Buildings)
//
//TDA_CTF_Zone
//Team_ID=0:int					This is what team the Script Zone works for (0=Nod, 1=GDI)
//Max_Capture=5:int				This is the number of captures that will end the game
//Flag_Preset_Name:string		Flag Preset Name
//Building_To_Destroy1=0:int	1st Building to Destroy on Max Captures
//Building_To_Destroy2=0:int	2nd Building to Destroy on Max Captures
//Building_To_Destroy3=0:int	3th Building to Destroy on Max Captures
//Building_To_Destroy4=0:int	4th Building to Destroy on Max Captures
//Building_To_Destroy5=0:int	5th Building to Destroy on Max Captures
//Play_Capture_Sounds=1:int		This setting will enable the CTF Event sounds (0=disable, 1=enable)
