/*	Renegade Scripts.dll
	Scripts to do with death, damage and health
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class JFW_Spawn_Object_Death : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Timer_Destroy_Building : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Blow_Up_On_Death : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Give_Points_On_Death : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Armour_Regen : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Health_Regen : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Death_Destroy_Object : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Spawn_Object_Death_Enable : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Spawn_Object_Death_Random : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Spawn_Object_Death_Enable_Random : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Killed(GameObject *obj,GameObject *shooter);
	void Register_Auto_Save_Variables();
};

class JFW_Engineer_Target : public ScriptImpClass {
	float health;
	float shieldstrength;
	int repairid;
	void Register_Auto_Save_Variables();
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Destroyed(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Engineer_Repair : public ScriptImpClass {
	bool active;
	int repairid;
	int targetid;
	int priority;
	void Register_Auto_Save_Variables();
	void Created(GameObject *obj);
	void Sound_Heard(GameObject *obj,const CombatSound & sound);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
};

class JFW_Invulnerability_Timer : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Death_Destroy_Object_Delay : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Set_Health_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Health_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Max_Health_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Max_Health_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Shield_Strength_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Shield_Strength_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Max_Shield_Strength_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Max_Shield_Strength_On_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Health_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Health_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Max_Health_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Max_Health_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Shield_Strength_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Shield_Strength_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Set_Max_Shield_Strength_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Add_Max_Shield_Strength_On_Custom_Sender : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Regenerate_Health_Conditional : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JFW_Building_Damage : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Building_Damage_Scale : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Blow_Up_On_Death_Driver : public ScriptImpClass {
	int DriverID;
	void Created(GameObject *obj);
	void Killed(GameObject *obj,GameObject *shooter);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Register_Auto_Save_Variables();
};

class JFW_Random_DriverDeath : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Building_Damage_Percentage : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class JFW_Animation_Frame_Damage : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Register_Auto_Save_Variables();
};

class JFW_Damage_Do_Damage : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Register_Auto_Save_Variables();
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Invulnerable_On_Create : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Destroy_Self_Timer : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Change_Model_Health : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Register_Auto_Save_Variables();
};

class JFW_Change_Model_Health2 : public ScriptImpClass {
	bool enabled;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Register_Auto_Save_Variables();
};

class JFW_Damage_Animation : public ScriptImpClass {
	bool enable;
	void Created(GameObject *obj);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Timer_Expired(GameObject *obj,int number);
	void Register_Auto_Save_Variables();
};

class JFW_Spawn_Object_Death_Weapon : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Spawn_Object_Death_Team : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Pilot_Repair : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Damage_Occupants_Death : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class JFW_Engineer_Target_2 : public ScriptImpClass {
	float health;
	float shieldstrength;
	int repairid;
	void Register_Auto_Save_Variables();
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Damaged(GameObject *obj,GameObject *damager,float damage);
	void Destroyed(GameObject *obj);
	void Timer_Expired(GameObject *obj,int number);
};

class JFW_Engineer_Repair_2 : public ScriptImpClass {
	bool active;
	int repairid;
	int targetid;
	int priority;
	void Register_Auto_Save_Variables();
	void Created(GameObject *obj);
	void Sound_Heard(GameObject *obj,const CombatSound & sound);
	void Action_Complete(GameObject *obj,int action,ActionCompleteReason reason);
};

struct KillMessage {
	unsigned int PresetID;
	unsigned int StringID;
	bool SendDriverCustom;
};

class JFW_Kill_Message_Display : public ScriptImpClass {
	KillMessage *messages;
	int count;
	int KillerTeam;
	unsigned int KillerID;
	unsigned int KillerPlayerID;
	unsigned int KilledID;
	unsigned int KilledPlayerID;
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
public:
	~JFW_Kill_Message_Display();
};

class JFW_Kill_Message : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};
