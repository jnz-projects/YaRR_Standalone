; ************************* Scripts by Mark "Shawk" Sararu *************************



====================================================================================================

; ************************* [Script Name] SH_ConsoleCommand

====================================================================================================

 [Description]

 - Looks at ConsoleCommand.txt. If it exists, reads in one line and runs it. Attach to a Daves_Arrow or Invisible_Object. Operates on a timer.

 [Parameters]

 - NONE

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] SH_PCT_Custom

====================================================================================================

 [Description]

 - Shows a purchase terminal to the sender.

 [Parameters]

 - Message (Message needed to show the terminal)

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] SH_PCT_Powerup

====================================================================================================

 [Description]

 - Attach to a powerup. Displays a purchase terminal to whom it's picked up.

 [Parameters]

 - NONE

 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] SH_Spawn_Difficulty

====================================================================================================

 [Description]

 - Spawns an object depending on the current difficulty level.

 [Parameters]

 - ObjectEasy: Name of the preset to spawn at the Easy difficulty level.
 - ObjectEasyEnabled: Set to 1 to enable the preset named in ObjectEasy to be spawned at the Easy difficulty level.
 - ObjectMedium: Name of the preset to spawn at the Medium difficulty level.
 - ObjectMediumEnabled: Set to 1 to enable the preset named in ObjectMedium to be spawned at the Medium difficulty level.
 - ObjectHard: Name of the preset to spawn at the Hard difficulty level.
 - ObjectHardEnabled: Set to 1 to enable the preset named in ObjectHard to be spawned at the Medium difficulty level.
 
 [Notes]

 - NONE

====================================================================================================

; ************************* [Script Name] SH_FileVerificationController

====================================================================================================

 [Description]

 - Provides the server component for the file validation to validate important files for RA:APB

 [Parameters]

 - INI: Not used, the filename is hardcoded to verify.ini now

 [Notes]

 - Requires custom code in apbshaders.dll (which is apb specific) in order to work
