/*	Renegade Scripts.dll
	Scripts by zunnie - mp-gaming.com/fan-maps.net
	Copyright 2007 Zunnie, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it 
	under the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code 
	with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
class z_Console_Input_Killed : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class z_Console_Input_Killed_2 : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class z_Console_Input_Custom_2 : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Enable_Multiple_Spawners_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Play_Three_Cinematics_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Play_Cinematic_Console_Input_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Destroy_Three_Objects_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Console_Input_Poke_2 : public ScriptImpClass {
	void Poked(GameObject *obj,GameObject *poker);
};

class z_Console_Input_Custom_Delay_1 : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
};

class z_Console_Input_Custom_Delay_2 : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
	void Timer_Expired(GameObject *obj,int number);
};

class z_RemoveWeaponModel : public ScriptImpClass {
	void Created(GameObject *obj);
};

class z_rwk : public ScriptImpClass {
	void Killed(GameObject *obj, GameObject *shooter);
};

class z_GameoverCreated : public ScriptImpClass {
	void Created(GameObject *obj);
};

class z_Created_Send_Custom_Param : public ScriptImpClass {
	void Created(GameObject *obj);
};

class z_NoDamageMoneyPoints : public ScriptImpClass {
	void Damaged(GameObject *obj, GameObject *damager, float damage);
};

class z_Set_Team : public ScriptImpClass {
	void Created(GameObject *obj);
};

class z_Set_Skin_Created : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Set_Skin : public ScriptImpClass {
	void Created(GameObject *obj);
};

class z_Death_Enable_Spawner : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class z_Death_Enable_3Spawners : public ScriptImpClass {
	void Killed(GameObject *obj,GameObject *shooter);
};

class z_UnkillableUntilEntered : public ScriptImpClass {
	void Damaged(GameObject *obj, GameObject *damager, float damage);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class z_DestroyVeh259 : public ScriptImpClass {
	void Created(GameObject *obj);
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
	void Timer_Expired(GameObject *obj, int number);
};

class z_VehExit : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};

class z_Spawn_When_Killed : public ScriptImpClass {
	void Killed (GameObject *obj,GameObject *shooter);
};

class z_Remove_Script_Custom : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_Teleport_Powerup : public ScriptImpClass {
	void Custom(GameObject *obj,int message,int param,GameObject *sender);
};

class z_blamo4sec : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
};

class z_ChangeTeamPowerup : public ScriptImpClass {
	void Custom(GameObject *obj, int message, int param, GameObject *sender);
};
