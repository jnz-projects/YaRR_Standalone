/*	Renegade Scripts.dll
	Shader Database Editor Shader Manager Class
	Copyright 2007 Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "shader.h"
#include "editorshader.h"
#include "editorshadermgr.h"
EditorShaderManagerClass *TheEditorShaderManager;

EditorShaderManagerClass::EditorShaderManagerClass()
{
	EditorShaderFactories = new SimpleDynVecClass<EditorShaderFactory *>(0);
	EditorShaders = 0;
}

EditorShaderManagerClass::~EditorShaderManagerClass()
{
	if (EditorShaders)
	{
		Unload_Database();
	}
	delete EditorShaderFactories;
	EditorShaderFactories = 0;
}

EditorShaderClass *EditorShaderManagerClass::Load(ChunkLoadClass& cload)
{
	int x = EditorShaderFactories->Count();
	for (int i = 0;i < x;i++)
	{
		if ((*EditorShaderFactories)[i]->Chunk_ID == cload.Cur_Chunk_ID())
		{
			return (*EditorShaderFactories)[i]->Load(cload);
		}
	}
	char *c = new char[cload.Cur_Chunk_Length()];
	cload.Read(c,cload.Cur_Chunk_Length());
	delete[] c;
	return 0;
}

void EditorShaderManagerClass::Unload_Database()
{
	if (!EditorShaders)
	{
		return;
	}
	int x = EditorShaders->Count();
	for (int i = 0;i < x;i++)
	{
		delete (*EditorShaders)[i];
	}
	delete EditorShaders;
	EditorShaders = 0;
}

void EditorShaderManagerClass::Load_Database(FileClass *file)
{
	EditorShaders = new SimpleDynVecClass<EditorShaderClass *>(0);
	ChunkLoadClass cload(file);
	while (cload.Open_Chunk())
	{
		EditorShaderClass *shader = Load(cload);
		if (shader)
		{
			EditorShaders->Add(shader);
		}
		cload.Close_Chunk();
	}
}

void EditorShaderManagerClass::Save_Database(FileClass *file)
{
	ChunkSaveClass csave(file);
	if (EditorShaders)
	{
		int x = EditorShaders->Count();
		for (int i = 0;i < x;i++)
		{
			(*EditorShaders)[i]->Save(csave);
		}
	}
}

void EditorShaderManagerClass::Register_Editor_Shader_Factory(EditorShaderFactory *loader)
{
	EditorShaderFactories->Add(loader);
}
