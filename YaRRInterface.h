
/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRInterface : public YaRRLink
{
	static char *MapName;
	static bool YaRRLoaded;
public:
	static unsigned int GetHostName(void *id);
	static void ObjectCreate(void *stub, GameObject *o);
	static void VersionHook(int PlayerID,float Version);
	static int Purchase_Hook(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset,const char *data);
	static void Purchase_Mon_Hook(BaseControllerClass *base,GameObject *purchaser,unsigned int cost,unsigned int preset, unsigned int purchaseret, const char *data);

	static void Load();
	static void Unload();
	static void Host_Hook(int ID, TextMessageEnum Type, const char *Msg);
	static void Player_Join_Hook(int ID, const char *Nick);
	static void Player_Leave_Hook(int ID);
	static void Level_Loaded_Hook();
	static void GameOver_Hook();

};