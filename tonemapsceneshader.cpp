/*	Renegade Scripts.dll
	ToneMapSceneShaderClass
	Copyright 2007 Mark Sararu, Jonathan Wilson

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "scripts.h"
#include "shadereng.h"
#include "resourcemanager.h"
#include "shaderstatemanager.h"
#include "effect.h"
#include "shader_scene.h"
#include "tonemapsceneshader.h"

ToneMapSceneShaderClass::ToneMapSceneShaderClass()
{
#ifndef SDBEDIT
	Validated = false;
	ValidationResult = false;
	SceneTextureHandle = 0;
	InputTextureHandle = 0;
	TransferTexture = 0;
	TransferTextureHandle = 0;
#endif
	TransferTextureName = 0;
	SceneShaderClass::SceneShaderClass();
}
ToneMapSceneShaderClass::~ToneMapSceneShaderClass()
{
	if (TransferTextureName)
	{
		delete[] TransferTextureName;		
	}
	TransferTextureName = 0;
#ifndef SDBEDIT
	Free_Texture(TransferTexture);
#endif
	SceneShaderClass::~SceneShaderClass();
}
void ToneMapSceneShaderClass::Load(ChunkLoadClass& cload)
{
 	while (cload.Open_Micro_Chunk())
	{
		switch(cload.Cur_Micro_Chunk_ID())
		{
			case MC_TONEMAPSCENESHADER_TRANSFERTEXTURE:
				TransferTextureName = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(&TransferTextureName,cload.Cur_Micro_Chunk_Length());
				break;
			case MC_SCENESHADER_UID:
				cload.Read(&UID,sizeof(unsigned int));
				break;
			case MC_SCENESHADER_NAME:
				Name = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(Name,cload.Cur_Micro_Chunk_Length());
				break;
			case MC_SCENESHADER_FXFILENAME:
				FXFilename = new char[cload.Cur_Micro_Chunk_Length()];
				cload.Read(FXFilename,cload.Cur_Micro_Chunk_Length());
				#ifndef SDBEDIT
				Effect = new EffectClass(FXFilename);			
				#endif
				break;
		}
		cload.Close_Micro_Chunk();
	}	
}
void ToneMapSceneShaderClass::Save(ChunkSaveClass& csave)
{
	csave.Begin_Chunk(CHUNK_TONEMAPSCENESHADER);
	csave.Begin_Micro_Chunk(MC_TONEMAPSCENESHADER_TRANSFERTEXTURE);
	csave.Write(TransferTextureName,strlen(TransferTextureName) + 1);
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MC_SCENESHADER_UID);
	csave.Write(&UID,sizeof(unsigned int));
	csave.End_Micro_Chunk();
	csave.Begin_Micro_Chunk(MC_SCENESHADER_NAME);
	csave.Write(Name,strlen(Name) + 1);
	csave.End_Micro_Chunk();
 	csave.Begin_Micro_Chunk(MC_SCENESHADER_FXFILENAME);
	csave.Write(FXFilename,strlen(FXFilename) + 1);
	csave.End_Micro_Chunk();
	csave.End_Chunk();
}
#ifndef SDBEDIT
void ToneMapSceneShaderClass::Initialize()
{
	SceneShaderClass::Initialize();
}
bool ToneMapSceneShaderClass::Validate()
{
	if (Validated) 
	{
		return ValidationResult;
	}
	if (!Effect->Initialized)
	{
		if (Effect->EffectLoadTask)
		{
			return false;
		}
		else
		{
			ValidationResult = false;
			Validated = true;
			return false;
		}
	}
	D3DXHANDLE TechniqueHandle = Effect->GetTechniqueByName("SceneShader");
	HRESULT hr = Effect->ValidateTechnique(TechniqueHandle);
	if (FAILED(hr))
	{
		ValidationResult = false;
	} 
	else 
	{
		Effect->SetTechnique("SceneShader");
		InputTextureHandle = Effect->GetParameterBySemantic(NULL,"InputTexture");
		SceneTextureHandle = Effect->GetParameterBySemantic(NULL,"SceneTexture");
		TransferTexture = Load_Texture(TransferTextureName,MIP_LEVELS_ALL,WW3D_FORMAT_UNKNOWN,true);
		TransferTexture->Init();
		if (TransferTexture->Is_Missing_Texture()) {
			ValidationResult = false;
			Validated = true;
			return ValidationResult;
		}
		TransferTextureHandle =	Effect->GetParameterBySemantic(NULL,"TransferTexture");
		ValidationResult = true;
	}
	Validated = true;
	return ValidationResult;
}
void ToneMapSceneShaderClass::Render(SceneShaderRenderInfo *render)
{
	unsigned int cPasses;
	Effect->Begin(&cPasses,0);
	if (TransferTexture->Initialized != true)
	{
		TransferTexture->Init();
	}
	TransferTexture->LastAccessed = *SyncTime;
	Effect->SetTexture(TransferTextureHandle,TransferTexture->D3DTexture->texture9);
	Effect->SetTexture(SceneTextureHandle,render->SceneBuffer);
	for (unsigned int pass = 0; pass < cPasses; pass++)
	{
		if (pass > 0) 
		{
			SceneShaderController->RequestNewBuffer(&render);
		}
		StateManager->SetRenderTarget(0,render->OutputSurface);
		Direct3DDevice->Clear(0,NULL,D3DCLEAR_TARGET,D3DCOLOR_XRGB(0,0,0),0,0);
		Effect->SetTexture(InputTextureHandle,render->InputBuffer);
		Effect->BeginPass(pass);
		Effect->CommitChanges();
		Direct3DDevice->BeginScene();
		RenderQuad->Draw(0,0,render->Width,render->Height,render->UScale,render->VScale);
		Direct3DDevice->EndScene();
		Effect->EndPass();
	}
	Effect->End();
}
#endif

SceneShaderRegistrant<ToneMapSceneShaderClass> ToneMapSceneShaderRegistrant(CHUNK_TONEMAPSCENESHADER,"Tone Map Scene Shader");
