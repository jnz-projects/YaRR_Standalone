//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by sdbedit.rc
//
#define IDD_MAIN                        9
#define IDR_MENU1                       102
#define IDI_APP                         105
#define IDD_GLOW                        106
#define IDD_GLASS                       107
#define IDD_NORMALMAP                   109
#define IDD_GENERICPPS                  111
#define IDD_GENERICPPF                  112
#define IDD_BLOOMPPF                    113
#define IDD_BRIGHTPASSPPF               114
#define IDD_TONEMAPPPF                  115
#define IDC_SHADERTYPES                 1001
#define IDC_NEWSHADER                   1002
#define IDC_SHADERS                     1003
#define IDC_EDITSHADER                  1004
#define IDC_SHADERNAME                  1005
#define IDC_DELSHADER                   1006
#define IDC_SHADERFILE                  1010
#define IDC_OPEN                        1011
#define IDC_THICKNESS                   1012
#define IDC_REFRACT                     1013
#define IDC_TEXTURE                     1014
#define IDC_REFLECT                     1015
#define IDC_PPSHADERTYPES               1016
#define IDC_NEWPPSHADER                 1017
#define IDC_PPSHADERS                   1018
#define IDC_NORMAL                      1019
#define IDC_EDITPPSHADER                1019
#define IDC_PPSHADERNAME                1020
#define IDC_DELPPSHADER                 1021
#define IDC_UID                         1028
#define IDC_SCALE                       1029
#define IDC_SPECULAR                    1038
#define IDC_LIGHTCOLORX                 1039
#define IDC_LIGHTCOLORY                 1040
#define IDC_LIGHTCOLORZ                 1041
#define IDC_LIGHTCOLORW                 1042
#define IDC_AMBCOLORX                   1043
#define IDC_AMBCOLORY                   1044
#define IDC_AMBCOLORZ                   1045
#define IDC_AMBCOLORW                   1046
#define IDC_SPECCOLORX                  1047
#define IDC_SPECCOLORY                  1048
#define IDC_SPECCOLORZ                  1049
#define IDC_SPECCOLORW                  1050
#define IDC_LIGHTDIRX                   1070
#define IDC_COLORX                      1071
#define IDC_LIGHTDIRY                   1072
#define IDC_SURFCOLORX                  1072
#define IDC_LIGHTDIRZ                   1073
#define IDC_COLORY                      1074
#define IDC_COLORZ                      1075
#define IDC_AMBIENTX                    1076
#define IDC_SURFCOLORY                  1076
#define IDC_AMBIENTY                    1077
#define IDC_SURFCOLORZ                  1077
#define IDC_AMBIENTZ                    1078
#define IDC_COLORW                      1079
#define IDC_AMBIENTW                    1080
#define IDC_SURFCOLORW                  1080
#define IDC_SPECULARPOWER               1087
#define ID_FILE_EXIT                    40004
#define ID_FILE_NEWSDB                  40006
#define ID_FILE_OPENSDB                 40007
#define ID_FILE_SAVESDB                 40008
#define ID_FILE_NEWPOST                 40009
#define ID_FILE_OPENPOST                40010
#define ID_FILE_SAVEPOST                40011
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        117
#define _APS_NEXT_COMMAND_VALUE         40012
#define _APS_NEXT_CONTROL_VALUE         1051
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
